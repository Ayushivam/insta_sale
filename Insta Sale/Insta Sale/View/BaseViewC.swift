//
//  BaseViewC.swift
//  IOSAppTemplate
//
//  Created by Ayushi   on 12/04/21.
//

import UIKit

class BaseViewC: UIViewController {

    // MARK: - IBOutlets

    // MARK: - Properties

    // MARK: - View Life Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //--- Hide Navigation Bar ---//
        self.navigationController?.navigationBar.isHidden = true

        //--- Remove Default Back Button ---//
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = nil
        
        self.initialSetup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    deinit {
        
    }

    // MARK: - Init Function
    
    // MARK: - Notifications Functions

    // MARK: - Private Functions
    
    private func initialSetup() {
        
    }
    
    // MARK: - Internal Functions
 
    // MARK: - UIButton Actions
    
    // MARK: - API Calls
}
