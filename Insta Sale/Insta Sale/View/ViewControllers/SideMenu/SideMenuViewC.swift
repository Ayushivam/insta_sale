//
//  SideMenuViewC.swift
//  Insta Sale
//
//  Created by Sajiya on 16/10/21.
//

import UIKit
protocol passSelectActions: AnyObject {
    func passSelectFlag(success: Int)

}

class SideMenuViewC: BaseViewC {


    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bgView: UIView!
    // MARK: - Properties

    var arr = ["Home", "All Categories", "Notification","My Order","My Cart","My Whishlist","Coupon", "My Account", "Sell on Flazh Sale","Customer Support" ,"Logout"]
    var imageArr = [#imageLiteral(resourceName: "home") , #imageLiteral(resourceName: "All-Categories"), #imageLiteral(resourceName: "Notification"), #imageLiteral(resourceName: "My-Order"), #imageLiteral(resourceName: "My-cart"), #imageLiteral(resourceName: "My-wish-List"), #imageLiteral(resourceName: "coupon"), #imageLiteral(resourceName: "My-Account"), #imageLiteral(resourceName: "Sell-on-FlashSale"), #imageLiteral(resourceName: "Customer-Support"), #imageLiteral(resourceName: "logout")]
    weak var delegate : passSelectActions?

    // MARK: - View Life Cycle Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    private func initialSetup() {
        self.tableView.register(nib: SideMenuCell.className)
        self.tableView.reloadData()
        bgView.backgroundColor = UIColor.white
        bgView.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        bgView.layer.shadowOpacity = 4.0
        bgView.layer.shadowOffset = CGSize.zero
        bgView.layer.shadowRadius = 4.0
        //preconditionFailure()

    }
    
    // MARK: - UIButton Actions
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}


// MARK: - UITableViewDelegate, UITableViewDataSource

extension SideMenuViewC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - UITableViewDelegate
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fLog()
        if indexPath.row == 0 {
            self.dismiss(animated: true, completion: nil)

        }
                     
       else  if indexPath.row == 10 {
                    AlertControllerManager.showAlert(title: Text.Logout.localized, message: Message.LogoutConfirmation.localized, buttons: [Text.No.localized, Text.Yes.localized]) { (index) in
                        dLog(message: "Selected Index :: \(index)")
            
                        if index == 1 {
                            kUserDefaults.standard.clear()
                            AppConfig.defautMainQ.async {
                                kAppDelegate.setRootViewC()
                            }
                        }
                    }
        }
        
        
        else  {
            self.dismiss(animated: true, completion: nil)
            self.delegate?.passSelectFlag(success: indexPath.row)
        }
        
       
       
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SideMenuCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureCell()
        cell.headingLabel.text = arr[indexPath.row]
        cell.imgImageView.image = imageArr[indexPath.row]
        return cell
    }
    
}
