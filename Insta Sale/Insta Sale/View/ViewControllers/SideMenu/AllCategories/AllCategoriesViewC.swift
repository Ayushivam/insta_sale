//
//  AllCategoriesViewC.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 12/11/21.
//

import UIKit

class AllCategoriesViewC: BaseViewC {

    // MARK: - IBOutlets
  
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var subCategoriesTableView: UITableView!
    @IBOutlet weak var categorytableView: UITableView!
    // MARK: - Properties
    var categoriesArr = [CategoriesResponse]()
    var subCategoriesArr = [CategoriesResponse]()
//    var bannersArr = [CategoriesResponse]()
//    var feauturedArr = [CategoriesResponse]()

    @IBOutlet weak var categoryNameLabel: UILabel!
    var catUrl = ""
    var subCatUrl = ""

    // MARK: - View Life Cycle Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()

        classStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    private func initialSetup() {
        self.categorytableView.register(nib: CategoryTableCell.className)
        self.subCategoriesTableView.register(nib: SubCategoryTableViewCell.className)

        APIManager.apiForGetCategoryData(controller: self.navigationController!) { (status, categoryArr, subcategoryArr, bannersArr, catUrl, subCatUrl, bannerUrl) in
            if status {
                self.categoriesArr.removeAll()
                self.categoriesArr += CategoriesResponse.getCategoriesArray(responseArray: categoryArr as! Array<Dictionary<String, Any>>)
                self.categoryNameLabel.text = self.categoriesArr[0].name
                self.catUrl = catUrl
                self.categorytableView.reloadData()
                self.subCategoriesTableView.reloadData()
                self.apiForSubCat(id: self.categoriesArr[0].id)

            }
        }
    }
    private func apiForSubCat(id : Int) {
        APIManager.apiForGetShopCategoryData(id: "\(id)", controller: self.navigationController!) { (status, subCategoriesArr,subCatUrl) in
            if status {
                self.subCategoriesArr.removeAll()
                self.subCategoriesArr += CategoriesResponse.getSubCategoriesArray(responseArray: subCategoriesArr as! Array<Dictionary<String, Any>>)
                self.subCatUrl = subCatUrl
                self.subCategoriesTableView.reloadData()
                if self.subCategoriesArr.count == 0 {
                    self.tableViewHeight.constant = 0
                }
                else {
                    self.tableViewHeight.constant = CGFloat(self.subCategoriesArr.count * 50  + 10)

                }

            }
        }
    }
    
    // MARK: - UIButton Actions
    @IBAction func backBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}


// MARK: - UITableViewDelegate, UITableViewDataSource

extension AllCategoriesViewC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - UITableViewDelegate
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == categorytableView {

        return 85
        }
        else {
            return 50
        }
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fLog()
        
        if tableView == categorytableView {
            
            for i in categoriesArr {
                i.itemSelectable = false
            }
            categoriesArr[indexPath.row].itemSelectable = true
            self.categorytableView.reloadData()
            self.categoryNameLabel.text = categoriesArr[indexPath.row].name
            let id = categoriesArr[indexPath.row].id
            apiForSubCat(id: id)

        }
      
        else {
            let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: SubCategoryViewC.className)
            { coder in
                SubCategoryViewC(coder: coder)
            }
            objGetStartedViewC.categoryName = self.subCategoriesArr[indexPath.row].name
            objGetStartedViewC.id = self.subCategoriesArr[indexPath.row].id

            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
        }
       
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == categorytableView {
            return categoriesArr.count
        }
        else {
            return subCategoriesArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == categorytableView {
            let cell: CategoryTableCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureCell(self.categoriesArr, indexPath.row, self.catUrl)
            if categoriesArr[indexPath.row].itemSelectable == true {
                cell.bgViewWidth.constant = 65
                cell.bgViewHeight.constant = 65

            }
            else {
                cell.bgViewWidth.constant = 55
                cell.bgViewHeight.constant = 55

            }
            return cell
        }
        else {
            let cell: SubCategoryTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureCell(self.subCategoriesArr, indexPath.row, self.subCatUrl)

            return cell
        }
       
    }
    
}

