//
//  LogInViewC.swift
//  Insta Sale
//
//  Created by Sajiya on 22/09/21.
//

import UIKit
import AuthenticationServices
import GoogleSignIn

class LogInViewC: BaseViewC {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var emailTextField: PaddedTextField!
    @IBOutlet weak var passwordTextField: PaddedTextField!
    @IBOutlet weak var passwordShowBtn: UIButton!
    @IBOutlet weak var passwordEyeImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    // MARK: - Properties
    var userData = UserInfo()
    private var googleUserData = GIDGoogleUser()

    
    // MARK: - View Life Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    
    // MARK: - Private Functions
    
    private func initialSetup() {
        bgView.layer.roundCorners([.leftTop, .rightTop], corner: 15)
        self.hideKeyboardWhenTappedAround()
        self.emailTextField.text = "ayushianki@gmail.com"
        self.passwordTextField.text = "33298117"
    }
    
    private func inputValidation() {
        if emailTextField.text?.count == 0 {
            AlertControllerManager.showToast(message: Message.EmptyEmail.localized, type: AlertType.error)
        }
        else if !(emailTextField.text?.isValidEmail())!{
            AlertControllerManager.showToast(message: Message.InvalidEmail.localized, type: AlertType.error)
        }
        else  if passwordTextField.text!.count == 0 {
            AlertControllerManager.showToast(message: Message.EmptyPassword.localized, type: AlertType.error)
        }
        else {
            //API Call
            APIManager.apiForLogIn(email: emailTextField.text!, password: passwordTextField.text!, controller: self.navigationController!) { (status, data) in
                if status {
                    self.userData = UserInfo.getUserData(responseData: data)
                    kUserDefaults.userId = self.userData.id

                }
            }
        }
    }
    
    // MARK: - UIButton Actions
    
    @IBAction func passwordShowBtnAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            passwordTextField.isSecureTextEntry = false
            passwordEyeImageView.image = #imageLiteral(resourceName: "visibility (1)")
        }
        else {
            passwordTextField.isSecureTextEntry = true
            passwordEyeImageView.image = #imageLiteral(resourceName: "visibility")
        }
    }
    
    @IBAction func signInBtnAction(_ sender: UIButton) {
        inputValidation()
    }
    
    @IBAction func forgotPasswordBtnAction(_ sender: UIButton) {
        let objGetStartedViewC = Storyboard.Authentication.instantiateViewController(identifier: ForgotPasswordViewC.className)
        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
    
    @IBAction func signUpBtnAction(_ sender: UIButton) {
        let objGetStartedViewC = Storyboard.Authentication.instantiateViewController(identifier: SignUpViewC.className)
        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
    
    @IBAction func fbBtnAction(_ sender: UIButton) {
        fLog()
        FacebookManager.shared.loginWithFacebook(readPermissions: [Permission.email.stringValue, Permission.publicProfile.stringValue], target: self)
        FacebookManager.shared.fbInfoProvider = {
            (success, fbUser) in
            
            APIManager.apiForSignUp(name: "dwd", email: "dsf@gmail.com", password: "434",signUpType: "1" ,controller: self.navigationController!) { (status,msg) in
                if status {
                    AlertControllerManager.showAlert(title: "", message: msg, buttons: ["OK"]) { (index) in
                        self.navigationController?.popViewController(animated: true)

                    }
                }
            }
        }
    }
    
    
    @IBAction func googleBtnAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let googleShared = GIDSignIn.sharedInstance()
        googleShared?.presentingViewController = self
        googleShared?.delegate = self
        googleShared?.signOut()
        googleShared?.signIn()
    }
    
    @IBAction func appleBtnAction(_ sender: UIButton) {
        if #available(iOS 13.0, *) {
            AppleSignInManager.shared.addSignInButton() { (socialUserInfo, message, success) in
                if !success {
                    //AlertControllerManager.showToast(message: Message.APIFail.localized, type: .error)
                }
                else {
                    APIManager.apiForSignUp(name: "dwd", email: "dsf@gmail.com", password: "434",signUpType: "1" ,controller: self.navigationController!) { (status,msg) in
                        if status {
                            AlertControllerManager.showAlert(title: "", message: msg, buttons: ["OK"]) { (index) in
                                self.navigationController?.popViewController(animated: true)

                            }
                        }
                    }
                }
            }
        }

    }
    
}

//MARK:- UIText Field Delegate

extension LogInViewC: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 10:
            if str.length > 60
            {
                return false
            }
            break
        case 11:
            if str.length > 15
            {
                return false
            }
        default:
            break
        }
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .next {
            let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
            tf?.becomeFirstResponder()
        }
        else {
            view.endEditing(true)
        }
        return true
    }
}
//MARK: - Google SignIn Delegates
extension LogInViewC : GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let user = user,
           let userID = user.userID {
            self.googleUserData = user
           // self.viewModel.callAPIToSocialLogin(email: user.profile.email, socialID: userID, socialType: "2",firstName:user.profile.givenName,lastName: user.profile.familyName)
        }
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
    }
}
