//
//  IntroductionViewC.swift
//  Insta Sale
//
//  Created by Sajiya on 22/09/21.
//

import UIKit

class IntroductionViewC: BaseViewC {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Properties

    // MARK: - View Life Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    
    private func initialSetup() {
        self.collectionView.registerMultiple(nibs: [IntroductionCollectionViewCell.className])
        self.collectionView.reloadData()
        nextBtn.backgroundColor = UIColor.white
        nextBtn.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        nextBtn.layer.shadowOpacity = 4.0
        nextBtn.layer.shadowOffset = CGSize.zero
        nextBtn.layer.shadowRadius = 4.0
    }
    
    // MARK: - UIButton Actions
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
        let objGetStartedViewC = Storyboard.Authentication.instantiateViewController(identifier: LogInViewC.className)
        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
        

    }
    
}

// MARK: - collectionView, collectionView
extension IntroductionViewC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        fLog()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: IntroductionCollectionViewCell =
            collectionView.dequeueReusableCell(forIndexPath: indexPath)
        
        cell.configureCell()
        cell.pageController.numberOfPages = 2
        cell.pageController.currentPage = indexPath.row
        cell.pageController.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)

        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let padding: CGFloat = 0
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize.init(width:collectionViewSize , height: collectionView.frame.size.height)
    }
    
}
