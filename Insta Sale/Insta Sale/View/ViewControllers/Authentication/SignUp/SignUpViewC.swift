//
//  SignUpViewC.swift
//  Insta Sale
//
//  Created by Sajiya on 22/09/21.
//

import UIKit

class SignUpViewC: BaseViewC {

    // MARK: - IBOutlets
    @IBOutlet weak var nammeTF: PaddedTextField!
    @IBOutlet weak var emailTextField: PaddedTextField!
    @IBOutlet weak var passwordTextField: PaddedTextField!
    @IBOutlet weak var passwordShowImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    // MARK: - Properties


    // MARK: - View Life Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    
    // MARK: - Private Functions
    
    private func initialSetup() {
        bgView.layer.roundCorners([.leftTop, .rightTop], corner: 15)
        self.hideKeyboardWhenTappedAround()
    }
    
    private func inputValidation() {
        if nammeTF.text?.count == 0 {
            AlertControllerManager.showToast(message: Message.EmptyFirstname.localized, type: AlertType.error)
        }
        else if emailTextField.text?.count == 0 {
            AlertControllerManager.showToast(message: Message.EmptyEmail.localized, type: AlertType.error)
        }
        else if !(emailTextField.text?.isValidEmail())!{
            AlertControllerManager.showToast(message: Message.InvalidEmail.localized, type: AlertType.error)
        }
        else  if passwordTextField.text!.count == 0 {
            AlertControllerManager.showToast(message: Message.EmptyPassword.localized, type: AlertType.error)
        }
        else {
            APIManager.apiForSignUp(name: nammeTF.text!, email: emailTextField.text!, password: passwordTextField.text!, signUpType: "0",controller: self.navigationController!) { (status,msg) in
                if status {
                    AlertControllerManager.showAlert(title: "", message: msg, buttons: ["OK"]) { (index) in
                        self.navigationController?.popViewController(animated: true)

                    }
                }
            }
        }
        
        
    }

   
    // MARK: - UIButton Actions

    @IBAction func backBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpBtnAction(_ sender: UIButton) {
        inputValidation()
        
    }
    
    @IBAction func signInBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func passwordShowBtnAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            passwordTextField.isSecureTextEntry = false
            passwordShowImageView.image = #imageLiteral(resourceName: "visibility (1)")
        }
        else {
            passwordTextField.isSecureTextEntry = true
            passwordShowImageView.image = #imageLiteral(resourceName: "visibility")
        }
    }
}

//MARK:- UIText Field Delegate

extension SignUpViewC: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 10:
            if str.length > 60
            {
                return false
            }
            break
        case 11:
            if str.length > 60
            {
                return false
            }
            break
        case 12:
            if str.length > 15
            {
                return false
            }
        default:
            break
        }
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        
        if textField.tag == 10  {
            let cs = CharacterSet(charactersIn: Message.ACCEPTABLE_CHARACTERS.localized).inverted
            let filtered: String = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
            return (string == filtered)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .next {
            let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
            tf?.becomeFirstResponder()
        }
        else {
            view.endEditing(true)
        }
        return true
    }
}
