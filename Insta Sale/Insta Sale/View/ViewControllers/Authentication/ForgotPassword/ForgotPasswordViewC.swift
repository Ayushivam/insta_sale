//
//  ForgotPasswordViewC.swift
//  Insta Sale
//
//  Created by Sajiya on 22/09/21.
//

import UIKit

class ForgotPasswordViewC: BaseViewC {
    
    // MARK: - IBOutlets
    @IBOutlet weak var emailTF: PaddedTextField!
    @IBOutlet weak var bgView: UIView!
    
    // MARK: - Properties

    // MARK: - View Life Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    
    private func initialSetup() {
        bgView.layer.roundCorners([.leftTop, .rightTop], corner: 15)
        self.hideKeyboardWhenTappedAround()
    }
    
    private func inputValidation() {
        if emailTF.text?.count == 0 {
            AlertControllerManager.showToast(message: Message.EmptyEmail.localized, type: AlertType.error)
        }
        else if !(emailTF.text?.isValidEmail())!{
            AlertControllerManager.showToast(message: Message.InvalidEmail.localized, type: AlertType.error)
        }
        
        else {
            APIManager.apiForForgotPAssword(email: self.emailTF.text!, controller: self.navigationController!) { status, msg in
                if status {
                    AlertControllerManager.showAlert(title: "", message: msg, buttons: ["OK"]) { (index) in
                        self.navigationController?.popViewController(animated: true)
                }
            }
        }
       
    }
    }
    
    // MARK: - UIButton Actions
    
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func submitBtnAction(_ sender: Any) {
        inputValidation()
    }
}
//MARK:- UIText Field Delegate

extension ForgotPasswordViewC: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 10:
            if str.length > 60
            {
                return false
            }
            break
            
        default:
            break
        }
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
