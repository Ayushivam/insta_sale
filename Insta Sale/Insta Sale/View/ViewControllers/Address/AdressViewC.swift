//
//  AdressViewC.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 12/20/21.
//

import UIKit

class AdressViewC: BaseViewC {
    
    // MARK: - IBOutlets
    @IBOutlet weak var addAddBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Properties
    var addressArr = [CategoriesResponse]()
    var defaultAddArr = [CategoriesResponse]()
    var otherAddArr = [CategoriesResponse]()
    
    
    // MARK: - View Life Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    private func initialSetup() {
        self.tableView.register(nib: AddressTableViewCell.className)
        self.getAddressAPI()
    }
    
    
    func getAddressAPI() {
        APIManager.apiForGetAddress(controller: self.navigationController!) { status, arr in
            if status {
                
                self.addressArr.removeAll()
                self.defaultAddArr.removeAll()
                self.otherAddArr.removeAll()
                
                self.addressArr += CategoriesResponse.getAddressArray(responseArray: arr as! Array<Dictionary<String, Any>>)
                self.tableView.reloadData()

                for i in self.addressArr {
                    if i.defaultAdd == 1 {
                         self.defaultAddArr.append(i)
                        self.tableView.reloadData()

                    }
                    else {
                         self.otherAddArr.append(i)
                        self.tableView.reloadData()

                    }
                }
            }
        }
    }
    
    
    // MARK: - UIButton Actions
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addNewAddressBtnAction(_ sender: UIButton) {
        let objGetStartedViewC = Storyboard.Address.instantiateViewController(identifier: AddAddressViewC.className)
        { coder in
            AddAddressViewC(coder: coder)
        }
        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
    
}


// MARK: - UITableViewDelegate, UITableViewDataSource

extension AdressViewC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - UITableViewDelegate
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fLog()
        
        
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.defaultAddArr.count
        }
        else {
            return self.otherAddArr.count
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            if defaultAddArr.count == 0 {
                return 0
            }
            else {
                return 45
            }
            
        }
        else {
            if otherAddArr.count == 0 {
                return 0
            }
            else {
                return 45
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            if defaultAddArr.count == 0 {
                let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 0))
                return headerView
                
            }
            else {
                let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 45))
                headerView.backgroundColor = #colorLiteral(red: 0.8398797512, green: 0.8353447318, blue: 0.8264859319, alpha: 1)
                let label = UILabel()
                label.frame = CGRect.init(x: 20, y: 0, width: headerView.frame.width-10, height: 45)
                label.text = "Default Address"
                label.font = .boldSystemFont(ofSize: 17)
                label.textColor = .black
                
                headerView.addSubview(label)
                
                return headerView
            }
        }
        else {
            
            if otherAddArr.count == 0 {
                let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 0))
                return headerView
                
            }
            else {
                let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 45))
                headerView.backgroundColor = #colorLiteral(red: 0.8398797512, green: 0.8353447318, blue: 0.8264859319, alpha: 1)
                
                let label = UILabel()
                label.frame = CGRect.init(x: 20, y: 0, width: headerView.frame.width-10, height: 45)
                label.text = "Other Address"
                label.font = .boldSystemFont(ofSize: 17)
                label.textColor = .black
                
                headerView.addSubview(label)
                
                return headerView
            }
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell: AddressTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureCell(self.defaultAddArr, indexPath.row)
            cell.delegate = self
            return cell
        }
        else {
            let cell: AddressTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureCell(self.otherAddArr, indexPath.row)
            cell.delegate = self
            return cell
        }
        
    }
    
}
extension AdressViewC :  AddressTableViewCellDelegate {
    func tapRemove(cell: AddressTableViewCell, index: Int,addressId: Int) {
        APIManager.apiForRemoveAddress(addressID: "\(addressId)", controller: self.navigationController!) { status in
            if status {
                self.getAddressAPI()
            }
        }
    }
    func tapEdit(cell: AddressTableViewCell, index: Int, address :CategoriesResponse )
    {
        let objGetStartedViewC = Storyboard.Address.instantiateViewController(identifier: AddAddressViewC.className)
        { coder in
            AddAddressViewC(coder: coder)
        }
        objGetStartedViewC.isFromEdit = true
        objGetStartedViewC.addressArr = address
        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
    
    
}
