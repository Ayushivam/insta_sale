//
//  AddAddressViewC.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 12/20/21.
//

import UIKit

class AddAddressViewC: BaseViewC {
    @IBOutlet weak var stateAddress: UITextField!
    
    @IBOutlet weak var addNewAddressTitle: UILabel!
    @IBOutlet weak var addAddBtn: UIButton!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var defaultMarkBtn: UIButton!
    @IBOutlet weak var workBtn: UIButton!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var cityDistrict: UITextField!
    @IBOutlet weak var locTown: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var pincode: UITextField!
    @IBOutlet weak var mobNum: UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var saveAddressView: UIView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var contactView: UIView!
    // MARK: - IBOutlets
    var isFromEdit = false
    var addressArr = CategoriesResponse()
    var addressType = ""
    var defaultType = "0"

    // MARK: - Properties
    
    
    // MARK: - View Life Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    private func initialSetup() {
        contactView.backgroundColor = UIColor.white
        contactView.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        contactView.layer.shadowOpacity = 2.0
        contactView.layer.shadowOffset = CGSize.zero
        contactView.layer.shadowRadius = 2.0
        
        addressView.backgroundColor = UIColor.white
        addressView.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        addressView.layer.shadowOpacity = 2.0
        addressView.layer.shadowOffset = CGSize.zero
        addressView.layer.shadowOpacity = 2.0

        saveAddressView.backgroundColor = UIColor.white
        saveAddressView.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        saveAddressView.layer.shadowOpacity = 2.0
        saveAddressView.layer.shadowOffset = CGSize.zero
        saveAddressView.layer.shadowOpacity = 2.0
        
        if isFromEdit == true {
            self.addNewAddressTitle.text = "Update Address"
            self.addAddBtn.setTitle("UPDATE ADDRESS", for: .normal)
            self.name.text = addressArr.name
            self.mobNum.text = addressArr.mobile_noLoc
            self.pincode.text = addressArr.pincode
            self.address.text = addressArr.addressLoc
            self.locTown.text = addressArr.locality_town
            self.cityDistrict.text = addressArr.city_district
            self.stateAddress.text = addressArr.stateLoc
            
            if addressArr.address_type == "Home" {
                self.workBtn.borderWidth = 1
                self.homeBtn.borderWidth = 1
                self.workBtn.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                self.homeBtn.borderColor = #colorLiteral(red: 1, green: 0.6644936204, blue: 0, alpha: 1)
                self.addressType = "Home"
            }
            else {
                self.workBtn.borderWidth = 1
                self.homeBtn.borderWidth = 1
                self.homeBtn.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                self.workBtn.borderColor = #colorLiteral(red: 1, green: 0.6644936204, blue: 0, alpha: 1)
                self.addressType = "Work"
            }
            
            if addressArr.defaultAdd == 1 {
                checkImageView.image = #imageLiteral(resourceName: "check-box")
                self.defaultType = "1"
            }
            else {
                checkImageView.image = #imageLiteral(resourceName: "stop")
                self.defaultType = "0"
            }


        }
        else {
            self.addNewAddressTitle.text = "Add New Address"
            self.addAddBtn.setTitle("ADD ADDRESS", for: .normal)
            checkImageView.image = #imageLiteral(resourceName: "stop")
            self.defaultType = "0"
            self.workBtn.borderWidth = 1
            self.homeBtn.borderWidth = 1
            self.workBtn.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            self.homeBtn.borderColor = #colorLiteral(red: 1, green: 0.6644936204, blue: 0, alpha: 1)
            self.addressType = "Home"
        }
    }
    
    // MARK: - UIButton Actions
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addAddressBtnAction(_ sender: UIButton) {
        
        if name.text?.count == 0 {
            AlertControllerManager.showToast(message: Message.EmptyFirstname.localized, type: AlertType.error)
        }
        
        else if mobNum.text?.count == 0 {
            AlertControllerManager.showToast(message: "Please enter mobile number", type: AlertType.error)

        }
        
        else if mobNum.text?.count ?? 0 < 10 {
            AlertControllerManager.showToast(message: "Mobile number should be atleast 10 digits", type: AlertType.error)

        }
        
        else if pincode.text?.count == 0 {
            AlertControllerManager.showToast(message: "Please enter pincode", type: AlertType.error)

        }
        
        else if pincode.text?.count ?? 0 < 5 {
            AlertControllerManager.showToast(message: "Pincode should be atleast 5 digits", type: AlertType.error)

        }
        
        else if address.text?.count == 0 {
            AlertControllerManager.showToast(message: "Please enter Address", type: AlertType.error)
        }
        
        else if locTown.text?.count == 0 {
            AlertControllerManager.showToast(message: "Please enter Locality / Town", type: AlertType.error)
        }
        
        else if cityDistrict.text?.count == 0 {
            AlertControllerManager.showToast(message: "Please enter City / District", type: AlertType.error)
        }
        
        else if stateAddress.text?.count == 0 {
            AlertControllerManager.showToast(message: "Please enter State", type: AlertType.error)
        }
        
    
        
        else {
            APIManager.apiForCreateUpdateAddress(name: self.name.text!, mobNum: self.mobNum.text!, pincode: self.pincode.text!, address: self.address.text!, locality_town: self.locTown.text!, city_district: self.cityDistrict.text!, stateLoc: self.stateAddress.text!, address_type: self.addressType, defaultType: self.defaultType, isEdit: self.isFromEdit, address_Id: "\(self.addressArr.id)", controller: self.navigationController!) { status, msg in
                if status {
                    AlertControllerManager.showAlert(title: "", message: msg, buttons: ["OK"]) { (index) in
                        dLog(message: "Selected Index :: \(index)")

                        if index == 0 {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        }
        
        
       
        
    }
    
    @IBAction func defaultMarkBtnAction(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            checkImageView.image = #imageLiteral(resourceName: "check-box")
            self.defaultType = "1"
        }
        else {
            checkImageView.image = #imageLiteral(resourceName: "stop")
            self.defaultType = "0"

        }
    }
    
    
    @IBAction func workBtnActn(_ sender: UIButton) {
        self.workBtn.borderWidth = 1
        self.homeBtn.borderWidth = 1
        self.homeBtn.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.workBtn.borderColor = #colorLiteral(red: 1, green: 0.6644936204, blue: 0, alpha: 1)
        self.addressType = "Work"
    }
    
    
    @IBAction func homeBtnActn(_ sender: UIButton) {
        self.workBtn.borderWidth = 1
        self.homeBtn.borderWidth = 1
        self.workBtn.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.homeBtn.borderColor = #colorLiteral(red: 1, green: 0.6644936204, blue: 0, alpha: 1)
        self.addressType = "Home"

    }
}



