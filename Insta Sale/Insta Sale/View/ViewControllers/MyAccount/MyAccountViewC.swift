//
//  MyAccountViewC.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 12/11/21.
//

import UIKit

class MyAccountViewC: BaseViewC {

    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userProile: UIImageView!
    // MARK: - Properties
    var headingArr = ["Profile Details","Order","Help Center","WishList","Address","My Queries"]
    var titleArr = ["Change your profile details","Check your order details","Help regarding your recent purchases"," Your most loved styles","Save addresses for faster checkout"," Save addresses for faster checkout"]
    var imageArr = [#imageLiteral(resourceName: "My-Account") , #imageLiteral(resourceName: "My-Order"), #imageLiteral(resourceName: "Customer-Support"), #imageLiteral(resourceName: "My-wish-List"), #imageLiteral(resourceName: "My-wish-List"), #imageLiteral(resourceName: "My-wish-List")]
   var profileData = UserInfo()
    var image_url = ""
   
    // MARK: - View Life Cycle Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    private func initialSetup() {
        self.tableView.register(nib: MyAccountTableViewCell.className)
        APIManager.apiForGetProfile(controller: self.navigationController!) { status, data,image_url in
            if status {
                self.tableView.reloadData()

                self.profileData = UserInfo.getProfileData(responseData: data)
                self.userName.text = self.profileData.name
                self.userProile.sd_setImage(with: URL(string: image_url + self.profileData.imageURl), placeholderImage: #imageLiteral(resourceName: "user_profile"), options: SDWebImageOptions(rawValue: 0))
                self.image_url = image_url

            }
        }
    }

    // MARK: - UIButton Actions
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


// MARK: - UITableViewDelegate, UITableViewDataSource

extension MyAccountViewC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - UITableViewDelegate
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fLog()
        if indexPath.row == 0 {
            let objGetStartedViewC = Storyboard.MyAccount.instantiateViewController(identifier: ProfileDetailsViewC.className)
            { coder in
                ProfileDetailsViewC(coder: coder)
            }
            objGetStartedViewC.profileData = self.profileData
            objGetStartedViewC.image_url = self.image_url

            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
        }
        else if indexPath.row == 1 {
            let objGetStartedViewC = Storyboard.MyOrder.instantiateViewController(identifier: MyOrderViewC.className)
            { coder in
                MyOrderViewC(coder: coder)
            }
            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
            
        }
        else if indexPath.row == 2 {
            let objGetStartedViewC = Storyboard.SideMenu.instantiateViewController(identifier: CustomerSupportViewC.className)
            { coder in
                CustomerSupportViewC(coder: coder)
            }
            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
            
        }
        else if indexPath.row == 3 {
            let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: WishlistViewC.className)
            { coder in
                WishlistViewC(coder: coder)
            }
            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
            
        }
        else if indexPath.row == 4 {
            let objGetStartedViewC = Storyboard.Address.instantiateViewController(identifier: AdressViewC.className)
            { coder in
                AdressViewC(coder: coder)
            }
            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
            
        }
        else if indexPath.row == 5 {
            
        }
        
       
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MyAccountTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        //cell.configureCell()
        cell.titleLabel.text = self.titleArr[indexPath.row]
        cell.headingLabel.text = self.headingArr[indexPath.row]
        cell.cellImageView.image = self.imageArr[indexPath.row]
        return cell
    }
    
}
