//
//  ProfileDetailsViewC.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 12/20/21.
//

import UIKit
import Photos

class ProfileDetailsViewC: BaseViewC {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var profileBg: UIView!
    
    @IBOutlet weak var femaleCheckImageView: UIImageView!
    @IBOutlet weak var maleCheckImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var maleBtn: UIButton!

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var phoneNum: UITextField!
    @IBOutlet weak var emailAddress: UITextField!
    @IBOutlet weak var birthdayBtn: UIButton!
    @IBOutlet weak var location: UITextField!
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var profilePicBtn: UIButton!
    // MARK: - Properties
    
    var profileData = UserInfo()
    var image_url = ""
    var gender = ""
    // MARK: - View Life Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
        self.initialSetup()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    private func initialSetup() {
        self.emailAddress.isUserInteractionEnabled = false

        profileBg.backgroundColor = UIColor.white
        profileBg.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        profileBg.layer.shadowOpacity = 4.0
        profileBg.layer.shadowOffset = CGSize.zero
        profileBg.layer.shadowRadius = 4.0
        self.femaleCheckImageView.isHidden = true
        self.maleCheckImageView.isHidden = true
        
        self.name.text = self.profileData.name
        self.profileImageView.sd_setImage(with: URL(string: image_url + self.profileData.imageURl), placeholderImage: #imageLiteral(resourceName: "user_profile"), options: SDWebImageOptions(rawValue: 0))
        self.emailAddress.text = self.profileData.email
        self.phoneNum.text = self.profileData.mobile_no
        if self.profileData.dob == "" {
            self.birthdayBtn.setTitle("DOB", for: .normal)
        }
        else {
            self.birthdayBtn.setTitle(self.profileData.dob, for: .normal)
        }
         
        self.location.text =  self.profileData.location
        self.gender =  self.profileData.gender
        
        if gender == "" {
            self.femaleCheckImageView.isHidden = true
            self.maleCheckImageView.isHidden = true
        }
        else  if gender == "female" || gender == "Female" {
            self.femaleCheckImageView.isHidden = false
            self.maleCheckImageView.isHidden = true
            self.gender = "female"
        }
        else  if gender == "male" || gender == "Male" {
            self.femaleCheckImageView.isHidden = true
            self.maleCheckImageView.isHidden = false
            self.gender = "male"
        }
        else {
            self.femaleCheckImageView.isHidden = true
            self.maleCheckImageView.isHidden = true
        }



    }
    
    // MARK: - UIButton Actions
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func profileBtnAction(_ sender: Any) {
        self.showActionSheet()

    }
    
    @IBAction func birthdayBtnAction(_ sender: Any) {
        self.view.endEditing(true)
        commonOptionPickerDate(button: birthdayBtn,mode: .date)
    }
    
    @IBAction func femaleBtnAction(_ sender: Any) {
        self.femaleCheckImageView.isHidden = false
        self.maleCheckImageView.isHidden = true
        self.gender = "female"

    }
    
    @IBAction func maleBtnAction(_ sender: Any) {
        self.femaleCheckImageView.isHidden = true
        self.maleCheckImageView.isHidden = false
        self.gender = "male"

    }
    
    @IBAction func updateBtnAction(_ sender: Any) {
        
        if name.text?.count == 0 {
            AlertControllerManager.showToast(message: Message.EmptyFirstname.localized, type: AlertType.error)
        }
        
        else if phoneNum.text?.count == 0 {
            AlertControllerManager.showToast(message: "Please enter mobile number", type: AlertType.error)

        }
        
        else if phoneNum.text?.count ?? 0 < 10 {
            AlertControllerManager.showToast(message: "Mobile number should be atleast 10 digits", type: AlertType.error)

        }
        
        else if birthdayBtn.currentTitle == "DOB" {
            AlertControllerManager.showToast(message: "Please select DOB", type: AlertType.error)

        }
        
        else if location.text?.count == 0 {
            AlertControllerManager.showToast(message: "Please enter location", type: AlertType.error)
        }
        else if location.text?.count ?? 0 < 3 {
            AlertControllerManager.showToast(message: "Location should be atleast 3 or more characters", type: AlertType.error)

        }
        
        else if gender == "" {
            AlertControllerManager.showToast(message: "Please enter gender", type: AlertType.error)

        }
        
        else {
        
        APIManager.apiForUpdateProfile(name: self.name.text!, mobNum: self.phoneNum.text!, gender: self.gender, dob: self.birthdayBtn.currentTitle!, address: self.location.text!, controller: self.navigationController!) { status,msg in
            if status{
                AlertControllerManager.showOkAlert(title: "", message: msg)
            }
        }
    }
    }
    
    
    func commonOptionPickerDate(button: UIButton,mode:UIDatePicker.Mode) {
        let calendar = Calendar(identifier: .gregorian)
        var comps = DateComponents()
        comps.year = -16
        let maxDate = calendar.date(byAdding: comps, to: Date())
        
        RPicker.selectDate(title: "", cancelText: "Cancel", datePickerMode: .date, minDate: nil, maxDate: maxDate, style: .Inline,didSelectDate: {[weak self] (selectedDate) in
            
            //   TODO: Your implementation for date
            let   formattedDate =  Helper.convertDateFromString(dateString: "\(selectedDate)", withFormat: DateFormat.ddMMYYYY1, inputFormat: DateFormat.dateiOS)!
           
            button.setTitle(formattedDate, for: .normal)
            self!.birthdayBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        })
    }
    
    func apiProfileImageAPIUpdate() {
        var userDict : Dictionary = Dictionary<String,AnyObject>()
        userDict["id"] = "\(kUserDefaults.userId)" as AnyObject

        APIManager.apiForUpdateProfile(dict: userDict, data: (self.profileImageView.image!.pngData()!), haveImage: true) { status in
            print(status)
        }
    }
}

extension ProfileDetailsViewC {

    // MARK: - Open Action sheet for select media option
    func showActionSheet() {
        AlertControllerManager.showActionSheet(title: nil, message: nil, buttons: ["Camera", "Gallery"]) { [self] (index) in
            if index == 0 {
                self.openCamera()

            }
            else if index == 1 {
            self.openGallery()

            }
            
        }
    }

    // MARK: - open gallery
    internal func openGallery() {
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status {
            case .authorized: self.takeGalleryPhoto()
            case .notDetermined: self.takeGalleryPhoto()
            default: self.alertPromptToAllowCameraAccessViaSettings()
            }
        }
    }

    // MARK: - open camera
    internal func openCamera() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: .video)
        switch authStatus {
        case .authorized: takeCameraPhoto()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    self.takeCameraPhoto()
                }
            })
        default: alertPromptToAllowCameraAccessViaSettings()
        }
    }

    // MARK: - Target method for handling gallery response
    @objc private func takeGalleryPhoto() {
        ImgPickerHandler.sharedHandler.getImageDict(instance: self, isSourceCamera: false, rect: nil, mediaType: PickerMediaType.AllMedia, completion: {[weak self] (dict, success) in
            guard let sself = self else { return }

            if success {
                if let editedImage = dict[(sself.convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)) ] as? UIImage {
                    print(editedImage)
                    self?.profileImageView.image = editedImage.fixOrientationAndResize(width: 500,height:500)
                    self?.apiProfileImageAPIUpdate()
                }
                
            }
            else {
            }
        })
    }

// MARK: - Target method for handling camera response
    private func takeCameraPhoto() {

        ImgPickerHandler.sharedHandler.getImageDict(instance: self, isSourceCamera: true, rect: nil, mediaType: PickerMediaType.AllMedia, completion: {[weak self] (dict, success) in
            guard let sself = self else { return }

            if success {
                if let editedImage = dict[(sself.convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)) ] as? UIImage {
                    print(editedImage)
                    self?.profileImageView.image = editedImage.fixOrientationAndResize(width: 500,height:500)
                    self?.apiProfileImageAPIUpdate()

                }
                
            }
            else {
                }
        })
    }

    fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
        input.rawValue
    }

    fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
        Dictionary(uniqueKeysWithValues: input.map { key, value in
            (key.rawValue, value)
        })
    }
    func alertPromptToAllowCameraAccessViaSettings() {
        AppConfig.defautMainQ.asyncAfter(deadline: .now() + 0.1) {
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
    }
    
}



