//
//  HomeViewC.swift
//  Insta Sale
//
//  Created by Sajiya on 25/09/21.
//

import UIKit



class HomeViewC: BaseViewC {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var categoryBannerCollectionView: UICollectionView!
    @IBOutlet weak var category2CollectionView: UICollectionView!
  
    // MARK: - Properties

    var categoriesArr = [CategoriesResponse]()
    var subCategoriesArr = [CategoriesResponse]()
    var bannersArr = [CategoriesResponse]()
    var feauturedArr = [CategoriesResponse]()

    var catUrl = ""
    var subCatUrl = ""
    var bannerUrl = ""
    
    // MARK: - View Life Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
        startTimer()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    private func initialSetup() {
        self.tableView.register(nib: HomeTableViewCell.className)
        self.tableView.reloadData()
        
        self.categoryBannerCollectionView.registerMultiple(nibs: [CategoryBannerCollectionViewCell.className])
        self.categoryCollectionView.registerMultiple(nibs: [CategoryCollectionViewCell.className])
        self.category2CollectionView.registerMultiple(nibs: [Category2CollectionViewCell.className])

        APIManager.apiForGetCategoryData(controller: self.navigationController!) { (status, categoryArr, subcategoryArr, bannersArr, catUrl, subCatUrl, bannerUrl) in
            if status {
                self.categoriesArr.removeAll()
                self.subCategoriesArr.removeAll()
                self.bannersArr.removeAll()

                self.categoriesArr += CategoriesResponse.getCategoriesArray(responseArray: categoryArr as! Array<Dictionary<String, Any>>)
                self.subCategoriesArr += CategoriesResponse.getSubCategoriesArray(responseArray: subcategoryArr as! Array<Dictionary<String, Any>>)
                self.bannersArr += CategoriesResponse.getBannersArray(responseArray: bannersArr as! Array<Dictionary<String, Any>>)

                self.catUrl = catUrl
                self.subCatUrl = subCatUrl
                self.bannerUrl = bannerUrl

                self.categoryCollectionView.reloadData()
                self.category2CollectionView.reloadData()
                self.categoryBannerCollectionView.reloadData()
            }
        }
        APIManager.apiForGetFeaturedCategories(controller: self.navigationController!) { (status, feauturedArr) in
            if status {
                self.feauturedArr.removeAll()
                self.feauturedArr += CategoriesResponse.getFeaturedArray(responseArray: feauturedArr as! Array<Dictionary<String, Any>>)
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK:- Automatic Scroll
    func startTimer() {
        let _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }
    
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        let pageWidth:CGFloat = self.categoryBannerCollectionView.frame.width
        let maxWidth:CGFloat = pageWidth * CGFloat(3)
        let contentOffset:CGFloat = self.categoryBannerCollectionView.contentOffset.x
        var slideToX = contentOffset + pageWidth
        
        if  slideToX == maxWidth{
            slideToX = 0
        }
        self.categoryBannerCollectionView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.categoryBannerCollectionView.frame.height), animated: true)
    }
    

    
    
    // MARK: - UIButton Actions
    
    @IBAction func sideMenuBtnAction(_ sender: UIButton) {
        let objAddVaccineViewC = Storyboard.SideMenu.instantiateViewController(identifier: SideMenuViewC.className) { coder in
            SideMenuViewC(coder: coder)

        }
        objAddVaccineViewC.modalTransitionStyle   = .crossDissolve
        objAddVaccineViewC.modalPresentationStyle = .overFullScreen
        objAddVaccineViewC.delegate = self

        self.navigationController?.present(objAddVaccineViewC, animated: true)
      
    }
    
    @IBAction func searchBtnAction(_ sender: UIButton) {
        let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: SearchViewC.className)
        { coder in
            SearchViewC(coder: coder)
        }
        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)

    }
    
    @IBAction func cartBtnAction(_ sender: UIButton) {
        let objGetStartedViewC = Storyboard.Cart.instantiateViewController(identifier: CartViewC.className)
        { coder in
            CartViewC(coder: coder)
        }
        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
}

// MARK: - collectionView, collectionView

extension HomeViewC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        fLog()
        
        if collectionView == self.categoryCollectionView {
            
            let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: ShopByCategoryViewC.className)
            { coder in
                ShopByCategoryViewC(coder: coder)
            }
            objGetStartedViewC.categoryName = self.categoriesArr[indexPath.row].name
            objGetStartedViewC.id = self.categoriesArr[indexPath.row].id

            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
        }
        
        else if collectionView == self.category2CollectionView {
            
            let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: SubCategoryViewC.className)
            { coder in
                SubCategoryViewC(coder: coder)
            }
            objGetStartedViewC.categoryName = self.subCategoriesArr[indexPath.row].name
            objGetStartedViewC.id = self.subCategoriesArr[indexPath.row].id

            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
        }
        
        

    }
    
   // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.categoryBannerCollectionView {
            return self.bannersArr.count
        }
        
        else  if collectionView == self.categoryCollectionView {
            return self.categoriesArr.count
        }
        
        else if collectionView == self.category2CollectionView  {
            return self.subCategoriesArr.count
        }
        
        else {
            return 1
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.categoryBannerCollectionView {
            let cell: CategoryBannerCollectionViewCell =
                collectionView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureCell(self.bannersArr, indexPath.row, bannerUrl)
            cell.pageController.drawer = WormDrawer(numberOfPages: 3, height: 8, width: 8, space: 10, currentItem: CGFloat(indexPath.row))
            return cell
        }
        
        else if collectionView == self.categoryCollectionView {
            
            let cell: CategoryCollectionViewCell =
                collectionView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureCell(self.categoriesArr, indexPath.row, self.catUrl)
            return cell
        }
        
        else {
            let cell: Category2CollectionViewCell =
                collectionView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureCell(self.subCategoriesArr, indexPath.row, self.subCatUrl)
            return cell
        }

    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.categoryBannerCollectionView {
            return CGSize.init(width: collectionView.frame.size.width , height: collectionView.frame.size.height)
        }
        
        else if collectionView == self.categoryCollectionView {
            return CGSize.init(width:80 , height: collectionView.frame.size.height)
        }
        
        else  {
            return CGSize.init(width:80 , height: collectionView.frame.size.height)
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == self.categoryBannerCollectionView {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        
        else  if collectionView == self.categoryCollectionView {
            return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
            
        }
        else {
            return UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        }
      
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension HomeViewC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - UITableViewDelegate
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fLog()
     
       
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feauturedArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: HomeTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureCell(self.feauturedArr, indexPath.row, false)
        cell.delegate = self
        return cell
    }
    
}

extension HomeViewC : HomeTableViewCellDelegate {
    func tapSelectItem(cell: HomeTableViewCell,id: Int) {
        let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: ProductDetailViewC.className)
               { coder in
                   ProductDetailViewC(coder: coder)
               }
        objGetStartedViewC.id = id
               self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
    
}
extension HomeViewC : passSelectActions {
    func passSelectFlag(success: Int) {
        if success == 1
        {
            let objGetStartedViewC = Storyboard.SideMenu.instantiateViewController(identifier: AllCategoriesViewC.className)
            { coder in
                AllCategoriesViewC(coder: coder)
            }
            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
        }
        else if success == 2
        {
            let objGetStartedViewC = Storyboard.SideMenu.instantiateViewController(identifier: NotificationViewC.className)
            { coder in
                NotificationViewC(coder: coder)
            }
            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
        }
        
        else if success == 3
        {
            let objGetStartedViewC = Storyboard.MyOrder.instantiateViewController(identifier: MyOrderViewC.className)
            { coder in
                MyOrderViewC(coder: coder)
            }
            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
        }
        else if success == 4
        {
            let objGetStartedViewC = Storyboard.Cart.instantiateViewController(identifier: CartViewC.className)
            { coder in
                CartViewC(coder: coder)
            }
            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
        }
        
        else if success == 5
        {
            let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: WishlistViewC.className)
            { coder in
                WishlistViewC(coder: coder)
            }
            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
        }
        
        else if success == 6
        {
            let objGetStartedViewC = Storyboard.SideMenu.instantiateViewController(identifier: CouponViewC.className)
            { coder in
                CouponViewC(coder: coder)
            }
            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
        }
        
        else if success == 7
        {
            let objGetStartedViewC = Storyboard.MyAccount.instantiateViewController(identifier: MyAccountViewC.className)
            { coder in
                MyAccountViewC(coder: coder)
            }
            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
        }
        
        else if success == 9
        {
            let objGetStartedViewC = Storyboard.SideMenu.instantiateViewController(identifier: CustomerSupportViewC.className)
            { coder in
                CustomerSupportViewC(coder: coder)
            }
            self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
        }

    }
}
