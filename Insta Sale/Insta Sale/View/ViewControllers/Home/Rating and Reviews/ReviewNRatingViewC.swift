//
//  ReviewNRatingViewC.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 24/10/21.
//

import UIKit

class ReviewNRatingViewC: BaseViewC {
    // MARK: - IBOutlets

    @IBOutlet weak var tableView: UITableView!

    // MARK: - Properties
   var reviewArr = [CategoriesResponse]()

    // MARK: - View Life Cycle Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    // MARK: - Private Functions
    private func initialSetup() {
        self.tableView.registerMultiple(nibs: [ReviewsTableViewCell.className])
        self.tableView.reloadData()
    }

    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension ReviewNRatingViewC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - UITableViewDelegate
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
            return 72
        

    }
    
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fLog()
        
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return self.reviewArr.count
        
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

       
            let cell: ReviewsTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureCell(self.reviewArr, indexPath.row,true)
            return cell
            
        
       

       
    }
    
}
