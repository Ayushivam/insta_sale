//
//  SubCategoryViewC.swift
//  Insta Sale
//
//  Created by Sajiya on 05/10/21.
//

import UIKit

class SubCategoryViewC: BaseViewC {

    // MARK: - IBOutlets

    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var noDataFound: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    // MARK: - Properties
    var productsArr = [CategoriesResponse]()

    var categoryName = ""
    var id = 0
    var PageNo = 1
    var total = 0

    // MARK: - View Life Cycle Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
        self.initialSetup()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.PageNo = 1

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    private func initialSetup() {
        self.noDataFound.isHidden = true

        self.titleLabel.text = self.categoryName
        self.collectionView.registerMultiple(nibs: [SubCategoryCollectionViewCell.className])
        self.apiCall(optStr: "")
    }
    
    private func apiCall(optStr : String) {
        APIManager.apiForGetProductDetailCategoryData(id: "\(id)", opt: optStr, page: PageNo, controller: self.navigationController!) { (status, productsArr, total) in
            if status {
                self.productsArr.removeAll()
                self.productsArr += CategoriesResponse.getProductsArray(responseArray: productsArr as! Array<Dictionary<String, Any>>)
                self.total = total
                self.collectionView.reloadData()
                if self.productsArr.count == 0 {
                    self.noDataFound.isHidden = false
                }
                else {
                    self.noDataFound.isHidden = true
                }

        }
    }
    }

    
    // MARK: - UIButton Actions

    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchBtnAction(_ sender: UIButton) {
        let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: SearchViewC.className)
        { coder in
            SearchViewC(coder: coder)
        }
        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
    
    @IBAction func whishlistBtnAction(_ sender: UIButton) {
        let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: WishlistViewC.className)
        { coder in
            WishlistViewC(coder: coder)
        }
        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
    
    @IBAction func cartBtnAction(_ sender: UIButton) {
        let objGetStartedViewC = Storyboard.Cart.instantiateViewController(identifier: CartViewC.className)
        { coder in
            CartViewC(coder: coder)
        }
        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
    
    @IBAction func sortBtnAction(_ sender: UIButton) {
    }
    
    @IBAction func filterBtnAction(_ sender: Any) {
        let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: FilterViewC.className)
        { coder in
            FilterViewC(coder: coder)
        }
        objGetStartedViewC.id = "\(id)"
        objGetStartedViewC.delegate = self
        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        callReloadApi()


    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let translation = scrollView.panGestureRecognizer.translation(in: scrollView.superview!)
        if translation.y > 0 {
            print("scrolling down")
            self.filterView.isHidden = false

        } else {
            print("scrolling up")
            self.filterView.isHidden = true

        }

    }
  
    
    func callReloadApi()
    {

        if ((collectionView.contentOffset.y + collectionView.frame.size.height) >= collectionView.contentSize.height)
        {
            PageNo = PageNo + 1
            if self.total == self.productsArr.count {
            }
            else {
                APIManager.apiForGetProductDetailCategoryData(id: "\(id)", opt: "", page: PageNo, controller: self.navigationController!) { (status, productsArr, total) in
                    if status {
                        
                        self.productsArr += CategoriesResponse.getProductsArray(responseArray: productsArr as! Array<Dictionary<String, Any>>)
                        self.collectionView.reloadData()
                    }
                    if self.productsArr.count == 0 {
                        self.noDataFound.isHidden = false
                    }
                    else {
                        self.noDataFound.isHidden = true
                        
                    }
                    
                }
            }
            
            
        }
    }
    
}
// MARK: - collectionView, collectionView

extension SubCategoryViewC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        fLog()
        
        let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: ProductDetailViewC.className)
               { coder in
                   ProductDetailViewC(coder: coder)
               }
        objGetStartedViewC.id = self.productsArr[indexPath.row].id
               self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return  self.productsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: SubCategoryCollectionViewCell =
            collectionView.dequeueReusableCell(forIndexPath: indexPath)
        
        cell.configureCell(self.productsArr, indexPath.row)
        cell.delegate = self
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewSize = self.collectionView.frame.size.width - 15
        
        return CGSize(width: collectionViewSize/2 + 3, height: 300)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 2, bottom: 0, right: 2)
    }
    
    
}

  
extension SubCategoryViewC :  SubCategoryCollectionViewCellDelegate {
    func tapWishlist(cell: SubCategoryCollectionViewCell, index: Int) {
        let obj = productsArr[index]
        if obj.wishlist_id == "0" {
            obj.wishlist_id = "1"
            APIManager.apiForWishlist(prodID: "\(obj.id)", prodVariationID: "\(obj.prodVariationID)", controller: self.navigationController!) { status in
                if status {}
            }
        }
        else {
            
            APIManager.apiForRemoveWishlist(whislist: obj.wishlist_id, controller: self.navigationController!) { status in
                if status {
                    obj.wishlist_id = "0"

                }
            }
        }
        self.collectionView.reloadData()

    }
}
extension SubCategoryViewC : passApplyButtonActions {
    func passFilterValue(str : String) {
        self.apiCall(optStr: str)
    }
   
}
