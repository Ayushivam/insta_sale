//
//  SearchViewC.swift
//  Insta Sale
//
//  Created by Sajiya on 16/10/21.
//

import UIKit

class SearchViewC: BaseViewC {

    // MARK: - IBOutlets

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var crossImage: UIImageView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    var categoriesArr = [CategoriesResponse]()
    var SearchArr = [CategoriesResponse]()

    var catUrl = ""

    // MARK: - View Life Cycle Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    private func initialSetup() {
       
        self.tableView.registerMultiple(nibs: [SearchResultTableViewCell.className,SearchTextTableViewCell.className])

        self.tableView.reloadData()
        self.collectionView.registerMultiple(nibs: [SearchTableViewCell.className])
        self.crossImage.isHidden = true
        APIManager.apiForGetCategoryData(controller: self.navigationController!) { (status, categoryArr, subcategoryArr, bannersArr, catUrl, subCatUrl, bannerUrl) in
            if status {
                self.categoriesArr.removeAll()
                self.categoriesArr += CategoriesResponse.getCategoriesArray(responseArray: categoryArr as! Array<Dictionary<String, Any>>)
                self.catUrl = catUrl
                self.collectionView.reloadData()
            }
        }
    }
    
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func crossBtnAction(_ sender: UIButton) {
        self.crossImage.isHidden = true
        self.searchTF.text = ""
        self.SearchArr.removeAll()
        self.tableView.reloadData()
        view.endEditing(true)

    }
    
}
// MARK: - collectionView, collectionView

extension SearchViewC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        fLog()
        let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: ShopByCategoryViewC.className)
        { coder in
            ShopByCategoryViewC(coder: coder)
        }
        objGetStartedViewC.categoryName = self.categoriesArr[indexPath.row].name
        objGetStartedViewC.id = self.categoriesArr[indexPath.row].id

        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.categoriesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: SearchTableViewCell =
            collectionView.dequeueReusableCell(forIndexPath: indexPath)
        
        cell.configureCell(self.categoriesArr, indexPath.row, catUrl)
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewSize = self.collectionView.frame.width/3.0 - 8
        
        return CGSize(width: collectionViewSize, height: 80)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 2, left: 2, bottom: 2, right: 2)
    }
    
    
}



// MARK: - UITableViewDelegate, UITableViewDataSource

extension SearchViewC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - UITableViewDelegate
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 60

        }
        else {
            return 30

        }
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fLog()
        let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: SubCategoryViewC.className)
        { coder in
            SubCategoryViewC(coder: coder)
        }
        objGetStartedViewC.categoryName = self.SearchArr[indexPath.row].subcategory
        objGetStartedViewC.id = self.SearchArr[indexPath.row].id

        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
       
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
            return 2

        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.SearchArr.count

        }
        else {
            return 1

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell: SearchResultTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureCell(self.SearchArr, indexPath.row)
            return cell
        }
        else {
            let cell: SearchTextTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
           
            return cell
        }
        
       
    }
    
}
extension SearchViewC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.crossImage.isHidden = false

    }
    
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        APIManager.apiForSearch(searchText: textField.text!, controller: self.navigationController!) { status, arr in
            if status {
                self.SearchArr.removeAll()
                self.SearchArr += CategoriesResponse.getSearchArray(responseArray: arr as! Array<Dictionary<String, Any>>)
                self.tableView.reloadData()
            }
        }
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 10:
            if str.length > 100
            {
                return false
            }
            break
            
        default:
            break
        }
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        crossImage.isHidden = true
        return true
    }
}
