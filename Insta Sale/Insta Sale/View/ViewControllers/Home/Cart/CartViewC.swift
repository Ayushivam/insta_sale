//
//  CartViewC.swift
//  Insta Sale
//
//  Created by Sajiya on 17/10/21.
//

import UIKit

class CartViewC: BaseViewC {
    
    // MARK: - IBOutlets
    
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    
    // MARK: - View Life Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    private func initialSetup() {
        self.tableView.registerMultiple(nibs: [CartAddressTableViewCell.className,CartFooterTableViewCell.className,CartItemTableViewCell.className,CartAddressTableViewCell.className,])
        tableView.estimatedRowHeight = 80

        self.tableView.reloadData()
        
    }
    
    // MARK: - UIButton Actions
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: - UITableViewDelegate, UITableViewDataSource

extension CartViewC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - UITableViewDelegate
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension

        }
        else  if indexPath.section == 2 {
            return 200
        }
        else {
            return 50
        }
        }
    
    
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fLog()
        if indexPath.section == 0 {
        

        }
        else {
          

        }
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
            return 4
 
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
       else  if section == 1 ||  section == 3 {
            return 1
        }
        else {
            return 2
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {

            let cell: CartAddressTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
           
            return cell
        }
        else  if indexPath.section == 1 {
            let cell: CartFooterTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.itemsLabel.text = "2 ITEMS"
            cell.totalLabel.isHidden = false
            return cell
        }
        else if indexPath.section == 3 {
            let cell: CartFooterTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.itemsLabel.text = "COUPONS"
            cell.totalLabel.isHidden = true
            return cell
        }
        
        else {
            let cell: CartItemTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            return cell
        }
        
       
    }
    
}
