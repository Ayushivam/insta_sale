//
//  FilterViewC.swift
//  Insta Sale
//
//  Created by Sajiya on 16/10/21.
//

import UIKit

protocol passApplyButtonActions: AnyObject {
    func passFilterValue(str : String)

}
class FilterViewC: BaseViewC {
   
    // MARK: - IBOutlets

    @IBOutlet weak var propertyTableView: UITableView!
    @IBOutlet weak var categoryTableView: UITableView!
    
    // MARK: - Properties
    var id = ""
    var productsFilterArr = [CategoriesResponse]()
    var productsCategoryFilterArr = [CategoriesResponse]()
    var idArr = Array<Int>()
    weak var delegate : passApplyButtonActions?

    // MARK: - View Life Cycle Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    private func initialSetup() {
        self.propertyTableView.registerMultiple(nibs: [PropertyTableViewCell.className])
        self.categoryTableView.registerMultiple(nibs: [PropertyCategoryTableViewCell.className])
        
        APIManager.apiForGetFilters(id: id, controller: self.navigationController!) { status, productFilterArr in
            if status {
                self.productsFilterArr.removeAll()
                self.productsCategoryFilterArr.removeAll()

                self.productsFilterArr += CategoriesResponse.getFilterArray(responseArray: productFilterArr as! Array<Dictionary<String, Any>>)
                self.propertyTableView.reloadData()
                self.productsCategoryFilterArr += self.productsFilterArr[0].productsCategoryArr
                self.categoryTableView.reloadData()

            }
        }
    }
    
    // MARK: - UIButton Actions

    @IBAction func backButtonAction(_ sender: UIButton) {
        self.delegate?.passFilterValue(str: "")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func applyBtnAction(_ sender: UIButton) {
        self.idArr.removeAll()
        for i in productsFilterArr {
            for j in i.productsCategoryArr {
                if j.itemSelectable == true {
                    print(j.id)
                    self.idArr.append(j.id)
                }
            }
        }
        
        let selectedValue = (self.idArr.map{String($0)}).joined(separator: ",")
        self.delegate?.passFilterValue(str: selectedValue)
        self.navigationController?.popViewController(animated: true)

        self.idArr.removeAll()

    }
    
    @IBAction func clearBtnAction(_ sender: UIButton) {
        self.idArr.removeAll()

        APIManager.apiForGetFilters(id: id, controller: self.navigationController!) { status, productFilterArr in
            if status {
                self.productsFilterArr.removeAll()
                self.productsCategoryFilterArr.removeAll()

                self.productsFilterArr += CategoriesResponse.getFilterArray(responseArray: productFilterArr as! Array<Dictionary<String, Any>>)
                self.propertyTableView.reloadData()
                self.productsCategoryFilterArr += self.productsFilterArr[0].productsCategoryArr
                self.categoryTableView.reloadData()

            }
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension FilterViewC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - UITableViewDelegate
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == propertyTableView {
            return 70
        }
        else {
            return 50
        }
        }
    
    
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fLog()
        if tableView == propertyTableView {
            self.productsCategoryFilterArr.removeAll()
            self.productsCategoryFilterArr += self.productsFilterArr[indexPath.row].productsCategoryArr
            for i in productsFilterArr {
                i.itemSelectable = false
            }
            productsFilterArr[indexPath.row].itemSelectable = true
            self.propertyTableView.reloadData()
            self.categoryTableView.reloadData()

        }
        else {
            productsCategoryFilterArr[indexPath.row].itemSelectable = !productsCategoryFilterArr[indexPath.row].itemSelectable
            
            self.categoryTableView.reloadRows(at: [IndexPath(row:indexPath.row,section:indexPath.section)], with: .automatic)

        }
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
            return 1
 
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == propertyTableView {
            return self.productsFilterArr.count
        }
        else {
            return self.productsCategoryFilterArr.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == propertyTableView {
            
            let cell: PropertyTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureCell(self.productsFilterArr,indexPath.row)
            if productsFilterArr[indexPath.row].itemSelectable == true {
                cell.contentView.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9529411765, blue: 0.9450980392, alpha: 1)
            }
            else {
                cell.contentView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

            }
            return cell
        }
        else {
            let cell: PropertyCategoryTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureCell(self.productsCategoryFilterArr,indexPath.row)
            return cell
        }
        
       
    }
    
}
