//
//  WishlistViewC.swift
//  Insta Sale
//
//  Created by Sajiya on 17/10/21.
//

import UIKit

class WishlistViewC: BaseViewC {
    
    // MARK: - IBOutlets

    @IBOutlet weak var noDataFound: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    var productsArr = [CategoriesResponse]()

    var categoryName = ""
    var id = 0
    var PageNo = 1
    var total = 0

    // MARK: - View Life Cycle Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    private func initialSetup() {
        self.noDataFound.isHidden = true

        self.collectionView.registerMultiple(nibs: [SubCategoryCollectionViewCell.className])
        APIManager.apiForGetWishList(controller: self.navigationController!) { status, getWishlistarr in
        
            if status {
                self.productsArr.removeAll()
                self.productsArr += CategoriesResponse.getProductsArray(responseArray: getWishlistarr as! Array<Dictionary<String, Any>>)
                self.collectionView.reloadData()
                if self.productsArr.count == 0 {
                    self.noDataFound.isHidden = false
                }
                else {
                    self.noDataFound.isHidden = true

                }

        }
    }
    }
    
    // MARK: - UIButton Actions

    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func cartBtnAction(_ sender: UIButton) {
        let objGetStartedViewC = Storyboard.Cart.instantiateViewController(identifier: CartViewC.className)
        { coder in
            CartViewC(coder: coder)
        }
        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }

}

// MARK: - collectionView, collectionView

extension WishlistViewC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{

    // MARK: - UICollectionViewDelegate

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        fLog()
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }

    // MARK: - UICollectionViewDataSource

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return  self.productsArr.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell: SubCategoryCollectionViewCell =
            collectionView.dequeueReusableCell(forIndexPath: indexPath)

        cell.configureCell(self.productsArr, indexPath.row)
        cell.delegate = self
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        let collectionViewSize = self.collectionView.frame.size.width - 15

        return CGSize(width: collectionViewSize/2 + 3, height: 300)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 2, bottom: 0, right: 2)
    }


}

extension WishlistViewC :  SubCategoryCollectionViewCellDelegate {
    func tapWishlist(cell: SubCategoryCollectionViewCell, index: Int) {
        let obj = productsArr[index]
        if obj.wishlist_id != "0" {
            APIManager.apiForRemoveWishlist(whislist: obj.wishlist_id, controller: self.navigationController!) { status in
                if status {
                    self.productsArr.remove(at: index)
                    self.collectionView.reloadData()

                }
            }
        }
      

    }
}
