//
//  ShopByCategoryViewC.swift
//  Insta Sale
//
//  Created by Sajiya on 04/10/21.
//

import UIKit

class ShopByCategoryViewC: BaseViewC {
    
    // MARK: - IBOutlets

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Properties
    var subCategoriesArr = [CategoriesResponse]()
    var subCatUrl = ""
    var id = 0

    var categoryName = ""
    // MARK: - View Life Cycle Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    private func initialSetup() {
        self.titleLabel.text = self.categoryName
        self.collectionView.registerMultiple(nibs: [ShopByCategoryCollectionViewCell.className])
        APIManager.apiForGetShopCategoryData(id: "\(id)", controller: self.navigationController!) { (status, subCategoriesArr,subCatUrl) in
            if status {
                self.subCategoriesArr.removeAll()
                self.subCategoriesArr += CategoriesResponse.getSubCategoriesArray(responseArray: subCategoriesArr as! Array<Dictionary<String, Any>>)
                self.subCatUrl = subCatUrl
                self.collectionView.reloadData()

            }
        }
    }
    
    // MARK: - UIButton Actions

    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
 
}

// MARK: - collectionView, collectionView

extension ShopByCategoryViewC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        fLog()
        let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: SubCategoryViewC.className)
        { coder in
            SubCategoryViewC(coder: coder)
        }
        objGetStartedViewC.categoryName = self.subCategoriesArr[indexPath.row].name
        objGetStartedViewC.id = self.subCategoriesArr[indexPath.row].id

        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.subCategoriesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: ShopByCategoryCollectionViewCell =
            collectionView.dequeueReusableCell(forIndexPath: indexPath)
        
        cell.configureCell(self.subCategoriesArr, indexPath.row, subCatUrl)
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewSize = self.collectionView.frame.width/3.0 - 8
        
        return CGSize(width: collectionViewSize, height: 130)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 2, left: 2, bottom: 2, right: 2)
    }
    
    
}
