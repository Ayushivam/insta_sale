//
//  ProductDetailViewC.swift
//  Insta Sale
//
//  Created by Sajiya on 16/10/21.
//

import UIKit
import Cosmos

class ProductDetailViewC: BaseViewC {


    // MARK: - IBOutlets

    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var discountPriceLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var productImagesCollectionView: UICollectionView!
    @IBOutlet weak var productName: UILabel!
    // MARK: - Properties
    var id = 0
    var relatedProductArr = [CategoriesResponse]()
    var comboProductArr = [CategoriesResponse]()
    var productsFilterArr = [CategoriesResponse]()
    var reviewArr = [CategoriesResponse]()

    var productsDescription = ""

    // MARK: - View Life Cycle Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        classStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        classEnd()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        classReleased()
    }
    
    // MARK: - Private Functions
    private func initialSetup() {
        self.productImagesCollectionView.registerMultiple(nibs: [CategoryBannerCollectionViewCell.className])
        self.tableView.registerMultiple(nibs: [BrandsOptionsTableViewCell.className,ProductDescTableViewCell.className,ReviewsTableViewCell.className,ComboTableViewCell.className,HomeTableViewCell.className])
        APIManager.apiForProductDetail(id: "\(self.id)", controller: self.navigationController!) { status, relatedArr, produtsDetails in
            if status {
                self.relatedProductArr.removeAll()
                self.relatedProductArr += CategoriesResponse.getProductsArray(responseArray: relatedArr as! Array<Dictionary<String, Any>>)
                self.productName.text = produtsDetails.validatedValue("product_name", expected: "" as AnyObject) as? String
                
                let description = produtsDetails.validatedValue("description", expected: "" as AnyObject) as! String
                let data = description.data(using: String.Encoding.unicode)!
                let attrStr = try? NSAttributedString(
                    data: data,
                    options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                    documentAttributes: nil)
                self.descriptionLabel.text = (attrStr?.string)!
                self.productsDescription = (attrStr?.string)!
                                
                let discountPrice = produtsDetails.validatedValue("discounted_price", expected: 0 as AnyObject) as? Int
                
                if discountPrice == nil {
                    self.discountPriceLabel.text =  "$0"

                }
                else {
                    self.discountPriceLabel.text =  "$" + "\(discountPrice ?? 0)"

                }

                let price = produtsDetails.validatedValue("price", expected: 0 as AnyObject) as? Int
                if price == nil {
                    self.priceLabel.text =  "$0"

                }
                else {
                    self.priceLabel.text =  "$" + "\(price ?? 0)"

                }
                
                self.ratingView.rating = 5

                self.productsFilterArr.removeAll()
                let array : Array  = produtsDetails.validatedValue("product_filter", expected: [] as AnyObject) as! Array<AnyObject>
                self.productsFilterArr += CategoriesResponse.getOptionFilterArray(responseArray: array as! Array<Dictionary<String, Any>>)
                var idArr = Array<String>()

                for i in self.productsFilterArr{
                    for j in i.filterOptionArr {
                        idArr.append("\(j.option_id)")
                    }
                    
                    let string = idArr.joined(separator: ",")
                    print(string)
                //    let arr = idArr.components(separatedBy: ",")
                 //   let formattedArray = (arr.map{Int($0)}).joined(separator: ",")

                }
            
                let vID = produtsDetails.validatedValue("product_variation_id", expected: 0 as AnyObject) as? Int


                APIManager.apiForGetReviews(id: "\(self.id)", vId: "\(vID ?? 0)", controller: self.navigationController!) { status, reviewArr in
                    if status {
                      self.reviewArr.removeAll()
                       self.reviewArr += CategoriesResponse.getReviewsArray(responseArray: reviewArr as! Array<Dictionary<String, Any>>)
                        self.tableView.reloadData()

                    }
                }
                self.tableView.reloadData()

            }
        }
        
        APIManager.apiForComboProduct(id: "\(self.id)", controller: self.navigationController!) { status, relatedArr in
            if status {
               self.comboProductArr.removeAll()
               self.comboProductArr += CategoriesResponse.getComboProductsArray(responseArray: relatedArr as! Array<Dictionary<String, Any>>)
                self.tableView.reloadData()

            }
        }
        
        
        tableView.estimatedRowHeight = 80

        self.productImagesCollectionView.reloadData()
    }
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func wishListBtnAction(_ sender: UIButton) {
        let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: WishlistViewC.className)
        { coder in
            WishlistViewC(coder: coder)
        }
        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
    
    @IBAction func cartBtnAction(_ sender: UIButton) {
        let objGetStartedViewC = Storyboard.Cart.instantiateViewController(identifier: CartViewC.className)
        { coder in
            CartViewC(coder: coder)
        }
        self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
    }
}


// MARK: - collectionView, collectionView

extension ProductDetailViewC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        fLog()
    }
    
   // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell: CategoryBannerCollectionViewCell =
                collectionView.dequeueReusableCell(forIndexPath: indexPath)
        cell.pageController.drawer = WormDrawer(numberOfPages: 3, height: 8, width: 8, space: 10, currentItem: CGFloat(indexPath.row))
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize.init(width: collectionView.frame.size.width , height: collectionView.frame.size.height)
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension ProductDetailViewC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - UITableViewDelegate
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 65

        }
        else if indexPath.section == 1 {
            return UITableView.automaticDimension
        }
        else if indexPath.section == 2 {
            return 135
        }
        else if indexPath.section == 3 {
            let count = comboProductArr.count
            if count == 0 {
                return 0
            }
            else {
                return CGFloat(165 + count * 150)

            }
        }
        else {
            if self.relatedProductArr.count == 0 {return 0}
            else {return 250}
            
        }

    }
    
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fLog()
         if indexPath.section == 2 {
             let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: ReviewNRatingViewC.className)
             { coder in
                 ReviewNRatingViewC(coder: coder)
             }
             objGetStartedViewC.reviewArr = self.reviewArr
             self.navigationController?.pushViewController(objGetStartedViewC, animated: true)
        }
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
            return 5
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return productsFilterArr.count
        }
        else if section == 1 {
            return 5
        }
        else if section == 2 {
            return self.reviewArr.count
        }
        else {
            return 1
        }
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.section == 0 {
            let cell: BrandsOptionsTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureCell(self.productsFilterArr, indexPath.row)
            
            return cell
            
        }
        else if indexPath.section == 1 {
            let cell: ProductDescTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            if indexPath.row  == 0 {
                cell.descriptionLabel.text = self.productsDescription
                cell.mainHeadingLabel.text = "Product Details"

            }
            else  if indexPath.row  == 1 {
                cell.descriptionLabel.text = "You can return the product within 30 days if the delivered product is damages, expire, different from ordered."
                cell.mainHeadingLabel.text = "Hassle Free 30 Days Return"

            }
            else  if indexPath.row  == 2 {
                cell.descriptionLabel.text = "Delivery: 1-3 Business Day\n\nCarriers: USPS/UPS/Fedex\n\nCost: Free/Variant By carrier."
                cell.mainHeadingLabel.text = "Delivery & Services For"

            }
            else  if indexPath.row  == 3 {
                cell.descriptionLabel.text = "Always Enjoy fast and reliable shipping when you buy your products from FlazhSale. Our ordering and shipping services are among the best in the industry due to short processing times and a variety of shipping methods. Your orders are packaged at one of the many local warehouses across the country, minimizing delivery times and ensuring you get your purchases as soon as possible."
                cell.mainHeadingLabel.text = "Shipping Info"

            }
            
            else {
                cell.descriptionLabel.text = "FlazhSale by Smart Geeks LLC has partnered with PayPal to offer you the very best in payment services. PayPal allows you to buy with confidence, knowing your payments are processed quickly and securely with state-of-the-art encryption technology."
                cell.mainHeadingLabel.text = "Payments Options: Paypal, Debit/Credit"

            }
            return cell
            
        }
        
        else if indexPath.section == 2 {
            let cell: ReviewsTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureCell(self.reviewArr,indexPath.row,false)
            return cell
            
        }
        else if indexPath.section == 3 {
            let cell: ComboTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.configureCell(self.comboProductArr, indexPath.row)
            cell.contentView.backgroundColor = #colorLiteral(red: 0.9576483369, green: 0.9528413415, blue: 0.9441305995, alpha: 1)
           
            return cell
            
        }
        else {
                       let cell: HomeTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
           cell.configureCell(self.relatedProductArr, indexPath.row, true)
            return cell
        }

       
    }
    
}
