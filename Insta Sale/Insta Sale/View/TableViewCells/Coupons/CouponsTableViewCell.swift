//
//  CouponsTableViewCell.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 12/11/21.
//

import UIKit

class CouponsTableViewCell: BaseTableViewCell,ReusableView {
    
    @IBOutlet weak var discoutLabel: UILabel!
    @IBOutlet weak var expiryDate: UILabel!
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var offLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        offLabel.text = "%\nOFF"
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // MARK: - Internal Functions
        internal func configureCell(_ couponArr: [CategoriesResponse], _ index: Int) {
            self.code.text = couponArr[index].code
            self.title.text = "On minimum purchase of $" + "\(couponArr[index].minimum_order)"
            self.expiryDate.text = couponArr[index].valid_till
            self.discoutLabel.text = "\(couponArr[index].discount_Coupon)"


    }
    //MARK: - UI BUTTON ACTIONS
    
    @IBAction func copyBtnAction(_ sender: UIButton) {
        UIPasteboard.general.string = code.text
        AlertControllerManager.showToast(message: "Code copied", type: .success)
    }
}
