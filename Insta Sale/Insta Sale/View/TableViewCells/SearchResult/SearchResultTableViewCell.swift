//
//  SearchResultTableViewCell.swift
//  Insta Sale
//
//  Created by Sajiya on 17/10/21.
//

import UIKit

class SearchResultTableViewCell: BaseTableViewCell,ReusableView {

    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var suggestionName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Private Functions
    
    private func initialSetup() {
    
}
// MARK: - Internal Functions

    internal func configureCell(_ searchArr: [CategoriesResponse], _ index: Int) {
        self.suggestionName.text = searchArr[index].name
        self.category.text = searchArr[index].subcategory


      
    }
    
}
