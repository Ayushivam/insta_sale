//
//  PropertyTableViewCell.swift
//  Insta Sale
//
//  Created by Sajiya on 18/10/21.
//

import UIKit

class PropertyTableViewCell: BaseTableViewCell, ReusableView {

    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var name: UILabel!
    var index = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Internal Functions

    internal func configureCell(_ productFilterArr: [CategoriesResponse], _ index: Int) {
      
        self.index = index
        self.name.text = productFilterArr[index].name
    }
}
