//
//  HomeTableViewCell.swift
//  Insta Sale
//
//  Created by Sajiya on 06/10/21.
//

import UIKit
protocol HomeTableViewCellDelegate: AnyObject {
    
    func tapSelectItem(cell: HomeTableViewCell, id: Int)
}

class HomeTableViewCell: BaseTableViewCell , ReusableView {
    
    // MARK: - IBOutlets
  
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headingLabel: UILabel!
    
    // MARK: - Properties
    var productsArr = [CategoriesResponse]()
    internal weak var delegate: HomeTableViewCellDelegate?

    
    // MARK: - View Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        classStart()
        self.initialSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Private Functions
    
    private func initialSetup() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.registerMultiple(nibs: [BestOffersCollectionViewCell.className])
        self.collectionView.reloadData()
}
// MARK: - Internal Functions

    internal func configureCell(_ featuredArr: [CategoriesResponse], _ index: Int, _ isFromProductDetail:Bool) {
        if isFromProductDetail == false {
            self.headingLabel.text = featuredArr[index].name
            self.productsArr = featuredArr[index].products
            self.moreBtn.isHidden = false
        }
        else {
            self.headingLabel.text = "Related Products"
            self.productsArr = featuredArr
            self.moreBtn.isHidden = true
        }
      
        self.collectionView.reloadData()
    }
}

// MARK: - collectionView, collectionView

extension HomeTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        fLog()
        self.delegate?.tapSelectItem(cell: self, id: self.productsArr[indexPath.row].id)
            
        
    }
    
   // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      
            let cell: BestOffersCollectionViewCell =
                collectionView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureCell(self.productsArr, indexPath.row)
            return cell

    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
     
            return CGSize.init(width:145 , height: 220)
        
     
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
       
        return UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 1)

      
    }
}

