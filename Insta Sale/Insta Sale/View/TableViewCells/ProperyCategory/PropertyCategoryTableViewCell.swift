//
//  PropertyCategoryTableViewCell.swift
//  Insta Sale
//
//  Created by Sajiya on 18/10/21.
//

import UIKit

class PropertyCategoryTableViewCell: BaseTableViewCell,ReusableView {

    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Internal Functions

    internal func configureCell(_ productFilterArr: [CategoriesResponse], _ index: Int) {
        self.name.text = productFilterArr[index].name
        checkImageView.image = productFilterArr[index].itemSelectable == true ? #imageLiteral(resourceName: "check-box") : #imageLiteral(resourceName: "stop")

      
        
    }

}
