//
//  CategoryTableCell.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 12/11/21.
//

import UIKit

class CategoryTableCell: BaseTableViewCell,ReusableView {
    @IBOutlet weak var catImageView: UIImageView!
    
    @IBOutlet weak var bgViewWidth: NSLayoutConstraint!
    @IBOutlet weak var bgViewHeight: NSLayoutConstraint!
    // MARK: - View Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        classStart()
        self.initialSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Private Functions
    
    private func initialSetup() {
       
    
}
// MARK: - Internal Functions

    internal func configureCell(_ catArr: [CategoriesResponse], _ index: Int, _ catUrl : String) {
        self.catImageView.sd_setImage(with: URL(string: catUrl + catArr[index].imgCateg), placeholderImage: #imageLiteral(resourceName: "logo_main"), options: SDWebImageOptions(rawValue: 0))
    }
    
}
