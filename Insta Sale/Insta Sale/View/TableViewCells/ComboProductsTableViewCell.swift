//
//  ComboProductsTableViewCell.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 24/10/21.
//

import UIKit

class ComboProductsTableViewCell: BaseTableViewCell, ReusableView {
    var comboArr = [CategoriesResponse]()
    @IBOutlet weak var addImageView: UIImageView!
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var discountPrice: UILabel!
    @IBOutlet weak var imagView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Private Functions
    
    private func initialSetup() {
        
}
    
    internal func configureCell(_ comboArr: [CategoriesResponse], _ index: Int) {
        self.comboArr = comboArr
        self.nameLabel.text = self.comboArr[index].name
        self.imagView.sd_setImage(with: URL(string: comboArr[index].imgCateg), placeholderImage: #imageLiteral(resourceName: "logo_main"), options: SDWebImageOptions(rawValue: 0))
        self.Price.text =  "$" + "\(comboArr[index].price)"
        self.discountPrice.text = "$" + "\(comboArr[index].discounted_price)"
        let lastElement = comboArr.count - 1

        if index == lastElement {
            addImageView.isHidden = true
        }
        else {
            addImageView.isHidden = false

        }

    }
    
}
