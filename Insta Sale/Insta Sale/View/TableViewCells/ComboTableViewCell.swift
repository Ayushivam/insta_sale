//
//  ComboTableViewCell.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 24/10/21.
//

import UIKit

class ComboTableViewCell: BaseTableViewCell,ReusableView {

    @IBOutlet weak var tableView: UITableView!
    var comboProductArr = [CategoriesResponse]()
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Private Functions
    
    private func initialSetup() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.registerMultiple(nibs: [ComboProductsTableViewCell.className])
        self.tableView.reloadData()
}
    
    internal func configureCell(_ comboArr: [CategoriesResponse], _ index: Int) {
        self.comboProductArr = comboArr
        self.tableView.reloadData()


    }
    
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension ComboTableViewCell: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - UITableViewDelegate
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fLog()
     
       
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comboProductArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ComboProductsTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        
        cell.configureCell(self.comboProductArr, indexPath.row)
       // cell.delegate = self

        return cell
    }
    
}
