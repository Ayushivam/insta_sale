//
//  NotificationTableViewCell.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 12/11/21.
//

import UIKit

class NotificationTableViewCell: BaseTableViewCell, ReusableView {

    // MARK: - IBOutlets
  
    @IBOutlet weak var bgView: UIView!
    //    @IBOutlet weak var headingLabel: UILabel!
//
//    @IBOutlet weak var imgImageView: UIImageView!
    // MARK: - Properties

    
    // MARK: - View Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        classStart()
        self.initialSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Private Functions
    
    private func initialSetup() {
        bgView.backgroundColor = UIColor.white
        bgView.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        bgView.layer.shadowOpacity = 2.5
        bgView.layer.shadowOffset = CGSize.zero
        bgView.layer.shadowRadius = 2.5
    
}
// MARK: - Internal Functions

    internal func configureCell() {
      
    }
}
