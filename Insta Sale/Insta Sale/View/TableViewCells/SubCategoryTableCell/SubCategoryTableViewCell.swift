//
//  SubCategoryTableViewCell.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 12/11/21.
//

import UIKit

class SubCategoryTableViewCell: BaseTableViewCell,ReusableView {
    
    @IBOutlet weak var subCatNameLabel: UILabel!
    // MARK: - View Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        classStart()
        self.initialSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Private Functions
    
    private func initialSetup() {
       
    
}
// MARK: - Internal Functions
    internal func configureCell(_ subCatArr: [CategoriesResponse], _ index: Int, _ subCatUrl : String) { self.subCatNameLabel.text = subCatArr[index].name
    
}

}
