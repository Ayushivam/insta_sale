//
//  SideMenuCell.swift
//  Insta Sale
//
//  Created by Sajiya on 16/10/21.
//

import UIKit

class SideMenuCell: BaseTableViewCell , ReusableView {
    
    // MARK: - IBOutlets
  
    @IBOutlet weak var headingLabel: UILabel!
    
    @IBOutlet weak var imgImageView: UIImageView!
    // MARK: - Properties

    
    // MARK: - View Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        classStart()
        self.initialSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Private Functions
    
    private func initialSetup() {
    
}
// MARK: - Internal Functions

    internal func configureCell() {
      
    }
}

    

