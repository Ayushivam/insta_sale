//
//  BaseTableViewCell.swift
//  IOSAppTemplate
//
//  Created by Ayushi   on 12/04/21.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets

    // MARK: - Properties

    // MARK: - View Life Cycle Functions ::

    override func awakeFromNib() {
        super.awakeFromNib()

        self.selectionStyle = .none
        self.accessoryType = .none
        self.contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        
        self.initialSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Private Functions
    
    private func initialSetup() {
    }
    
    // MARK: - Internal Functions
    
    //internal func reloadCell() {
    //}
        
    // MARK: - UIButton Actions
}
