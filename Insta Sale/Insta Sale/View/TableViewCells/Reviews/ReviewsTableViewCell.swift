//
//  ReviewsTableViewCell.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 24/10/21.
//

import UIKit

class ReviewsTableViewCell: BaseTableViewCell,ReusableView{

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var topConstraints: NSLayoutConstraint!
    @IBOutlet weak var viewReviewsBtn: UIButton!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
     
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    internal func configureCell(_ reviewArr: [CategoriesResponse], _ index: Int, _ isFromRNR: Bool) {
        if isFromRNR == true {
            self.heading.isHidden = true
            self.viewReviewsBtn.isHidden = true
            self.topConstraints.constant = 1
            self.mainView.backgroundColor = UIColor.clear
            self.name.text = reviewArr[index].name
            self.time.text = reviewArr[index].created_at
            self.comment.text = reviewArr[index].comment
            self.img.sd_setImage(with: URL(string: reviewArr[index].imgCateg), placeholderImage: #imageLiteral(resourceName: "right (1)"), options: SDWebImageOptions(rawValue: 0))

        }
        else {
            self.heading.isHidden = false
            self.viewReviewsBtn.isHidden = false
            self.topConstraints.constant = 31
            bgView.backgroundColor = UIColor.white
            bgView.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            bgView.layer.shadowOpacity = 2.5
            bgView.layer.shadowOffset = CGSize.zero
            bgView.layer.shadowRadius = 2.5
            self.mainView.backgroundColor = UIColor.white
            self.heading.text = "Customer Reviews(" + "\(reviewArr.count)" + ")"
            self.name.text = reviewArr.first?.name
            self.time.text = reviewArr.first?.created_at
            self.comment.text = reviewArr.first?.comment
            self.img.sd_setImage(with: URL(string: reviewArr.first!.imgCateg), placeholderImage: #imageLiteral(resourceName: "right (1)"), options: SDWebImageOptions(rawValue: 0)) }


        }
 
}
