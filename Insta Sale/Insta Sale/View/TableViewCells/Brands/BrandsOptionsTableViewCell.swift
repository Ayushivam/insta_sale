//
//  BrandsOptionsTableViewCell.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 24/10/21.
//

import UIKit

class BrandsOptionsTableViewCell: BaseTableViewCell, ReusableView {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var filterOptions: UILabel!
    
    var catArr = [CategoriesResponse]()
    var arr = [CategoriesResponse]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // MARK: - Private Functions
    
    private func initialSetup() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.registerMultiple(nibs: [BrandsCollectionViewCell.className])
    }
    
    internal func configureCell(_ optionFilterArr: [CategoriesResponse], _ index: Int) {
        self.filterOptions.text  = optionFilterArr[index].name
        self.catArr = optionFilterArr[index].filterOptionArr
        self.arr = optionFilterArr
        self.collectionView.reloadData()
    }
    
}

// MARK: - collectionView, collectionView

extension BrandsOptionsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        fLog()
    }
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.catArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: BrandsCollectionViewCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configureCell(self.catArr, self.arr,indexPath.row)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                return CGSize(width: catArr[indexPath.row].name.size(withAttributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 11)]).width + 10, height: 38)
    }
    
    
}

