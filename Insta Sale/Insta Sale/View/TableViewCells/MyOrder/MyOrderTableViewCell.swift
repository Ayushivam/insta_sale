//
//  MyOrderTableViewCell.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 12/11/21.
//

import UIKit

class MyOrderTableViewCell: BaseTableViewCell, ReusableView {

    @IBOutlet weak var exchangeReturn: UILabel!
    @IBOutlet weak var productSize: UILabel!
    @IBOutlet weak var productDes: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var status: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    internal func configureCell(_ productArr: [CategoriesResponse], _ index: Int) {
        let arr = productArr[index].myCart
        self.status.text = productArr[index].order_status
        self.date.text = productArr[index].order_status_dt
        self.productImg.sd_setImage(with: URL(string: arr[index].imgCateg), placeholderImage: #imageLiteral(resourceName: "logo_main"), options: SDWebImageOptions(rawValue: 0))
        self.productName.text = arr[index].name
        self.productDes.text = arr[index].productDes

    }
    
}
