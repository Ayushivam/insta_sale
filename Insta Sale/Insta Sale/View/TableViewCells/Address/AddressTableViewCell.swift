//
//  AddressTableViewCell.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 12/25/21.
//

import UIKit

protocol AddressTableViewCellDelegate: AnyObject {
    
    func tapRemove(cell: AddressTableViewCell, index: Int, addressId: Int)
    func tapEdit(cell: AddressTableViewCell, index: Int, address :CategoriesResponse )

}


class AddressTableViewCell: BaseTableViewCell, ReusableView {
    
    @IBOutlet weak var addressType: UILabel!
    @IBOutlet weak var locData: UILabel!
    internal weak var delegate : AddressTableViewCellDelegate?
    var index = 0
    var addressArr = [CategoriesResponse]()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    internal func configureCell(_ addressArr: [CategoriesResponse],_ index: Int) {
        self.addressArr = addressArr
        let nameAddress =  "\(addressArr[index].name) \n\n" + "\(addressArr[index].addressLoc)"
        let LocCity = ", " + "\(addressArr[index].locality_town) \n" + "\(addressArr[index].city_district)"
        let pinCodeState = ", " + "\(addressArr[index].pincode) \n" + "\(addressArr[index].stateLoc) \n"
        let numberLoc = "\(addressArr[index].mobile_noLoc)"

        self.locData.text = nameAddress + LocCity + pinCodeState + numberLoc
        self.index = index

        //self.addressType.text = addressArr[index].address_type
    }
    
    @IBAction func removeAddressBtnAction(_ sender: UIButton) {
        AlertControllerManager.showAlert(title: "", message: "Do you want to delete ?", buttons: [Text.Cancel.localized, Text.Delete.localized]) { (index) in
            dLog(message: "Selected Index :: \(index)")

            if index == 1 {
                self.delegate?.tapRemove(cell: self, index: self.index, addressId: self.addressArr[self.index].id)
            }
        }

    }
    
    @IBAction func editButtonAction(_ sender: UIButton) {
        self.delegate?.tapEdit(cell: self, index: self.index, address: self.addressArr[self.index])
    }
    
}
