//
//  ProductDescTableViewCell.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 24/10/21.
//

import UIKit

class ProductDescTableViewCell: BaseTableViewCell,ReusableView {

    @IBOutlet weak var mainHeadingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
