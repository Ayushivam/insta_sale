//
//  BestOffersCollectionViewCell.swift
//  Insta Sale
//
//  Created by Sajiya on 27/09/21.
//

import UIKit

class BestOffersCollectionViewCell: BaseCollectionViewCell, ReusableView {
    
    // MARK: - IBOutlets
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var discountPrice: UILabel!
    @IBOutlet weak var productDesLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    // MARK: - Properties
    
    // MARK: - View Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        classStart()
    }
    
    // MARK: - Private Functions
    
    // MARK: - Internal Functions
    
    internal func configureCell(_ productArr: [CategoriesResponse], _ index: Int) {
        self.productNameLabel.text = productArr[index].name
        self.productDesLabel.text = productArr[index].productDes
        self.Price.text =  "$" + "\(productArr[index].price)"
        self.discountPrice.text = "$" + "\(productArr[index].discounted_price)"

        self.productImageView.sd_setImage(with: URL(string: productArr[index].imgCateg), placeholderImage: #imageLiteral(resourceName: "logo_main"), options: SDWebImageOptions(rawValue: 0))
    }

}
