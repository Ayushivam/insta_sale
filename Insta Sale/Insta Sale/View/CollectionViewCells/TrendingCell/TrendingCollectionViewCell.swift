//
//  TrendingCollectionViewCell.swift
//  Insta Sale
//
//  Created by Sajiya on 27/09/21.
//

import UIKit

class TrendingCollectionViewCell: BaseCollectionViewCell, ReusableView {
    
    // MARK: - IBOutlets
    
    // MARK: - Properties
    
    // MARK: - View Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        classStart()
    }
    
    // MARK: - Private Functions
    
    // MARK: - Internal Functions
    
    internal func configureCell() {
      
    }

}
