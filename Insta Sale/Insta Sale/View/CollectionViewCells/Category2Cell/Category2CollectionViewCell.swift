//
//  Category2CollectionViewCell.swift
//  Insta Sale
//
//  Created by Sajiya on 27/09/21.
//

import UIKit

class Category2CollectionViewCell: BaseCollectionViewCell, ReusableView {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var subCatImageView: UIImageView!
    @IBOutlet weak var subCatNameLabel: UILabel!
    // MARK: - Properties
    
    // MARK: - View Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.backgroundColor = UIColor.white
        bgView.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        bgView.layer.shadowOpacity = 2.5
        bgView.layer.shadowOffset = CGSize.zero
        bgView.layer.shadowRadius = 2.5
        classStart()
    }
    
    // MARK: - Private Functions
    
    // MARK: - Internal Functions
    
    internal func configureCell(_ subCatArr: [CategoriesResponse], _ index: Int, _ subCatUrl : String) { self.subCatNameLabel.text = subCatArr[index].name
        self.subCatImageView.sd_setImage(with: URL(string: subCatUrl + subCatArr[index].imgCateg), placeholderImage: #imageLiteral(resourceName: "logo_main"), options: SDWebImageOptions(rawValue: 0)) }

}
