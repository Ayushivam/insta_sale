//
//  CategoryCollectionViewCell.swift
//  Insta Sale
//
//  Created by Sajiya on 27/09/21.
//

import UIKit

class CategoryCollectionViewCell: BaseCollectionViewCell, ReusableView {
    @IBOutlet weak var catImageView: UIImageView!
    @IBOutlet weak var catNameLabel: UILabel!
    // MARK: - IBOutlets
    
    // MARK: - Properties
    
    // MARK: - View Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        classStart()
    }
    
    // MARK: - Private Functions
    
    // MARK: - Internal Functions
    
    internal func configureCell(_ catArr: [CategoriesResponse], _ index: Int, _ catUrl : String) {
        self.catNameLabel.text = catArr[index].name
        self.catImageView.sd_setImage(with: URL(string: catUrl + catArr[index].imgCateg), placeholderImage: #imageLiteral(resourceName: "logo_main"), options: SDWebImageOptions(rawValue: 0))
    }

}
