//
//  ShopByCategoryCollectionViewCell.swift
//  Insta Sale
//
//  Created by Sajiya on 05/10/21.
//

import UIKit

class ShopByCategoryCollectionViewCell: BaseCollectionViewCell, ReusableView {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var subNameLabel: UILabel!
    @IBOutlet weak var subCatImageView: UIImageView!
    // MARK: - IBOutlets
    
    // MARK: - Properties
    
    // MARK: - View Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.shadowColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        mainView.layer.shadowOpacity = 2.5
        mainView.layer.shadowOffset = CGSize.zero
        mainView.layer.shadowRadius = 2.5
        classStart()
    }
    
    // MARK: - Private Functions
    
    // MARK: - Internal Functions
    
    internal func configureCell(_ subCatArr: [CategoriesResponse], _ index: Int, _ subCatUrl : String) { self.subNameLabel.text = subCatArr[index].name
        self.subCatImageView.sd_setImage(with: URL(string: subCatUrl + subCatArr[index].imgCateg), placeholderImage: #imageLiteral(resourceName: "logo_main"), options: SDWebImageOptions(rawValue: 0))
        
    }

    

}
