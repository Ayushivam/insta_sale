//
//  SubCategoryCollectionViewCell.swift
//  Insta Sale
//
//  Created by Sajiya on 05/10/21.
//

import UIKit

protocol SubCategoryCollectionViewCellDelegate: AnyObject {
    
    func tapWishlist(cell: SubCategoryCollectionViewCell, index: Int)
}


class SubCategoryCollectionViewCell: BaseCollectionViewCell, ReusableView {
    // MARK: - IBOutlets

    @IBOutlet weak var wishListImageView: UIImageView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var discountPrice: UILabel!
    @IBOutlet weak var productDesLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    
    // MARK: - Properties
    internal weak var delegate : SubCategoryCollectionViewCellDelegate?
    var index = 0
    var productArr =  [CategoriesResponse]()
    // MARK: - View Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        classStart()
    }
    
    // MARK: - Private Functions
    
    // MARK: - Internal Functions
    
    internal func configureCell(_ productArr: [CategoriesResponse], _ index: Int) {
        self.index = index
        
        self.productNameLabel.text = productArr[index].name
        self.productDesLabel.text = productArr[index].productDes
        self.Price.text =  "$" + "\(productArr[index].price)"
        self.discountPrice.text = "$" + "\(productArr[index].discounted_price)"
 
        self.productImageView.sd_setImage(with: URL(string: productArr[index].imgCateg), placeholderImage: #imageLiteral(resourceName: "logo_main"), options: SDWebImageOptions(rawValue: 0))
        
        if productArr[index].wishlist_id == "0" {
            self.wishListImageView.image =  #imageLiteral(resourceName: "heart (1)")
        }
        else {
            self.wishListImageView.image = #imageLiteral(resourceName: "heart (3)")
        }
      
    }

    @IBAction func wishlistBtnAction(_ sender: UIButton) {
        AlertControllerManager.showAlert(title: "", message: "Do you want to delete ?", buttons: [Text.Cancel.localized, Text.Delete.localized]) { (index) in
            dLog(message: "Selected Index :: \(index)")

            if index == 1 {
                self.delegate?.tapWishlist(cell: self, index: self.index)
            }
        }
        
    }
}
