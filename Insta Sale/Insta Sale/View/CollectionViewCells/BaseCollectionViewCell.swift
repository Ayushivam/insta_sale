//
//  BaseCollectionViewCell.swift
//  IOSAppTemplate
//
//  Created by Ayushi   on 12/04/21.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    
    // MARK: - Properties
    
    // MARK: - View Life Cycle Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.initialSetup()
    }
    
    // MARK: - Private Functions
    
    private func initialSetup() {
    }
    
    // MARK: - Internal Functions
    
    internal func reloadCell() {
    }
    
}
