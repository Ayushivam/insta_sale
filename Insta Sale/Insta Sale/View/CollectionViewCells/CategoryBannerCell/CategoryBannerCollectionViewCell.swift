//
//  CategoryBannerCollectionViewCell.swift
//  Insta Sale
//
//  Created by Sajiya on 27/09/21.
//

import UIKit

class CategoryBannerCollectionViewCell: BaseCollectionViewCell, ReusableView {
    
    // MARK: - IBOutlets
    @IBOutlet weak var pageController: AdvancedPageControlView!
    @IBOutlet weak var bannerImageView: UIImageView!
    
    // MARK: - Properties
    
    // MARK: - View Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        classStart()
    }
    
    // MARK: - Private Functions
    
    // MARK: - Internal Functions
    
    internal func configureCell(_ bannerCatArr: [CategoriesResponse], _ index: Int, _ bannerUrl : String) {
        self.bannerImageView.sd_setImage(with: URL(string: bannerUrl + bannerCatArr[index].imgCateg), placeholderImage: #imageLiteral(resourceName: "right (1)"), options: SDWebImageOptions(rawValue: 0)) }

    

}
