//
//  IntroductionCollectionViewCell.swift
//  Insta Sale
//
//  Created by Sajiya on 22/09/21.
//

import UIKit

class IntroductionCollectionViewCell: BaseCollectionViewCell, ReusableView {

    // MARK: - IBOutlets
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pageController: UIPageControl!
    
    // MARK: - Properties
    
    // MARK: - View Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        classStart()
    }
    
    // MARK: - Private Functions
    
    // MARK: - Internal Functions
    
    internal func configureCell() {
      
    }

}
