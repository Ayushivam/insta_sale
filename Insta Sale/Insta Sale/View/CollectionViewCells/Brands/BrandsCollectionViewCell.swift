//
//  BrandsCollectionViewCell.swift
//  Insta Sale
//
//  Created by Mobcoder Technologies Private Limited on 22/11/21.
//

import UIKit

class BrandsCollectionViewCell: BaseCollectionViewCell ,ReusableView{

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var category: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    internal func configureCell(_ optionFilterArr: [CategoriesResponse], _ FilterArr: [CategoriesResponse],_ index: Int) {
        self.category.text  = optionFilterArr[index].name
       
    }

}


extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
}
