//
//  SearchTableViewCell.swift
//  Insta Sale
//
//  Created by Sajiya on 17/10/21.
//

import UIKit

class SearchTableViewCell: BaseCollectionViewCell, ReusableView {
    @IBOutlet weak var subNameLabel: UILabel!
    @IBOutlet weak var subCatImageView: UIImageView!
    // MARK: - IBOutlets
    
    // MARK: - Properties
    
    // MARK: - View Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
      
        classStart()
    }
    
    // MARK: - Private Functions
    
    // MARK: - Internal Functions
    
    internal func configureCell(_ subCatArr: [CategoriesResponse], _ index: Int, _ subCatUrl : String) { self.subNameLabel.text = subCatArr[index].name
        self.subCatImageView.sd_setImage(with: URL(string: subCatUrl + subCatArr[index].imgCateg), placeholderImage: #imageLiteral(resourceName: "logo_main"), options: SDWebImageOptions(rawValue: 0))
        
    }

    

}
