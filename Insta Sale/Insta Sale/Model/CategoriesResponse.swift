//
//  CategoriesResponse.swift
//  Insta Sale
//
//  Created by Sajiya on 04/10/21.
//

import Foundation
class CategoriesResponse: NSObject {
    var id = 0
    var rating = 0
    var name  = ""
    var imgCateg = ""
    var thumbnail = ""
    var display_on_home_page = 0
    var redirectID = 0
    var redirectType = ""
    var options = ""
    var productDes = ""
    var price = Double()
    var discounted_price = Double()
    var products = [CategoriesResponse]()
    var wishlist_id = ""
    var prodVariationID = 0
    var category_id = 0
    var category = ""
    var subcategory_id = 0
    var subcategory = ""
    var comment = ""
    var minimum_order = 0
    var discount_Coupon = 0
    var defaultAdd = 0
    var address_type = ""
    var filterStatus = 0
    var created_at = ""
    var productsCategoryArr = [CategoriesResponse]()
    var filterOptionArr = [CategoriesResponse]()
    var myCart = [CategoriesResponse]()
   var order_status = ""
    var filter_id = 0
    var itemSelectable = false
    var idArr = [Int]()
    var product_id = 0
    var product_filterid = 0
    var option_id = 0
    var title = ""
    var code = ""
    var valid_till = ""
    var mobile_noLoc = ""
    var  pincode = ""
    var  addressLoc = ""
    var  locality_town = ""
    var  city_district = ""
    var  stateLoc = ""
    var order_status_dt = ""
    class func getCategoriesArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
        
        for i in 0..<responseArray.count  {
            let item = responseArray[i]
            let modalObj = CategoriesResponse()
            modalObj.id = item.validatedValue("id", expected: 0 as AnyObject) as! Int
            modalObj.name = item.validatedValue("name", expected: "" as AnyObject) as! String
            modalObj.imgCateg = item.validatedValue("img", expected: "" as AnyObject) as! String
            modalObj.thumbnail = item.validatedValue("thumbnail", expected: "" as AnyObject) as! String
            modalObj.display_on_home_page = item.validatedValue("display_on_home_page", expected: 0 as AnyObject) as! Int
         
            modalObj.itemSelectable = i == 0 ? true : false
            
            resultArray.append(modalObj)
        }

        return resultArray
    }
    
    class func getSubCategoriesArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
        for item in responseArray {
            let modalObj = CategoriesResponse()
            
            modalObj.id = item.validatedValue("id", expected: 0 as AnyObject) as! Int
            modalObj.name = item.validatedValue("name", expected: "" as AnyObject) as! String
            modalObj.imgCateg = item.validatedValue("img", expected: "" as AnyObject) as! String
            modalObj.thumbnail = item.validatedValue("thumbnail", expected: "" as AnyObject) as? String ?? ""
            //modalObj.display_on_home_page = item.validatedValue("display_on_home_page", expected: 0 as AnyObject) as? Int ?? 0

            resultArray.append(modalObj)

        }
        return resultArray
    }
    
    class func getBannersArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
        for item in responseArray {
            let modalObj = CategoriesResponse()
            
            modalObj.id = item.validatedValue("id", expected: 0 as AnyObject) as! Int
            modalObj.name = item.validatedValue("name", expected: "" as AnyObject) as! String
            modalObj.imgCateg = item.validatedValue("img", expected: "" as AnyObject) as! String
            modalObj.redirectType = item.validatedValue("redirect_type", expected: "" as AnyObject) as! String
            modalObj.redirectID = item.validatedValue("redirect_to_id", expected: 0 as AnyObject) as! Int

            resultArray.append(modalObj)

        }
        return resultArray
    }
    
    class func getProductsArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
        for item in responseArray {
            let modalObj = CategoriesResponse()
            
            modalObj.id = item.validatedValue("id", expected: 0 as AnyObject) as! Int
            modalObj.options = item.validatedValue("options", expected: "" as AnyObject) as! String
            modalObj.name = item.validatedValue("product_name", expected: "" as AnyObject) as! String
            modalObj.imgCateg = item.validatedValue("img", expected: "" as AnyObject) as! String
            let description = item.validatedValue("description", expected: "" as AnyObject) as! String
            let data = description.data(using: String.Encoding.unicode)!
            let attrStr = try? NSAttributedString(
                data: data,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
            modalObj.productDes = (attrStr?.string)!
            modalObj.discounted_price = item.validatedValue("discounted_price", expected: 0 as AnyObject) as! Double
            modalObj.price = item.validatedValue("price", expected: 0 as AnyObject) as! Double
            modalObj.wishlist_id = item.validatedValue("wishlist_id", expected: "" as AnyObject) as! String
            modalObj.prodVariationID = item.validatedValue("product_variation_id", expected: 0 as AnyObject) as! Int

            resultArray.append(modalObj)

        }
        return resultArray
    }
    
    class func getComboProductsArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
        for item in responseArray {
            let modalObj = CategoriesResponse()
            
            modalObj.id = item.validatedValue("id", expected: 0 as AnyObject) as! Int
            modalObj.options = item.validatedValue("options", expected: "" as AnyObject) as! String
            modalObj.name = item.validatedValue("product_name", expected: "" as AnyObject) as! String
            modalObj.imgCateg = item.validatedValue("img", expected: "" as AnyObject) as! String
            let description = item.validatedValue("description", expected: "" as AnyObject) as! String
            let data = description.data(using: String.Encoding.unicode)!
            let attrStr = try? NSAttributedString(
                data: data,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
            modalObj.productDes = (attrStr?.string)!
            modalObj.discounted_price = item.validatedValue("discounted_price", expected: 0 as AnyObject) as! Double
            modalObj.price = item.validatedValue("price", expected: 0 as AnyObject) as! Double
            modalObj.wishlist_id = item.validatedValue("wishlist_id", expected: "" as AnyObject) as! String
            modalObj.prodVariationID = item.validatedValue("product_variation_id", expected: 0 as AnyObject) as! Int

            resultArray.append(modalObj)

        }
        return resultArray
    }
    
    class func getFeaturedArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
        for item in responseArray {
            let modalObj = CategoriesResponse()
            
            modalObj.id = item.validatedValue("id", expected: 0 as AnyObject) as! Int
            modalObj.name = item.validatedValue("name", expected: "" as AnyObject) as! String
            modalObj.products += CategoriesResponse.getProductsArray(responseArray: item.validatedValue("products", expected: "" as AnyObject) as! Array<AnyObject> as! Array<Dictionary<String, Any>>)
            resultArray.append(modalObj)

        }
        return resultArray
    }
    
    class func getSearchArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
        for item in responseArray {
            let modalObj = CategoriesResponse()
            
            modalObj.id = item.validatedValue("id", expected: 0 as AnyObject) as! Int
            modalObj.name = item.validatedValue("suggestion", expected: "" as AnyObject) as! String
            modalObj.category_id = item.validatedValue("category_id", expected: 0 as AnyObject) as! Int
            modalObj.category = item.validatedValue("category", expected: "" as AnyObject) as! String
            modalObj.subcategory_id = item.validatedValue("subcategory_id", expected: 0 as AnyObject) as! Int
            modalObj.subcategory = item.validatedValue("subcategory", expected: "" as AnyObject) as! String

            resultArray.append(modalObj)

        }
        return resultArray
}
    
    class func getFilterArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
          
        for i in 0..<responseArray.count  {
            let item = responseArray[i]
            let modalObj = CategoriesResponse()
            modalObj.id = item.validatedValue("id", expected: 0 as AnyObject) as! Int
            modalObj.category_id = item.validatedValue("category_id", expected: 0 as AnyObject) as! Int
            modalObj.name = item.validatedValue("name", expected: "" as AnyObject) as! String
            modalObj.subcategory_id = item.validatedValue("subcategory_id", expected: 0 as AnyObject) as! Int
            modalObj.subcategory = item.validatedValue("subcategory", expected: "" as AnyObject) as! String
            modalObj.filterStatus = item.validatedValue("status", expected: 0 as AnyObject) as! Int
            modalObj.created_at = item.validatedValue("created_at", expected: "" as AnyObject) as! String
            modalObj.itemSelectable = i == 0 ? true : false
            modalObj.productsCategoryArr += CategoriesResponse.getProductsCategoryArray(responseArray: item.validatedValue("options", expected: "" as AnyObject) as! Array<AnyObject> as! Array<Dictionary<String, Any>>)
            
            resultArray.append(modalObj)
        }
            
            return resultArray
}
    
    
    class func getProductsCategoryArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
        for i in 0..<responseArray.count  {
            let item = responseArray[i]
            let modalObj = CategoriesResponse()
            modalObj.id = item.validatedValue("id", expected: 0 as AnyObject) as! Int
            modalObj.name = item.validatedValue("name", expected: "" as AnyObject) as! String
            modalObj.filterStatus = item.validatedValue("status", expected: 0 as AnyObject) as! Int
            modalObj.created_at = item.validatedValue("created_at", expected: "" as AnyObject) as! String
            modalObj.filter_id = item.validatedValue("filter_id", expected: 0 as AnyObject) as! Int
            modalObj.itemSelectable =  false
            modalObj.idArr =  []

            resultArray.append(modalObj)
        }
        
        
        return resultArray
}
    
    class func getOptionFilterArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
          
        for i in 0..<responseArray.count  {
            let item = responseArray[i]
            let modalObj = CategoriesResponse()
            modalObj.id = item.validatedValue("id", expected: 0 as AnyObject) as! Int
            modalObj.product_id = item.validatedValue("product_id", expected: 0 as AnyObject) as! Int
            modalObj.name = item.validatedValue("filter", expected: "" as AnyObject) as! String
            modalObj.filterOptionArr += CategoriesResponse.getfilter_optionsArray(responseArray: item.validatedValue("filter_options", expected: "" as AnyObject) as! Array<AnyObject> as! Array<Dictionary<String, Any>>)
            //
            
            modalObj.itemSelectable = i == 0 ? true : false
            
            resultArray.append(modalObj)
        }
            
            return resultArray
}
   
    
    class func getfilter_optionsArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
          
        for i in 0..<responseArray.count  {
            let item = responseArray[i]
            let modalObj = CategoriesResponse()
            modalObj.id = item.validatedValue("id", expected: 0 as AnyObject) as! Int
            modalObj.product_filterid = item.validatedValue("product_filter_id", expected: 0 as AnyObject) as! Int
            modalObj.option_id = item.validatedValue("option_id", expected: 0 as AnyObject) as! Int
            modalObj.name = item.validatedValue("name", expected: "" as AnyObject) as! String
            
            
            resultArray.append(modalObj)
        }
            
            return resultArray
}
    
  //  {"id":36,"product_filter_id":3,"option_id":37,"name":"#000080"}
    
    
    class func getReviewsArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
        for item in responseArray {
            let modalObj = CategoriesResponse()
            
            modalObj.id = item.validatedValue("id", expected: 0 as AnyObject) as! Int
            modalObj.rating = item.validatedValue("rating", expected:  0 as AnyObject) as? Int ?? 0
            modalObj.created_at = item.validatedValue("created_at", expected: "" as AnyObject) as! String
            modalObj.name = item.validatedValue("name", expected: "" as AnyObject) as! String
            modalObj.imgCateg = item.validatedValue("img", expected: "" as AnyObject) as! String
            modalObj.comment = item.validatedValue("message", expected: "" as AnyObject) as! String

            resultArray.append(modalObj)

        }
        return resultArray
    }
    
    
    class func getCouponsArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
        for i in 0..<responseArray.count  {
            let item = responseArray[i]
            let modalObj = CategoriesResponse()
            modalObj.id = item.validatedValue("category_id", expected: 0 as AnyObject) as! Int
            modalObj.code = item.validatedValue("code", expected: "" as AnyObject) as! String
            modalObj.title = item.validatedValue("title", expected: "" as AnyObject) as! String
           modalObj.valid_till = item.validatedValue("valid_till", expected: "" as AnyObject) as! String
            modalObj.discount_Coupon = item.validatedValue("discount", expected: 0 as AnyObject) as! Int
            modalObj.minimum_order = item.validatedValue("minimum_order", expected: 0 as AnyObject) as! Int

            modalObj.itemSelectable =  false
            modalObj.idArr =  []

            resultArray.append(modalObj)
        }
        
        
        return resultArray
}
    
    class func getAddressArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
        for i in 0..<responseArray.count  {
            let item = responseArray[i]
            let modalObj = CategoriesResponse()
            modalObj.id = item.validatedValue("id", expected: 0 as AnyObject) as! Int

            modalObj.defaultAdd = item.validatedValue("default", expected: 0 as AnyObject) as! Int
            modalObj.address_type = item.validatedValue("address_type", expected: "" as AnyObject) as! String
            modalObj.name = item.validatedValue("name", expected: "" as AnyObject) as! String
            modalObj.mobile_noLoc = item.validatedValue("mobile_no", expected: "" as AnyObject) as! String
            modalObj.pincode = item.validatedValue("pincode", expected: "" as AnyObject) as! String
            modalObj.addressLoc = item.validatedValue("address", expected: "" as AnyObject) as! String
            modalObj.locality_town = item.validatedValue("locality_town", expected: "" as AnyObject) as! String
            modalObj.city_district = item.validatedValue("city_district", expected: "" as AnyObject) as! String
            modalObj.stateLoc = item.validatedValue("state", expected: "" as AnyObject) as! String


            resultArray.append(modalObj)
        }
       
        return resultArray
}
    
    
    
    class func getOrderArray(responseArray: Array<Dictionary<String, Any>>) -> Array<CategoriesResponse> {
        var resultArray = [CategoriesResponse]()
        for i in 0..<responseArray.count  {
            let item = responseArray[i]
            let modalObj = CategoriesResponse()
            modalObj.id = item.validatedValue("id", expected: 0 as AnyObject) as! Int
            modalObj.order_status = item.validatedValue("order_status", expected: "" as AnyObject) as! String
            modalObj.order_status_dt = item.validatedValue("order_status_dt", expected: "" as AnyObject) as! String

            modalObj.myCart += CategoriesResponse.getProductsArray(responseArray: item.validatedValue("mycart", expected: "" as AnyObject) as! Array<AnyObject> as! Array<Dictionary<String, Any>>)

            resultArray.append(modalObj)
        }

        
        return resultArray
}
}

//{"address":"house no-11, street no -1, siyaram enclave","locality_town":"bhondsi","city_district":"gurgaon","state":"fsdfsf"}
