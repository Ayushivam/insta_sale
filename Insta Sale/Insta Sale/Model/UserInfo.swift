//
//  UserInfo.swift
//  Insta Sale
//
//  Created by Sajiya on 27/09/21.
//

import Foundation
class UserInfo: NSObject {
    var id = 0
    var name = ""
    var email = ""
    var imageURl = ""
    var mobile_no = ""
    var dob = ""
    var gender = ""
    var location = ""
    var defaultAdd = 0
    class func getUserData(responseData: Dictionary<String,AnyObject>) -> UserInfo {

        let modalObj = UserInfo()
        modalObj.id = responseData.validatedValue("id", expected: 0 as AnyObject) as! Int
        modalObj.name = responseData.validatedValue("name", expected: "" as AnyObject) as! String
        modalObj.email = responseData.validatedValue("email", expected: "" as AnyObject) as! String
    return modalObj
}
    
    class func getProfileData(responseData: Dictionary<String,AnyObject>) -> UserInfo {

        let modalObj = UserInfo()
        modalObj.id = responseData.validatedValue("id", expected: 0 as AnyObject) as! Int
        modalObj.name = responseData.validatedValue("name", expected: "" as AnyObject) as! String
        modalObj.email = responseData.validatedValue("email", expected: "" as AnyObject) as! String
        modalObj.imageURl = responseData.validatedValue("image", expected: "" as AnyObject) as? String ?? ""
        modalObj.mobile_no = responseData.validatedValue("mobile_no", expected: "" as AnyObject) as? String ?? ""
        modalObj.dob = responseData.validatedValue("dob", expected: "" as AnyObject) as? String ?? ""
        modalObj.gender = responseData.validatedValue("gender", expected: "" as AnyObject) as? String ?? ""
        modalObj.location = responseData.validatedValue("location", expected: "" as AnyObject) as? String ?? ""

    return modalObj
}
    
    
}
