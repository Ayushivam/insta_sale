//
//  AppDelegate.swift
//  Insta Sale
//
//  Created by Ayushi   on 22/09/21.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import GoogleSignIn

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var isReachable = false

    var sideMenuController = JASidePanelController()
    var window: UIWindow?
    var navController :  UINavigationController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupReachability()

        // Override point for customization after application launch.
        Thread.sleep(forTimeInterval: 1.0)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        // Facebook
        FBSDKCoreKit.ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            let statusBar = UIView(frame: window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            statusBar.backgroundColor = .white
            window?.addSubview(statusBar)
        } else {
            UIApplication.shared.statusBarStyle = .lightContent
        }
            
        GIDSignIn.sharedInstance().clientID = Helper.getGoogleClientID()
        kAppDelegate.setRootViewC()

        return true
    }
    
   

    func setupReachability() {
        // Allocate a reachability object
        let reach = Reachability.forInternetConnection()
        self.isReachable = reach!.isReachable()
        // Set the blocks
        reach?.reachableBlock = { (reachability) in
            DispatchQueue.main.async(execute: {
                self.isReachable = true
            })
        }
        reach?.unreachableBlock = { (reachability) in
            DispatchQueue.main.async(execute: {
                self.isReachable = false
            })}
        reach?.startNotifier()
    }

}

