//
//  AppDelegate_Router.swift
//  Insta Sale
//
//  Created by Sajiya on 22/09/21.
//

import Foundation

extension AppDelegate {
    
    // MARK: - Internal Functions ::
    
    internal func setRootViewC() {
        fLog()
        
        if kUserDefaults.isFirstLaunch {
            self.setWalkthroughViewCAsRoot()
        } else if kUserDefaults.isUserLogin {
            self.setTabBarViewCAsRoot()
        } else {
            setGetStartedViewCAsRoot()
        }
    }
    
    internal func setWalkthroughViewCAsRoot() {
        fLog()
        
        kUserDefaults.isFirstLaunch = false
        let objGetStartedViewC = Storyboard.Authentication.instantiateViewController(identifier: IntroductionViewC.className)
        
        let objNavigationC = UINavigationController(rootViewController: objGetStartedViewC)
        kSceneDelegate.window!.rootViewController = objNavigationC
    }
    
    internal func setGetStartedViewCAsRoot() {
        fLog()
        let objGetStartedViewC = Storyboard.Authentication.instantiateViewController(identifier: LogInViewC.className)
        
        let objNavigationC = UINavigationController(rootViewController: objGetStartedViewC)
        kSceneDelegate.window!.rootViewController = objNavigationC
        
    }
    
    internal func setTabBarViewCAsRoot() {
        fLog()
        let objGetStartedViewC = Storyboard.Home.instantiateViewController(identifier: HomeViewC.className)
        
        let objNavigationC = UINavigationController(rootViewController: objGetStartedViewC)
        kSceneDelegate.window!.rootViewController = objNavigationC
    }
    
    func setSlidePanel() -> JASidePanelController {
        let home = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabViewController")
        let side = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SideMenuViewController")
        let navigation = UINavigationController.init(rootViewController: home)
        self.sideMenuController.centerPanel = navigation
        self.sideMenuController.leftPanel = side
        self.sideMenuController.leftFixedWidth = 250
        self.sideMenuController.shouldDelegateAutorotateToVisiblePanel = false
        self.sideMenuController.shouldResizeLeftPanel = true
        self.sideMenuController.bounceOnSidePanelOpen = false
        self.sideMenuController.allowLeftSwipe = true
        self.sideMenuController.allowLeftOverpan = false
        return self.sideMenuController
    }
    
}

