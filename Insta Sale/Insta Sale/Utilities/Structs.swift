    //
    //  Structs.swift
    //  IOSAppTemplate
    //
    //  Created by Sajiya on 12/04/21.
    //
    
    import UIKit
    
    class Structs: NSObject {}
    
    struct Storyboard {
        static let Authentication         = UIStoryboard(name: "Authentication", bundle: nil)
        static let Home                   = UIStoryboard(name: "Home", bundle: nil)
        static let Thankyou               = UIStoryboard(name: "Thankyou", bundle: nil)
        static let Passport               = UIStoryboard(name: "Passport", bundle: nil)
        static let ForgotPassword         = UIStoryboard(name: "ForgotPassword", bundle: nil)
        static let VaccinationCard        = UIStoryboard(name: "VaccinationCard", bundle: nil)
        static let HealthIInfo            = UIStoryboard(name: "HealthIInfo", bundle: nil)
        static let MyProfile              = UIStoryboard(name: "MyProfile", bundle: nil)
        static let Cart              = UIStoryboard(name: "Cart", bundle: nil)

        static let SideMenu              = UIStoryboard(name: "SideMenu", bundle: nil)
        static let MyOrder              = UIStoryboard(name: "MyOrder", bundle: nil)
        static let MyAccount              = UIStoryboard(name: "MyAccount", bundle: nil)
        static let Address              = UIStoryboard(name: "Address", bundle: nil)

    }
    
    struct PlaceholderImage {
        static let User                     = UIImage(named: "placeholder_user")
        static let Video                    = UIImage(named: "placeholder_video")
        static let Image                    = UIImage(named: "placeholder_image")
    }
