//
//  UIFont_Extension.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 12/04/21.
//

import Foundation
import UIKit

extension UIFont {
    
   
    
    static func  fontAvantGardeLTBook(_ size : CGFloat) -> UIFont {
        return UIFont(name:"AvantGardeLT-Book", size: size)!
    }
    
   
}
