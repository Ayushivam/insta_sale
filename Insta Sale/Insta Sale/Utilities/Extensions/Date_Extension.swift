//
//  Date+Additions.swift

//
//  Created by Rohit Pathak on 23/01/20.
//  Copyright © 2020 Mobcoder. All rights reserved.
//


import Foundation

struct DateFormat
{
    static let shortDateFormat              = "MM-dd-yyyy"
    static let shortDateFormatWithSpace     = "dd MMM YYYY"
    static let timeFormat                   = "hh:mm a"
    static let exactTimeFormat              = "HH:mm"
    static let timeFormatAMPM               = "hh:mm AM"
    static let dayOfMonth                   = "dd MMMM"
    static let dayOfMonth1                  = "d MMM"
    static let monthofYear                  = "MMM YYYY"
    static let apiDate                      = "YYYY-MM-dd"
    static let fbApiDate                    = "MM/dd/yyyy"
    static let sleepFormat                  = "dd-MMM-yyyy HH:mm a"
    static let onlyDay1                     = "d"
    static let meridian                     = "a"
    static let onlyHour                     = "HH"
    static let onlyHour1                    = "H"
    static let onlyWeek                     = "EEE"
    static let onlyMonth                    = "MM"
    static let onlyMonth1                   = "mm"
    static let onlyMonth2                   = "MMM"
    static let onlyYear                     = "YYYY"
    static let isoFormat                    = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    static let iso8601Format                = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    static let dateWithDay                  = "EEE d, yyyy"
    static let standardFormat               = "yyyy-MM-dd'T'HH:mm:ss.000Z"
    static let ddMMYYYY                     = "dd/MM/yyyy"
    static let ddMMYYYY1                    = "dd-MM-yyyy"
    static let weekDateFormat               = "EEE, MMM d, yyyy"
    static let onlyTimeFormat               = "HH a"
    static let onlyDay                      = "EEEE"
    static let dayOfYear                    = "dd MMM yyyy"
    static let monthOfYear                  = "MMMM yyyy"
    static let apiTime                      = "HH:mm:ss"
    static let apiDateTime                  = "yyyy-MM-dd HH:mm:ss"
    static let apiPostDateTime              = "dd.MM.yyyy HH:mm:ss"
    static let apiDateTimeZone              = "yyyy-MM-dd HH:mm:ss ZZZ"
    static let ISO                          = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    static let apiDateWithTime              = "MM/dd/yyyy HH:mm:ss"
    static let apiDateFormat                = "yyyyMMdd"
    static let apiMonthDate                 = "MMM dd"
    static let monthDateYear                = "MMM dd yyyy"
    static let dateWithTime                 = "MMM dd yyyy hh:mm a"
    static let dateiOS                      = "yyyy-MM-dd HH:mm:ss z"
}

extension Date {
    
    
    
    func toMillisTimeStamp() -> Int64! {
            return Int64(self.timeIntervalSince1970*1000)
        }
    
    func localToUTC(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "H:mm:ss"
        
        return dateFormatter.string(from: dt!)
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormat.isoFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = DateFormat.isoFormat
        
        return dateFormatter.string(from: dt!)
    }
    
    func getStringDate(format: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let strDate = dateFormatter.string(from: self)
        return strDate
    }
    
    func getStringDate() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = DateFormat.ddMMYYYY
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
    
    func getMonth() -> Int? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = DateFormat.onlyMonth
        let strMonth = dateFormatter.string(from: self)
        return Int(strMonth)
    }
    
    public func dateStringWith(strFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = strFormat
        return dateFormatter.string(from: self)
    }
    
    static func convertFBDateString(dateString: String) -> Date {
        let dateFormatter = Foundation.DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = DateFormat.fbApiDate
        let convertedDate = dateFormatter.date(from: dateString)
        return convertedDate!
    }
    
    func changeDateFomrat(dateString: String)->String {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = DateFormat.apiDate
        let showDate = inputFormatter.date(from: dateString)
        inputFormatter.dateFormat = DateFormat.shortDateFormat
        let resultString = inputFormatter.string(from: showDate!)
        return resultString
    }
    
    func userAge() -> Int {
        let units:NSCalendar.Unit = [.year]
        let calendar = NSCalendar.current as NSCalendar
        calendar.timeZone = NSTimeZone.default
        calendar.locale = NSLocale.current
        let components = calendar.components(units, from: self, to: Date(), options: NSCalendar.Options.wrapComponents)
        let years = components.year
        return years!
    }
    
    func isDayTime()->Bool{
        
        var timeExist:Bool
        let calendar = Calendar.current
        let startTimeComponent = DateComponents(calendar: calendar, hour:06, minute: 00)
        let endTimeComponent   = DateComponents(calendar: calendar, hour:18, minute: 00)
        
        let startOfToday = calendar.startOfDay(for: self)
        let startTime    = calendar.date(byAdding: startTimeComponent, to:
            startOfToday)!
        let endTime      = calendar.date(byAdding: endTimeComponent, to:
            startOfToday)!
        
        if startTime <= self && self <= endTime {
            timeExist = true
        } else {
            timeExist = false
        }
        return timeExist
    }
    
    public var calendar: Calendar {
        return Calendar.current
    }
    
    /// Era.
    public var era: Int {
        return calendar.component(.era, from: self)
    }
    
    /// Year.
//    public var year: Int {
//        get {
//            return calendar.component(.year, from: self)
//        }
//    }
    
    func timeStampToStr(_ format: String, timeStamp: Double) -> String {
        let formm = DateFormatter()
        formm.dateFormat = format
        let timestampInSec = timeStamp / 1000
        let date = Date(timeIntervalSince1970: timestampInSec)
        return formm.string(from: date)
    }
    
    func localDate() -> Date {
        if let timeZone = TimeZone(abbreviation: "UTC") {
            let seconds = TimeInterval(timeZone.secondsFromGMT(for: self))
            return Date(timeInterval: seconds, since: self)
        }
        return self
        //return Formatter.preciseLocalTime.string(for: self) ?? ""
    }
    
    func dateString(_ format: String = "MMM-dd-YYYY, hh:mm a") -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: self)
    }
    
    
    // or GMT time
    func utcDate() -> Date {
        
        if let timeZone = TimeZone(abbreviation: "UTC") {
            let seconds = -TimeInterval(timeZone.secondsFromGMT(for: self))
            return Date(timeInterval: seconds, since: self)
        }
        return self
    }
    
    static func timeSince(_ dateStr: String, numericDates: Bool = false) -> String {
        
        let utcDate = dateStr.toDateInstaceFromStandardFormat()
        if let date = utcDate?.localDate() {
            let calendar = NSCalendar.current
            let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
            let now = Date().toLocalTime()
            let earliest = now < date ? now : date
            let latest = (earliest == now) ? date : now
            let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
            if (components.year! >= 2) {
                return "\(components.year!)yrs ago"
            } else if (components.year! >= 1){
                if (numericDates){
                    return "1yr ago"
                } else {
                    return "1yr ago"
                }
            } else if (components.month! >= 2) {
                return "\(components.month!)mon ago"
            } else if (components.month! >= 1){
                if (numericDates){
                    return "1mon ago"
                } else {
                    return "1mon ago"
                }
            } else if (components.weekOfYear! >= 2) {
                return "\(components.weekOfYear!)w ago"
            } else if (components.weekOfYear! >= 1){
                if (numericDates){
                    return "1w ago"
                } else {
                    return "1w ago"
                }
            } else if (components.day! >= 2) {
                return "\(components.day!)d ago"
            } else if (components.day! >= 1){
                if (numericDates){
                    return "1d ago"
                } else {
                    return "1d ago"
                }
            } else if (components.hour! >= 2) {
                return "\(components.hour!)h ago"
            } else if (components.hour! >= 1){
                if (numericDates){
                    return "1h ago"
                } else {
                    return "1h ago"
                }
            } else if (components.minute! >= 2) {
                return "\(components.minute!)mins ago"
            } else if (components.minute! >= 1){
                if (numericDates){
                    return "1min ago"
                } else {
                    return "1min ago"
                }
            } else if (components.second! >= 3) {
                return "\(components.second!)secs ago"
            } else {
                return "Just now"
            }
        }
        return ""
    }
    
    
    static func timeLeft(_ dateStr: String, numericDates: Bool = false) -> String {
        
        let utcDate = dateStr.toDateInstaceFromStandardFormat()
        if let date = utcDate?.localDate() {
            let calendar = NSCalendar.current
            let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
            let now = Date()
            let earliest = now < date ? now : date
            let latest = (earliest == now) ? date : now
            let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
            if (components.year! >= 2) {
                return "\(components.year!)yrs ago"
            } else if (components.year! >= 1){
                if (numericDates){
                    return "1yr ago"
                } else {
                    return "1yr ago"
                }
            } else if (components.month! >= 2) {
                return "\(components.month!)mon ago"
            } else if (components.month! >= 1){
                if (numericDates){
                    return "1mon ago"
                } else {
                    return "1mon ago"
                }
            } else if (components.weekOfYear! >= 2) {
                return "\(components.weekOfYear!)w ago"
            } else if (components.weekOfYear! >= 1){
                if (numericDates){
                    return "1w ago"
                } else {
                    return "1w ago"
                }
            } else if (components.day! >= 2) {
                return "\(components.day!)d ago"
            } else if (components.day! >= 1){
                if (numericDates){
                    return "1d ago"
                } else {
                    return "1d ago"
                }
            } else if (components.hour! >= 2) {
                return "\(components.hour!)h ago"
            } else if (components.hour! >= 1){
                if (numericDates){
                    return "1h ago"
                } else {
                    return "1h ago"
                }
            } else if (components.minute! >= 2) {
                return "\(components.minute!)mins ago"
            } else if (components.minute! >= 1){
                if (numericDates){
                    return "1min ago"
                } else {
                    return "1min ago"
                }
            } else if (components.second! >= 3) {
                return "\(components.second!)secs ago"
            } else {
                return "Just now"
            }
        }
        return ""
    }
    
}

extension String {
    func toDateInstaceFromStandardFormat() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Calendar.current.timeZone
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = DateFormat.ISO
        return dateFormatter.date(from: self)
    }
    
    func getDateStringFrom(inputFormat: String, outputFormat: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Calendar.current.timeZone
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = inputFormat
        guard let dateInstance = dateFormatter.date(from: self) else { return nil }
        dateFormatter.dateFormat = outputFormat
        return dateFormatter.string(from: dateInstance)
    }
}

extension Date {
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let formatter = DateFormatter()
        let timezone = TimeZone.current
        formatter.timeZone = TimeZone(identifier: "UTC")
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
}

extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the amount of nanoseconds from another date
    func nanoseconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.nanosecond], from: date, to: self).nanosecond ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        var result: String = ""
//        if years(from: date)   > 0 { return "\(years(from: date))y"   }
//        if months(from: date)  > 0 { return "\(months(from: date))M"  }
//        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
//        if seconds(from: date) > 0 { return "\(seconds(from: date))" }
//        if days(from: date)    > 0 { result = result + " " + "\(days(from: date)) D" }
//        if hours(from: date)   > 0 { result = result + " " + "\(hours(from: date)) H" }
//        if minutes(from: date) > 0 { result = result + " " + "\(minutes(from: date)) M" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))" }
        return ""
    }
}

// MARK: - ISO data formatter
extension DateFormatter {
    static let iso8601DateFormatter: DateFormatter = {
        let enUSPOSIXLocale = Locale(identifier: "en_US_POSIX")
        let iso8601DateFormatter = DateFormatter()
        iso8601DateFormatter.locale = enUSPOSIXLocale
        iso8601DateFormatter.dateFormat = DateFormat.isoFormat
        iso8601DateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return iso8601DateFormatter
    }()
    
    static let iso8601WithoutMillisecondsDateFormatter: DateFormatter = {
        let enUSPOSIXLocale = Locale(identifier: "en_US_POSIX")
        let iso8601DateFormatter = DateFormatter()
        iso8601DateFormatter.locale = enUSPOSIXLocale
        iso8601DateFormatter.dateFormat = DateFormat.iso8601Format
        iso8601DateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return iso8601DateFormatter
    }()
    
    static func date(fromISO8601String string: String) -> Date? {
        if let dateWithMilliseconds = iso8601DateFormatter.date(from: string) {
            return dateWithMilliseconds
        }
        if let dateWithoutMilliseconds = iso8601WithoutMillisecondsDateFormatter.date(from: string) {
            return dateWithoutMilliseconds
        }
        return nil
    }
}

// MARK: - Calendar Extension
extension Calendar {
    static let gregorian = Calendar(identifier: .gregorian)
}

// MARK: - Date Extension
extension Date {
    var startDateOfWeek: Date? {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
    }
    
    var startDateOfMonth: Date? {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.year, .month], from: self))
    }
    
    var startDateOfYear: Date? {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.year], from: self))
    }
}

extension DateFormatter {
    
    func years<R: RandomAccessCollection>(_ range: R) -> [String] where R.Iterator.Element == Int {
        
        setLocalizedDateFormatFromTemplate("yyyy")
        
        var comps = DateComponents(month: 1, day: 1)
        var res = [String]()
        
        for y in range {
            
            comps.year = y
            if let date = calendar.date(from: comps) {
                res.append(string(from: date))
            }
        }
        return res
    }
}

extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return end - start
    }
}

extension Date {

    func differenceOfDate(rhs: Date) -> TimeInterval {
        return self.timeIntervalSince1970 - rhs.timeIntervalSince1970
    }
}
