//
//  UITableView_Extension.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 12/04/21.
//

import Foundation
import UIKit

extension UITableView {
    
    func register(nib nibName: String) {
        let nib = UINib(nibName: nibName, bundle: nil)
        self.register(nib, forCellReuseIdentifier: nibName)
    }
    
    func registerMultiple(nibs arrayNibs: [String]) {
        for nibName in arrayNibs {
            self.register(nib: nibName)
        }
    }
    
    func registerHeaderFooter(nib nibName: String) {
        let nib = UINib(nibName: nibName, bundle: nil)
        self.register(nib, forHeaderFooterViewReuseIdentifier: nibName)
    }
    
    func registerMultipleHeaderFooter(nibs arrayNibs: [String]) {
        for nibName in arrayNibs {
            let nib = UINib(nibName: nibName, bundle: nil)
            self.register(nib, forHeaderFooterViewReuseIdentifier: nibName)
        }
    }
    
    // MARK: - Remove empty cells
    
    func hideEmptyCells() {
        let view = UIView()
        view.backgroundColor = .clear
        view.frame = .zero
        self.tableFooterView = view
    }
    
    enum Position {
        case top
        case bottom
    }
    
    // MARK: - Scroll to top
    
    func scrollToTop(animation: Bool) {
        if self.tableHeaderView != nil {
            self.setContentOffset(.zero, animated: animation)
        } else {
            let indexPath = IndexPath(row: 0, section: 0)
            self.scrollToRow(at: indexPath, at: .middle, animated: animation)
        }
    }
    
    // MARK: - Scroll to bottom
    
    func scrollToBottom(animation: Bool) {
        let section = self.numberOfSections - 1
        let lastRow = self.numberOfRows(inSection: section) - 1
        if section >= 0, lastRow >= 0 {
            let indexPath = IndexPath(row: lastRow, section: section)
            self.scrollToRow(at: indexPath, at: .top, animated: animation)
        }
    }
    
    func scroll(to: Position, animated: Bool) {
        let sections = numberOfSections
        let rows = numberOfRows(inSection: sections - 1)
        switch to {
        case .top:
            if rows > 0 {
                let indexPath = IndexPath(row: 0, section: 0)
                self.scrollToRow(at: indexPath, at: .top, animated: animated)
            }
            break
        case .bottom:
            if rows > 0 {
                let indexPath = IndexPath(row: rows - 1, section: sections - 1)
                self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
            }
            break
        }
    }
    
    func register<T:UITableViewCell>(_: T.Type) where T: ReusableView {
        self.register(T.self, forCellReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func register<T:UITableViewCell>(_: T.Type) where T: ReusableView, T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        self.register(nib, forCellReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func dequeueReusableCell<T:UITableViewCell>() -> T where T: ReusableView {
        guard let cell = self.dequeueReusableCell(withIdentifier: T.defaultReuseIdentifier) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.defaultReuseIdentifier)")
        }
        return cell
    }
    
    func dequeueReusableCell<T:UITableViewCell>(forIndexPath indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = self.dequeueReusableCell(withIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.defaultReuseIdentifier)")
        }
        return cell
    }
}

extension UITableView {
    func emptyCell() -> UITableViewCell {
        let cellDefault: UITableViewCell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cellDefault.selectionStyle = .none
        cellDefault.textLabel?.text = ""
        cellDefault.imageView?.image = nil
        return cellDefault
    }
}

protocol ReusableView: class {
    static var defaultReuseIdentifier: String { get }
}

protocol NibLoadableView: class {
    static var nibName: String { get }
}

extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return String(describing: Self.self)
    }
}

extension NibLoadableView where Self: UIView {
    static var nibName: String {
        return String(describing: Self.self)
    }
}
