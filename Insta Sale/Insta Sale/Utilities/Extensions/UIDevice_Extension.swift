//
//  UIDevice_Extensions.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 12/04/21.
//

import Foundation

import UIKit

extension UIDevice {
    
    static var isSimulator: Bool = {
        #if targetEnvironment(simulator)
        return true
        #else
        return false
        #endif
    }()
    
    var hasNotch: Bool {
        if #available(iOS 10, *) {
            let bottom = kApplication.windows.first?.safeAreaInsets.bottom ?? 0
            return bottom > 0
        } else {
            let bottom = kKeyWindow?.safeAreaInsets.bottom ?? 0
            return bottom > 0
        }
    }
    
    var topSafeAreaHeight: CGFloat {
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows[0]
            let topPadding = window.safeAreaInsets.top
            dLog(message: "Top Safe Area Height :: \(topPadding)")
            return topPadding
        } else {
            return 0.0
        }
    }
    
    var bottomSafeAreaHeight: CGFloat {
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows[0]
            let bottomPadding = window.safeAreaInsets.bottom
            dLog(message: "Bottom Safe Area Height :: \(bottomPadding)")
            return bottomPadding
        } else {
            return 0.0
        }
    }
    
    var topAndBottomSafeAreaHeight: CGFloat {
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows[0]
            let topPadding = window.safeAreaInsets.top
            let bottomPadding = window.safeAreaInsets.bottom
            dLog(message: "Top And Bottom Safe Area Height :: \(topPadding+bottomPadding)")
            return topPadding + bottomPadding
        } else {
            return 0.0
        }
    }
}

