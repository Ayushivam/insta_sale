//
//  Encodable_Extension.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 12/04/21.
//

import UIKit

extension Encodable {
  var dictionary: [String: Any] {
    guard let data = try? JSONEncoder().encode(self) else { return [String: Any]() }

    if let dictionary = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] {
        return dictionary
    } else {
        return [String: Any]()
    }
  }
}
