//
//  AVCaptureSession_Extension.swift
//  Talegate
//
//  Created by Sajiya on 22/04/21.
//

import AVFoundation

extension AVCaptureSession {
    func resetInputs() {
        // remove all sesison inputs
        for i in inputs {
            removeInput(i)
        }
    }
}
