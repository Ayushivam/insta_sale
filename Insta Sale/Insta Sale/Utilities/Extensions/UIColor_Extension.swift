//
//  UIColor_Extension.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 12/04/21.
//

import UIKit

extension UIColor {
    
    // MARK: - Initialization
    
    convenience init(hex: String, alpha: CGFloat = 1.0) {
        var hexFormatted: String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()

        if hexFormatted.hasPrefix("#") {
            hexFormatted = String(hexFormatted.dropFirst())
        }

        assert(hexFormatted.count == 6, "Invalid hex code used.")

        var rgbValue: UInt64 = 0
        Scanner(string: hexFormatted).scanHexInt64(&rgbValue)

        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: alpha)
    }
    
    // MARK: - Computed Properties ::
    
    var toHex: String? {
        return toHex()
    }
    
    // MARK: - From UIColor to Hex String ::
    
    func toHex(alpha: Bool = false) -> String? {
        guard let components = cgColor.components, components.count >= 3 else {
            return nil
        }
        
        let r = Float(components[0])
        let g = Float(components[1])
        let b = Float(components[2])
        var a = Float(1.0)
        
        if components.count >= 4 {
            a = Float(components[3])
        }
        
        if alpha {
            return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
        } else {
            return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
        }
    }
    
    class func colorWithRGBA(redC: CGFloat, greenC: CGFloat, blueC: CGFloat, alfa: CGFloat) -> UIColor {
        var red: CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue: CGFloat = 0.0
        var alpha: CGFloat = alfa
        
        red   = CGFloat(redC/255.0)
        green = CGFloat(greenC/255.0)
        blue  = CGFloat(blueC/255.0)
        alpha  = CGFloat(alpha)
        let color: UIColor =  UIColor(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha: alpha)
        return color
    }
    
    func colorWithAlpha(color: CGFloat, alfa: CGFloat) -> UIColor {
        let red: CGFloat = CGFloat(color / 255.0)
        let green: CGFloat = CGFloat(color / 255.0)
        let blue: CGFloat = CGFloat(color / 255.0)
        let alpha: CGFloat = alfa
        let color: UIColor =  UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    // MARK: Generate random colours
    
    /// random colour with a fixed alpha
    static var random1: UIColor {
        return UIColor(red: .random(in: 0 ... 1),
                       green: .random(in: 0 ... 1),
                       blue: .random(in: 0 ... 1),
                       alpha: 0.79)
    }
    
    
    /// random colour with random alpha
    static var randomColourAndRandomAlpha: UIColor {
        return UIColor(red: .random(in: 0 ... 1),
                       green: .random(in: 0 ... 1),
                       blue: .random(in: 0 ... 1),
                       alpha: .random(in: 0.50 ... 0.90))
    }
}
