//
//  Double_Extension.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 12/04/21.
//

import UIKit

extension Double {
    func rounded(toPlaces places:Int) -> Double {
        //let divisor = pow(10.0, Double(places))
        //return (self * divisor).rounded() / divisor
        return Double(Int((pow(10, Double(places)) * self).rounded())) / pow(10, Double(places))
    }

    func roundedStr(toPlaces places:Int) -> String {
        //let divisor = pow(10.0, Double(places))
        //return (self * divisor).rounded() / divisor
        return String(Double(Int((pow(10, Double(places)) * self).rounded())) / pow(10, Double(places)))
    }
}
