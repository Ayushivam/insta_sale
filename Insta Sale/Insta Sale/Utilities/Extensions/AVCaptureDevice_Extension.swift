//
//  AVCaptureDevice_Extension.swift
//  Talegate
//
//  Created by Sajiya on 22/04/21.
//

import AVFoundation

extension AVCaptureDevice {
    func tryToggleTorch() {
        guard hasFlash else { return }
        guard hasTorch else { return }
        do {
            try lockForConfiguration()
            switch torchMode {
            case .auto:
                torchMode = .on
            case .on:
                torchMode = .off
            case .off:
                //torchMode = .auto
                torchMode = .on
            @unknown default:
                fatalError()
            }
            unlockForConfiguration()
        } catch _ { }
    }
}
