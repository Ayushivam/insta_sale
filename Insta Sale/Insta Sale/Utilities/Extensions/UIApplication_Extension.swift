//
//  UIApplication_Extension.swift
//  Talegate
//
//  Created by Sajiya on 22/04/21.
//

import Foundation
public extension UIApplication {
    
    func clearLaunchScreenCache() {
        #if DEBUG
        do {
            let launchScreenPath = "\(NSHomeDirectory())/Library/SplashBoard"
            try FileManager.default.removeItem(atPath: launchScreenPath)
        } catch {
            print("Failed to delete launch screen cache - \(error)")
        }
        #endif
    }
    
}
