//
//  UIWindow_Extension.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 12/04/21.
//

import Foundation

extension UIWindow {
    
    static var currentController: UIViewController? {
        let SceneDelegate = UIApplication.shared.delegate as! AppDelegate
        return SceneDelegate.window?.currentController
        // gauerd le
    }
    
    var currentController: UIViewController? {
        if let vc = self.rootViewController {
            return getCurrentController(vc: vc)
        }
        return nil
    }
    
    func getCurrentController(vc: UIViewController) -> UIViewController {
        
        if let pc = vc.presentedViewController {
            return getCurrentController(vc: pc)
        }
        else if let nc = vc as? UINavigationController {
            if nc.viewControllers.count > 0 {
                return getCurrentController(vc: nc.viewControllers.last!)
            } else {
                return nc
            }
        }
            
        else {
            return vc
        }
    }
    static var key: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }
}

