//
//  NSObject_Extension.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 12/04/21.
//

import Foundation

extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}
