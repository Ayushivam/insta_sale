//
//  UserDefaults_Extension.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 13/04/21.
//

import Foundation

import UIKit

let APP_CENTER_KEY = "APP_CENTER_KEY"
let APPLE_USER_NAME = "APPLE_USER_NAME"
let APPLE_USER_LNAME = "APPLE_USER_LNAME"

let APPLE_USER_EMAIL = "APPLE_USER_EMAIL"
let APPLE_USER_IDENTIFIER = "APPLE_USER_ID"
extension UserDefaults {

    struct UDKey {
        static let IsFirstLaunch             = "UD_Key_Is_First_Launch"
        static let DeviceToken               = "UD_Key_Device_Token"
        static let AccessToken               = "UD_Key_Access_Token"
        static let socialToken               = "UD_Key_Social_Token"

        static let RefreshToken              = "UD_Key_Refresh_Token"
        
        static let IsUserLogin               = "UD_Key_Is_User_Login"
        static let IsPassportVerify          = "UD_Key_Is_Passport_Verify"
        static let UserId                    = "UD_Key_User_Id"
        static let UserFirstName             = "UD_Key_User_First_Name"
        static let UserLastName              = "UD_Key_User_Last_Name"
        static let UserLatitude              = "UD_Key_User_Latitude"
        static let UserLongitude             = "UD_Key_User_Longitude"
        static let UserLocation              = "UD_Key_User_Location"
    }

    @SharedPref(key: UDKey.IsFirstLaunch, defaultValue: true)
    static var isFirstLaunch: Bool
    
    @SharedPref(key: UDKey.DeviceToken, defaultValue: "")
    static var deviceToken: String
    
    @SharedPref(key: UDKey.AccessToken, defaultValue: "")
    static var accessToken: String
    
    @SharedPref(key: UDKey.socialToken, defaultValue: "")
    static var socialToken: String
    
    @SharedPref(key: UDKey.RefreshToken, defaultValue: "")
    static var refreshToken: String
    
    @SharedPref(key: UDKey.IsUserLogin, defaultValue: false)
    static var isUserLogin: Bool
    
    @SharedPref(key: UDKey.IsPassportVerify, defaultValue: false)
    static var IsPassportVerify: Bool
    
    @SharedPref(key: UDKey.UserId, defaultValue: 0)
    static var userId: Int
    
    @SharedPref(key: UDKey.UserFirstName, defaultValue: "")
    static var userFirstName: String
    
    @SharedPref(key: UDKey.UserLastName, defaultValue: "")
    static var userLastName: String
    
    @SharedPref(key: UDKey.UserLatitude, defaultValue: 0.0)
    static var userLatitude: Double
    
    @SharedPref(key: UDKey.UserLongitude, defaultValue: 0.0)
    static var userLongitude: Double
    
    @SharedPref(key: UDKey.UserLocation, defaultValue: "")
    static var userLocation: String
    
    func clear() {
        self.set("", forKey: UDKey.AccessToken)
        self.set("", forKey: UDKey.RefreshToken)
        self.set(false, forKey: UDKey.IsPassportVerify)

        self.set(false, forKey: UDKey.IsUserLogin)
        self.set("", forKey: UDKey.UserId)
        self.set("", forKey: UDKey.UserFirstName)
        self.set("", forKey: UDKey.UserLastName)
        self.set("", forKey: UDKey.socialToken)

    }
}
