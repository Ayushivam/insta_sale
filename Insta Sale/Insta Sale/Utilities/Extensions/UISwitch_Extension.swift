//
//  UISwitch_Extension.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 12/04/21.
//

import UIKit

extension UISwitch {
    func toggle() {
        self.setOn(!self.isOn, animated: true)
    }
}
