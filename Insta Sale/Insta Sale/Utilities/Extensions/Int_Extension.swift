//
//  Int_Extension.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 12/04/21.
//

import UIKit

extension Int {
    var toBool: Bool { return self != 0 }
}

extension Array where Element: Hashable {
    var uniques: Array {
        var buffer = Array()
        var added = Set<Element>()
        for elem in self {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
}
