//
//  Bool_Extension.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 12/04/21.
//

import UIKit

extension Bool {
    var toInt: Int { return self == false ? 0 : 1 }
}
