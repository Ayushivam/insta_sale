//
//  Localizable.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 13/04/21.
//

import UIKit

protocol Localizable {
    func localized(str: String) -> String
}

extension Localizable {
    func localized(str: String) -> String {
        return NSLocalizedString(str, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
