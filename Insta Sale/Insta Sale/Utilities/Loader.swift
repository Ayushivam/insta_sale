////
////  Loader.swift
////  IOSAppTemplate
////
////  Created by Sajiya on 12/04/21.
////
//
//import UIKit
//
//class Loader: NSObject {
//    
//    
//    // MARK: - Loading Indicator ::
//    
//    static let thickness = CGFloat(6.0)
//    static let radius = CGFloat(22.0)
//    
//    class func showLoader(message: String?) {
//        DispatchQueue.main.async {
//            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
//            SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.flat)
//            SVProgressHUD.show(withStatus: message!)
//        }
//    }
//    
//    class func showLoaderInView(title: String = "Loading...", view: UIView) {
//        
//        DispatchQueue.main.async {
//            SVProgressHUD.setBackgroundColor(UIColor.clear)
//            SVProgressHUD.setDefaultMaskType(.clear)
//            SVProgressHUD.setRingThickness(thickness)
//            SVProgressHUD.setRingRadius(radius)
//            
//            if !SVProgressHUD.isVisible() {
//                SVProgressHUD.show()
//            }
//        }
//    }
//    
//    class func hideLoader() {
//        DispatchQueue.main.async {
//            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.none)
//            SVProgressHUD.dismiss()
//        }
//    }
//    
//    class func hideLoaderInView(view: UIView) {
//        DispatchQueue.main.async {
//            SVProgressHUD.dismiss()
//        }
//    }
//}
