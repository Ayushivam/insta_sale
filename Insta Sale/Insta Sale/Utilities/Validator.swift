//
//  Validator.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 13/04/21.
//

import UIKit

class Validator: NSObject {
    
    class func validateFullName(name: String?) -> Bool {
        guard let name = name else { return false }
        return name.count > 0
    }
    
    class func validateUsername(username: String?) -> Bool {
        guard let name = username else { return false }
        return name.count > 0
    }
    
    
    class func validatePassword(password: String?) -> Bool {
        guard let password = password else { return false }
        
        //Minimum 8 characters at least 1 Alphabet and 1 Number:
        //let passRegEx = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
        
        //Your password must have at least 8 characters that contains 1 uppercase letter, 1 number and 1 special character.
        let passRegEx = "^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
        let trimmedString = password.trimmingCharacters(in: .whitespaces)
        let validatePassord = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        let isvalidatePass = validatePassord.evaluate(with: trimmedString)
        return isvalidatePass
    }
    
    class func validatePhoneNumber(phoneNumber: String?) -> Bool {
        guard let phoneNumber = phoneNumber else { return false }
        return phoneNumber.count > 0
    }
}
