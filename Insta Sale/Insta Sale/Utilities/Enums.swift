//
//  Enums.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 12/04/21.
//

import UIKit

class Enums: NSObject {}

enum DeviceType: Int {
    case iOS = 1
    case android = 2
    case web = 3
    
    var value: String {
        switch self {
        case .iOS: return "1"
        case .android: return "2"
        case .web: return "3"
        }
    }
    
    var description: String {
        switch self {
        case .iOS: return "IOS"
        case .android: return "ANDROID"
        case .web: return "WEB"
        }
    }
}

enum TransitionType: Int {
    case push = 1
    case present = 2
}

enum RegisterType: Int {
    case email = 1
    case google = 2
    case facebook = 3
    case apple = 4
    
    var value: String {
        switch self {
        case .email: return "1"
        case .google: return "2"
        case .facebook: return "3"
        case .apple: return "4"
        }
    }
    
    var description: String {
        switch self {
        case .email: return "Email"
        case .google: return "Google"
        case .facebook: return "Facebook"
        case .apple: return "Apple"
        }
    }
}

enum LoginType: Int {
    case email = 1
    case google = 2
    case facebook = 3
    case apple = 4
    
    var value: String {
        switch self {
        case .email: return "1"
        case .google: return "2"
        case .facebook: return "3"
        case .apple: return "4"
        }
    }
    
    var description: String {
        switch self {
        case .email: return "Email"
        case .google: return "Google"
        case .facebook: return "Facebook"
        case .apple: return "Apple"
        }
    }
}

enum Gender: Int {
    case male = 1
    case female = 2
}

enum PushNotificationType: Int {
    case dev = 1
    case dis = 2
    
    var value: String {
        switch self {
        case .dev: return "1"
        case .dis: return "2"
        }
    }
}

enum WebViewType: Int {
    case none = 0
    case termCondition = 1
    case privacyPolicy = 2
    case aboutUs = 3
    case contactUs = 4
}


enum TeamType: Int {
    case college = 1
    case pro = 2
    
    var value: String {
        switch self {
        case .college: return "1"
        case .pro: return "2"
        }
    }
    
    var description: String {
        switch self {
        case .college: return "College"
        case .pro: return "Pro"
        }
    }
}

enum HomeViewType: Int {
    case teamTales = 1
    case teamMatches = 2
}

enum HomePostType: Int {
    case yourTeams = 1
    case spectator = 2
}

enum PostType: Int {
    case video = 1
    case photo = 2
}

enum FilterType: Int {
    case none = 0
    case yourTeams
    case spectator
    case mostLike
    case followers
}

enum FilterViewType: Int {
    case none = 0
    case homePostType
    case homePostFilter
}

enum ActionType: Int {
    case none = 0
    case addTeammates
    case sendPrivateMessage
}
