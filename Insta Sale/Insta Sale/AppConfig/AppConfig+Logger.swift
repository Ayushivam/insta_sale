//
//  AppConfig+Logger.swift
//  Copyright © 2020 Mobcoder. All rights reserved.
//

import Foundation
import XCGLogger

// MARK: All app loggers
class LogHelper {
    
    // Create log methods
    static let log: XCGLogger = {
        
        // Create a logger object with no destinations
        let log = XCGLogger(identifier: "advancedLogger", includeDefaultDestinations: false)
        
        // Create a destination for the system console log (via NSLog)
        let systemDestination = AppleSystemLogDestination(identifier: "advancedLogger.appleSystemLogDestination")
        
        // Optionally set some configuration options
        systemDestination.outputLevel = .debug
        systemDestination.showLogIdentifier = false
        systemDestination.showFunctionName = true
        systemDestination.showThreadName = true
        systemDestination.showLevel = true
        systemDestination.showFileName = true
        systemDestination.showLineNumber = true
        
        // Add the destination to the logger
        log.add(destination: systemDestination)
        
        // Create a file log destination
        let logPath: URL = LogHelper.documentsDirectory.appendingPathComponent("XCGLogger_Log.txt")
        let autoRotatingFileDestination = AutoRotatingFileDestination(writeToFile: logPath, identifier: "advancedLogger.fileDestination", shouldAppend: true,
           attributes: [.protectionKey: FileProtectionType.completeUntilFirstUserAuthentication], // Set file attributes on the log file
         maxFileSize: 1024 * 1024, // 5k, not a good size for production (default is 1 megabyte)
         maxTimeInterval: 60 * 60 * 24, // 1 minute, also not good for production (default is 10 minutes)
        targetMaxLogFiles: 5) // Default is 10, max is 255
        
        // Optionally set some configuration options
        autoRotatingFileDestination.outputLevel = .debug
        autoRotatingFileDestination.showLogIdentifier = false
        autoRotatingFileDestination.showFunctionName = true
        autoRotatingFileDestination.showThreadName = true
        autoRotatingFileDestination.showLevel = true
        autoRotatingFileDestination.showFileName = true
        autoRotatingFileDestination.showLineNumber = true
        autoRotatingFileDestination.showDate = true
        
        // Process this destination in the background
        autoRotatingFileDestination.logQueue = XCGLogger.logQueue
        
        // Add the destination to the logger
        log.add(destination: autoRotatingFileDestination)
        
        //default output level
        log.outputLevel = .debug
        
        // Add basic app info, version info etc, to the start of the logs
        log.logAppDetails()
        
        let emojiLogFormatter = PrePostFixLogFormatter()
        emojiLogFormatter.apply(prefix: "🗯🗯🗯 ", postfix: " 🗯🗯🗯", to: .verbose)
        emojiLogFormatter.apply(prefix: "🔹🔹🔹 ", postfix: " 🔹🔹🔹", to: .debug)
        emojiLogFormatter.apply(prefix: "ℹ️ℹ️ℹ️ ", postfix: " ℹ️ℹ️ℹ️", to: .info)
        emojiLogFormatter.apply(prefix: "⚠️⚠️⚠️ ", postfix: " ⚠️⚠️⚠️", to: .warning)
        emojiLogFormatter.apply(prefix: "‼️‼️‼️ ", postfix: " ‼️‼️‼️", to: .error)
        emojiLogFormatter.apply(prefix: "💣💣💣 ", postfix: " 💣💣💣", to: .severe)
        log.formatters = [emojiLogFormatter]
        
        return log
    }()
    
    // Document directory
    static let documentsDirectory: URL = {
        let urls = FILE_MANAGER.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.endIndex - 1]
    }()
    
    static func verbose(_ sender: AnyObject) {
        #if DEBUG
        print(sender)
        #else
        log.verbose(sender)
        #endif
    }
    
    static func debug(_ sender: AnyObject) {
        log.debug(sender)
    }
    
    static func info(_ sender: AnyObject) {
        log.info(sender)
    }
    
    static func warning(_ sender: AnyObject) {
        log.warning(sender)
    }
    
    static func error(_ sender: AnyObject) {
        log.error(sender)
    }
    
    static func severe(_ sender: AnyObject) {
        log.severe(sender)
    }
    
    static func updateLogLevel(level:XCGLogger.Level) {
        log.outputLevel = level
    }
    
    // Get log files
    static func getLogFiles() -> [AnyObject] {
        var files = [AnyObject]()
        let documentsURL = LogHelper.documentsDirectory
        do {
            let fileURLs = try FILE_MANAGER.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            // process files
            for url in fileURLs {
                let name = url.lastPathComponent
                if name.contains("XCGLogger") {
                    files.append(url as AnyObject)
                }
            }
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
        return files
    }
}
