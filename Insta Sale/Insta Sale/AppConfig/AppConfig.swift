//
//  AppConfig.swift
//  IOSAppTemplate
//
//  Created by Sajiya on 12/04/21.
//

class AppConfig: NSObject {
    
    static let defautQ      = DispatchQueue.global(qos: .default)
    static let defautMainQ  = DispatchQueue.main
    static let backgrountQ  = DispatchQueue.global(qos: .background)
    static let priorityQ    = DispatchQueue.global(qos: .userInteractive)
    static var debugDate    = Date()
    
    struct WebUrl {
        static let TermsCondition   = "https://www.google.com/"
        static let PrivacyPolicy    = "https://www.google.com/"
        static let AboutUs          = "https://www.google.com/"
        static let ContactUs        = "https://www.google.com/"

    }
    
    struct SocialUrl {
        static let Facebook         = ""
        static let Twitter          = ""
        static let Instagram        = ""
    }
    
    enum AppCustomConfig: String {
        case APP_NAME
        case BUNDLE_ID
        case APP_STORE_ID
        case API_BASE_URL
        case SOCKET_BASE_URL
        case CDN_URL
        case AUTH_USERNAME
        case AUTH_PASSWORD
        case QR_IMAGE_URL

    }
    
    static func getAppName() -> String {
        let bundle = kMainBundle.object(forInfoDictionaryKey: AppCustomConfig.APP_NAME.rawValue)
        return bundle as! String
    }
    
    static func getBundleId() -> String {
        let bundle = kMainBundle.object(forInfoDictionaryKey: AppCustomConfig.BUNDLE_ID.rawValue)
        return bundle as! String
    }
    
    static func getAppStoreId() -> String {
        let bundle = kMainBundle.object(forInfoDictionaryKey: AppCustomConfig.APP_STORE_ID.rawValue)
        return bundle as! String
    }
    
    static func getAPIBaseURL() -> String {
        let bundle = kMainBundle.object(forInfoDictionaryKey: AppCustomConfig.API_BASE_URL.rawValue)
        return bundle as! String
    }
    
    static func getSocketBaseURL() -> String {
        let bundle = kMainBundle.object(forInfoDictionaryKey: AppCustomConfig.SOCKET_BASE_URL.rawValue)
        return bundle as! String
    }
    
    static func getCdnURL() -> String {
        let bundle = kMainBundle.object(forInfoDictionaryKey: AppCustomConfig.CDN_URL.rawValue)
        return bundle as! String
    }
    
    static func getAuthUsername() -> String {
        let bundle = kMainBundle.object(forInfoDictionaryKey: AppCustomConfig.AUTH_USERNAME.rawValue)
        return bundle as! String
    }
    
    static func getAuthPassword() -> String {
        let bundle = kMainBundle.object(forInfoDictionaryKey: AppCustomConfig.AUTH_PASSWORD.rawValue)
        return bundle as! String
    }
    
    static func getQrImageURL() -> String {
        let bundle = kMainBundle.object(forInfoDictionaryKey: AppCustomConfig.QR_IMAGE_URL.rawValue)
        return bundle as! String
    }
}
