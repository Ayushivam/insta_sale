//
//  ImagePickerHandler.swift
//  Whoop
//
//  Created by Sani's MacBook Pro on 13/10/20.
//  Copyright © 2020 Sani Kumar. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import Photos
import AVKit

enum PickerMediaType {
    case ImageOnly
    case VideoOnly
    case AllMedia
}

let videoMaxSizeInMB = Double(75.0)
let maxVideoDuration = TimeInterval(120)
let byte1024 = Double(1024.0)
var allowEditing = true

class ImgPickerHandler: NSObject
{
    static let sharedHandler = ImgPickerHandler()
    weak var guestInstance: UIViewController? = nil
    var imageClosure: ((UIImage?, Bool)->())? = nil
    var imgInfoClosure: (([String: Any], Bool)->())? = nil
    
    // Public Methods
    func getImage(instance: UIViewController, rect: CGRect?, completion: ((_ myImage: UIImage?, _ success: Bool)->())?) {
        guestInstance = instance
        let imgPicker = UIImagePickerController()
        imgPicker.delegate = self
        imgPicker.videoQuality = .typeMedium
        imgPicker.allowsEditing = allowEditing
        imgPicker.mediaTypes = [(kUTTypeImage) as String]
        
        let actionSheet = UIAlertController(title: AlertsMsgs.choose.localized, message: AlertsMsgs.selectToUpload.localized , preferredStyle: .actionSheet)
        let actionSelectCamera = UIAlertAction(title: AlertsMsgs.camera.localized, style: .default, handler: {
            UIAlertAction in
            self.openCamera(picker: imgPicker)
        })
        let actionSelectGallery = UIAlertAction(title: AlertsMsgs.gallery.localized, style: .default, handler: {
            UIAlertAction in
            self.openGallery(picker: imgPicker)
        })
        let actionCancel = UIAlertAction(title: AlertsMsgs.cancel.localized, style: .cancel, handler: nil)
        actionSheet.addAction(actionCancel)
        actionSheet.addAction(actionSelectCamera)
        actionSheet.addAction(actionSelectGallery)
        if kCurrentDevice.userInterfaceIdiom == .phone {
            self.guestInstance?.present(actionSheet, animated: true, completion: nil)
        } else {
            actionSheet.popoverPresentationController?.sourceView = guestInstance?.view
            actionSheet.popoverPresentationController?.sourceRect = rect!
            actionSheet.popoverPresentationController?.permittedArrowDirections = .any
            self.guestInstance?.present(actionSheet, animated: true, completion: nil)
        }
        imageClosure = {
            (image, success) in
            completion?(image, success)
        }
    }
    
    func getImageDict(instance: UIViewController, rect: CGRect?,mediaType: PickerMediaType = PickerMediaType.ImageOnly,completion: ((_ imgDict: [String: Any], _ success: Bool)->())?) {
        guestInstance = instance
        let imgPicker = UIImagePickerController()
        imgPicker.delegate = self
        imgPicker.allowsEditing = allowEditing
        switch mediaType {
        case .ImageOnly:
            imgPicker.mediaTypes = [(kUTTypeImage) as String]
        case .VideoOnly:
            imgPicker.mediaTypes = [(kUTTypeVideo) as String]
        case .AllMedia:
            imgPicker.mediaTypes = [(kUTTypeVideo) as String, (kUTTypeImage) as String]
        }
        let actionSheet = UIAlertController(title: AlertsMsgs.choose.localized, message: AlertsMsgs.selectToUpload.localized, preferredStyle: .actionSheet)
        let actionSelectCamera = UIAlertAction(title: AlertsMsgs.camera.localized, style: .default, handler: {
            UIAlertAction in
            
            self.openCamera(picker: imgPicker)
        })
        let actionSelectGallery = UIAlertAction(title: AlertsMsgs.gallery.localized, style: .default, handler: {
            UIAlertAction in
            self.openGallery(picker: imgPicker)
        })
        let actionCancel = UIAlertAction(title: AlertsMsgs.cancel.localized , style: .cancel, handler: nil)
        actionSheet.addAction(actionCancel)
        actionSheet.addAction(actionSelectCamera)
        actionSheet.addAction(actionSelectGallery)
        if kCurrentDevice.userInterfaceIdiom == .phone {
            self.guestInstance?.present(actionSheet, animated: true, completion: nil)
        } else {
            actionSheet.popoverPresentationController?.sourceView = guestInstance?.view
            actionSheet.popoverPresentationController?.sourceRect = rect!
            actionSheet.popoverPresentationController?.permittedArrowDirections = .up
            self.guestInstance?.present(actionSheet, animated: true, completion: nil)
        }
        imgInfoClosure = {
            (dictInfo, success) in
            completion?(dictInfo, success)
        }
    }
    
    func getImage(instance: UIViewController,isSourceCamera: Bool, completion: ((_ myImage: UIImage?, _ success: Bool)->())?) {
        guestInstance = instance
        AppConfig.defautMainQ.async {
            let imgPicker = UIImagePickerController()
            imgPicker.navigationBar.isHidden = false
            imgPicker.delegate = self
            imgPicker.allowsEditing = allowEditing
            imgPicker.mediaTypes =  [(kUTTypeImage) as String]
            if isSourceCamera {
                self.openCamera(picker: imgPicker)
                imgPicker.videoMaximumDuration = TimeInterval(maxVideoDuration)
            } else {
                self.openGallery(picker: imgPicker)
            }
            self.imageClosure = {
                (image, success) in
                completion?(image, success)
            }
        }
    }
    
    func getImageDict(instance: UIViewController,isSourceCamera:Bool,rect: CGRect?, mediaType: PickerMediaType = PickerMediaType.ImageOnly, completion: ((_ imgDict: [String: Any], _ success: Bool)->())?) {
        AppConfig.defautMainQ.async {
            self.guestInstance = instance
            let imgPicker = UIImagePickerController()
            imgPicker.delegate = self
            imgPicker.allowsEditing = false
            
            switch mediaType {
            case .ImageOnly:
                imgPicker.mediaTypes = [(kUTTypeImage) as String]
            case .VideoOnly:
                imgPicker.mediaTypes = [(kUTTypeMovie) as String]
            case .AllMedia:
                imgPicker.mediaTypes = [(kUTTypeMovie) as String, (kUTTypeImage) as String]
            }
            if isSourceCamera {
                self.openCamera(picker: imgPicker)
            } else {
                self.openGallery(picker: imgPicker)
            }
            
            self.imgInfoClosure = {
                (dictInfo, success) in
                completion?(dictInfo, success)
            }
        }
    }
    
    //Private Methods
    private func openCamera(picker: UIImagePickerController) {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            AppConfig.defautMainQ.async {
                picker.sourceType = UIImagePickerController.SourceType.camera
                picker.videoMaximumDuration = maxVideoDuration
                picker.videoQuality = .typeHigh
                self.guestInstance?.present(picker, animated: true, completion: nil)
            }
        } else {
            imgInfoClosure?([:], false)
            imageClosure?(nil, false)
            let alert = UIAlertController(title: AlertsMsgs.warning.localized, message: AlertsMsgs.dontHaveCamera.localized , preferredStyle: .alert)
            let actionOK = UIAlertAction(title: AlertsMsgs.ok.localized , style: .default, handler: nil)
            alert.addAction(actionOK)
            AppConfig.defautMainQ.async {
                self.guestInstance?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    private func openGallery(picker: UIImagePickerController) {
        AppConfig.defautMainQ.async {
            picker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.guestInstance?.present(picker, animated: true, completion: nil)
        }
    }
    
    //MARK: - Encode to mp4 from mov
    func encodeVideo(at videoURL: URL, completionHandler: ((URL?, Error?) -> Void)?)  {
        let avAsset = AVURLAsset(url: videoURL, options: nil)
            
        let startDate = Date()
            
        //Create Export session
        guard let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough) else {
            completionHandler?(nil, nil)
            return
        }
            
        //Creating temp path to save the converted video
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as URL
        let filePath = documentsDirectory.appendingPathComponent("rendered-Video.mp4")
            
        //Check if the file already exists then remove the previous file
        if FileManager.default.fileExists(atPath: filePath.path) {
            do {
                try FileManager.default.removeItem(at: filePath)
            } catch {
                completionHandler?(nil, error)
            }
        }
            
        exportSession.outputURL = filePath
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        let start = CMTimeMakeWithSeconds(0.0, preferredTimescale: 0)
        let range = CMTimeRangeMake(start: start, duration: avAsset.duration)
        exportSession.timeRange = range
            
        exportSession.exportAsynchronously(completionHandler: {() -> Void in
            switch exportSession.status {
            case .failed:
                print(exportSession.error ?? "NO ERROR")
                completionHandler?(nil, exportSession.error)
            case .cancelled:
                print("Export canceled")
                completionHandler?(nil, nil)
            case .completed:
                //Video conversion finished
                let endDate = Date()
                    
                let time = endDate.timeIntervalSince(startDate)
                print(time)
                print("Successful!")
                print(exportSession.outputURL ?? "NO OUTPUT URL")
                completionHandler?(exportSession.outputURL, nil)
                    
                default: break
            }
                
        })
    }
}

// MARK: - UIImagePicker Delegates

extension ImgPickerHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imgInfoClosure?([:], false)
        imageClosure?(nil, false)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])  {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        picker.dismiss(animated: true, completion: nil)
        
        if picker.sourceType != .camera, let videoUrl = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? URL {
            let videoSize = self.getFileSize(url: videoUrl)
            if videoSize > videoMaxSizeInMB {
                
                AlertControllerManager.showToast(message: AlertsMsgs.invalidVideoSize.localized, type: AlertType.error)

              //  Helper.showAlert(message: AlertsMsgs.invalidVideoSize.localized, type: .error)
                if let completion = imgInfoClosure {
                    completion([:], false)
                }
                return
            }
        }
        
        //imgInfoClosure?(info)
        
        imgInfoClosure?(info, true)
        var img: UIImage?
        if let editedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage {
            img = editedImage
        } else if let originalImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            img = originalImage
        }
        
        if img != nil {
            imageClosure?(img, true)
        }
    }
    
    func getFileSize(url: URL) -> Double {
        do {
            let attribute = try FILE_MANAGER.attributesOfItem(atPath: url.path)
            if let size = attribute[FileAttributeKey.size] as? NSNumber {
                return size.doubleValue / (byte1024 * byte1024)
            }
        } catch {
            print("Error: \(error)")
        }
        return 0.0
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
