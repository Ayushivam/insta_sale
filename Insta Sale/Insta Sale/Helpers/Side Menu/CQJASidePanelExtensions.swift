//
//  CQJASidePanelExtensions.swift
//  CabQ
//
//  Created by Raj Kumar Sharma on 12/09/17.
//  Copyright © 2017 Mobiloitte. All rights reserved.
//

import UIKit

class CQJASidePanelExtensions: NSObject {

}
extension UIViewController {
    
    func toggleSideMenu(animation: Bool = true) {
        
        self.sidePanelController.showLeftPanel(animated: true)
    }
    
    func slideToController(_ controller: UIViewController) {
        
        //        guard let centeralNavController = APPDELEGATE.sideMenuController.centerPanel as? UINavigationController,
        //            let appContainer = centeralNavController.viewControllers.first as? SMAppContainerVC else {
        //                return
        //        }
        guard (kAppDelegate.window?.rootViewController) != nil else {
            return
        }
        
        kAppDelegate.window?.rootViewController?.navigationController?.popToRootViewController(animated: false)
        kAppDelegate.window?.rootViewController?.navigationController?.setViewControllers([controller], animated: false)
        self.sidePanelController.toggleLeftPanel(nil)
    }
}
