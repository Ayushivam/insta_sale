//
//  StaticScreenViewController.swift
//  Zoloh
//
//  Created by Vibhuti Sharma on 14/02/19.
//  Copyright © 2019 nile. All rights reserved.
//

import UIKit

enum setNavigationPages {
    
    case faq,privacyPolicy,termsUse,cancellation
}

class StaticScreenViewController: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var titleLbl: UILabel!
    var navType : setNavigationPages?

    override func viewDidLoad() {
        super.viewDidLoad()
        let _ = (navType == setNavigationPages.faq) ? (titleLbl.text = "FAQ's") :  (navType == setNavigationPages.privacyPolicy) ? (titleLbl.text = "PRIVACY POLICY") : (navType == setNavigationPages.termsUse) ? (titleLbl.text = "TERMS & USE") :  (titleLbl.text = "CANCELLATION & REFUNDS")
        // Do any additional setup after loading the view.
    }

    @IBAction func bakcAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
