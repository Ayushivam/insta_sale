//
//  Helper.swift
//  NS804
//
//  Created by NS804 on 10/31/17.
//  Copyright © 2017 NS804. All rights reserved.
//

import UIKit
import AVFoundation
//import libPhoneNumber_iOS
//import Firebase
//import FirebaseDynamicLinks
//import FirebaseMessaging

class Helper {
    
    class func getBundleVersion() -> String {
        if let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            return buildNumber
        }
        return "1.0"
    }
    
    
    class func topViewController(for rootViewController: UIViewController!) -> UIViewController? {
        
        guard let rootVC = rootViewController else { return nil }
        if rootVC is UITabBarController {
            let rootTabBarVC = rootVC as! UITabBarController
            return topViewController(for: rootTabBarVC.selectedViewController)
        } else if rootVC is UINavigationController {
            let rootNavVC = rootVC as! UINavigationController
            return topViewController(for: rootNavVC.visibleViewController)
        } else if let rootPresentedVC = rootVC.presentedViewController {
            return topViewController(for: rootPresentedVC)
        }
        return rootViewController
    }    
    
    class func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.init(red: 26.0/255.0, green: 26.0/255.0, blue: 26.0/255.0, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 2.0
        shapeLayer.lineCap = .round
        shapeLayer.lineDashPattern = [10, 5]
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    

    
    class func blendImage(firstImage: UIImage, withImage secondImage: UIImage, drawX: CGFloat, drawY: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(firstImage.size, false, UIScreen.main.scale)
        let secondImageDrawX = drawX
        let secondImageDrawY = drawY
        firstImage.draw(at: CGPoint(x: 0,  y: 0))
        secondImage.draw(at: CGPoint(x: secondImageDrawX, y: secondImageDrawY))
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    class func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage? {
        let rect = CGRect(x: 0, y: 0, width: targetSize.width, height: targetSize.height)
        UIGraphicsBeginImageContextWithOptions(targetSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    class func getValueFromPlist(plistName:String = "Info", key: String) -> Any? {
        let configPath = Bundle.main.path(forResource: plistName, ofType: "plist")!
        let config = NSDictionary(contentsOfFile: configPath)
        return config!.object(forKey: key)
        
    }
    
    class func getGoogleClientID()->String{
        return self.getValueFromGooglePlist(key: "CLIENT_ID") as! String
    }
    
    private class func getValueFromGooglePlist(plistName:String = "credentials", key: String) -> Any? {
        let configPath = Bundle.main.path(forResource: plistName, ofType: "plist")!
        let config = NSDictionary(contentsOfFile: configPath)
        return config!.object(forKey: key)
    }
    
    class func showFootAndInchesFromCm(_ cms: Double) -> String {
        
        guard cms > 0 else{
            return ""
        }
        
        let feet = cms * 0.0328084
        var feetShow = Int(floor(feet))
        let feetRest: Double = ((feet * 100).truncatingRemainder(dividingBy: 100) / 100)
        var inches = Int(ceil(feetRest * 12))
        
        if inches == 12 {
            inches = 0
            feetShow += 1
        }
        
        return "\(feetShow)' \(inches)\""
        
    }
    
    class func getDateStringFromTimestampUtc(timestamp: Int64, _ dateFomat: String) -> String {
            let date = Date(timeIntervalSince1970: Double(timestamp/1000))
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFomat //Specify your format that you want
            return dateFormatter.string(from: date)
        }
    
    class func getDateFromTimestampUtc(timestamp: Int64, _ dateFomat: String) -> Date? {
            let date = Date(timeIntervalSince1970: Double(timestamp/1000))
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFomat //Specify your format that you want
            return date
        }
    
    class func convertDateFormater(_ date: String , _ dateFomat: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFomat
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = DateFormat.ddMMYYYY
            return  dateFormatter.string(from: date!)
        }
    
    
    class func convertDateFromString(dateString: String, withFormat format: String,inputFormat:String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = inputFormat
        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        return nil
    }


    
    class func getDateStringFromDate(_ date: Date, _ dateFomat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFomat //Specify your format that you want
        return dateFormatter.string(from: date)
    }
    
    class func getDirectoryPath() -> NSURL {
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("NewCarCapturedImages")
        let url = NSURL(string: path)
        return url!
    }
    
    class func saveImageDocumentDirectory(capturedImage: UIImage, imageName: String) {
        let docURL =  self.getDirectoryPath()
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: docURL.path!) {
            try! fileManager.createDirectory(atPath: docURL.path!, withIntermediateDirectories: true, attributes: nil)
        }
        let url = NSURL(string: docURL.path!)
        let imagePath = url!.appendingPathComponent(imageName)
        let urlString: String = imagePath!.absoluteString
        let imageData = capturedImage.jpegData(compressionQuality: 1.0)
        fileManager.createFile(atPath: urlString as String, contents: imageData, attributes: nil)
    }
    
    class func getImageFromDocumentDirectory(imageName: String) -> UIImage? {
        let fileManager = FileManager.default
        let imagePath = self.getDirectoryPath().appendingPathComponent(imageName)
        let urlString: String = imagePath!.absoluteString
        if fileManager.fileExists(atPath: urlString) {
            guard let image = UIImage(contentsOfFile: urlString) else {
                LogHelper.debug("Not able to create image with url" as AnyObject)
                return nil
            }
            return image
        } else {
            LogHelper.debug("Image not found" as AnyObject)
            return nil
        }
    }
    
    class func getAllImageFromDocumentDirectory() -> [UIImage] {
        let fileManager = FileManager.default
        let imagePath = self.getDirectoryPath()
        let urlString: String = imagePath.absoluteString!
        let contentOfDirectory = try! fileManager.contentsOfDirectory(atPath: urlString)
        var arrayofImages = [UIImage]()
        for content in contentOfDirectory {
            let imagePath = self.getDirectoryPath().appendingPathComponent(content)
            let urlString: String = imagePath!.absoluteString
            if fileManager.fileExists(atPath: urlString),
               let image = UIImage(contentsOfFile: urlString) {
                arrayofImages.append(image)
            }
        }
        return arrayofImages
    }
    
    class func getAllImageNameFromDocumentDirectory() -> [String] {
        let fileManager = FileManager.default
        let imagePath = self.getDirectoryPath()
        let urlString: String = imagePath.absoluteString!
        let contentOfDirectory = try! fileManager.contentsOfDirectory(atPath: urlString)
        return contentOfDirectory
    }
    
    class func isDirectoryExist() -> Bool {
        let docURL =  self.getDirectoryPath()
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: docURL.path!) {
            return true
        } else {
           return false
        }
    }
    
    class func deleteDirectory() {
        let docURL =  self.getDirectoryPath()
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: docURL.path!) {
            try! fileManager.removeItem(atPath: docURL.path!)
        } else {
            LogHelper.debug("Directory not found" as AnyObject)
        }
    }
    
    
    
    class func deleteImageFromDirectory(imageName: String) {
        let fileManager = FileManager.default
        let imagePath = self.getDirectoryPath().appendingPathComponent(imageName)
        let urlString: String = imagePath!.absoluteString
        if fileManager.fileExists(atPath: urlString) {
            try! fileManager.removeItem(atPath: urlString)
        } else {
            LogHelper.debug("File not found" as AnyObject)
        }
    }
    
    // MARK: - Get root view controller
    class func rootViewController() -> UIViewController {
        return (UIApplication.shared.keyWindow?.rootViewController)!
    }
    
    // MARK: - Get topmost view controller
    class func topMostViewController(rootViewController: UIViewController) -> UIViewController? {
        if let navigationController = rootViewController as? UINavigationController {
            return topMostViewController(rootViewController: navigationController.visibleViewController!)
        }
        if let tabBarController = rootViewController as? UITabBarController {
            if let selectedTabBarController = tabBarController.selectedViewController {
                return topMostViewController(rootViewController: selectedTabBarController)
            }
        }
        if let presentedViewController = rootViewController.presentedViewController {
            return topMostViewController(rootViewController: presentedViewController)
        }
        return rootViewController
    }
    
    class func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        imageGenerator.maximumSize = CGSize(width: 300, height: 300)
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
            
        } catch let error {
            print(error)
        }
        
        return nil
    }

    
    class func isValidOTP(otp:String)->Bool {
        return otp.count == 4
    }
    
    
    class  func minAgeValidation(_ date: Date)->Bool {
        
        if let minAgeBirthdate = Calendar.current.date(byAdding: .year, value: -18, to:Date()) {
            return date < minAgeBirthdate
        }
        
        return false
    }
    
    
    static func timeSince(_ dateStr: String, numericDates: Bool = false) -> String {
            if let date = DateFormatter.date(fromISO8601String: dateStr)?.toLocalTime() {
                let calendar = NSCalendar.current
                let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
                let now = Date().toLocalTime()
                let earliest = now < date ? now : date
                let latest = (earliest == now) ? date : now
                let components = calendar.dateComponents(unitFlags, from: earliest, to: latest)
                if components.year! >= 2 {
                    return "\(components.year!) years ago"
                } else if components.year! >= 1 {
                    if numericDates {
                        return "1 year ago"
                    } else {
                        return "1 year ago"
                    }
                } else if components.month! >= 2 {
                    return "\(components.month!) months ago"
                } else if components.month! >= 1 {
                    if numericDates {
                        return "1 month ago"
                    } else {
                        return "1 month ago"
                    }
                } else if components.weekOfYear! >= 2 {
                    return "\(components.weekOfYear!) weeks ago"
                } else if components.weekOfYear! >= 1 {
                    if numericDates {
                        return "1 week ago"
                    } else {
                        return "1 week ago"
                    }
                } else if components.day! >= 2 {
                    return "\(components.day!) days ago"
                } else if components.day! >= 1 {
                    if numericDates {
                        return "1 day ago"
                    } else {
                        return "1 day ago"
                    }
                } else if components.hour! >= 2 {
                    return "\(components.hour!) hours ago"
                } else if components.hour! >= 1 {
                    if numericDates {
                        return "1 hour ago"
                    } else {
                        return "1 hour ago"
                    }
                } else if components.minute! >= 2 {
                    return "\(components.minute!) mins ago"
                } else if components.minute! >= 1 {
                    if numericDates {
                        return "1 min ago"
                    } else {
                        return "1 min ago"
                    }
                } else if components.second! >= 3 {
                    return "\(components.second!) secs ago"
                } else {
                    return "Just now"
                }
            }
            return ""
        }
    

}
