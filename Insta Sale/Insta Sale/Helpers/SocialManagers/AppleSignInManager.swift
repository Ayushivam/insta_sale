//
//  AppleSignInManager.swift


import UIKit
import Foundation
import AuthenticationServices

class AppleSignInManager: NSObject, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {

    private var appleSignInResult:((_ socialUserInfo: SocialUserInfo?, _ message: String?, _ success: Bool)->())?

    // MARK: - Properties
    
    internal static let shared: AppleSignInManager = {
        return AppleSignInManager()
    }()

    // MARK: - Functions
    
    @available(iOS 13.0, *)
    func addSignInButton(onView completionClosure: @escaping (_ socialUserInfo: SocialUserInfo?, _ message: String?, _ success: Bool) -> ()) {
        
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]

        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()

        self.appleSignInResult = { socialUserInfo, message, success in
            completionClosure(socialUserInfo, message, success)
        }
    }

    @available(iOS 13.0, *)
    private func performExistingAccountSetupFlows() {
        let requests = [ASAuthorizationAppleIDProvider().createRequest(), ASAuthorizationPasswordProvider().createRequest()]

        let authorizationController = ASAuthorizationController(authorizationRequests: requests)
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }

    @available(iOS 13.0, *)
    func getCredentialState(userID: String) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        appleIDProvider.getCredentialState(forUserID: userID) { credentialState, _ in
            switch credentialState {
            case .authorized:
                // The Apple ID credential is valid.
                print("AppleSignInManager :: Status :: authorized")
                break
            case .revoked:
                // The Apple ID credential is revoked.
                print("AppleSignInManager :: Status :: revoked")
                break
            case .notFound:
                // No credential was found, so show the sign-in UI.
                print("AppleSignInManager :: Status :: notFound")
                break
            default:
                break
            }
        }
    }


    @available(iOS 13.0, *)
    @objc private func tapBtnAppleLogin(sender: ASAuthorizationAppleIDButton) {
        print("AppleSignInManager :: tapBtnAppleLogin:")
        
        
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]

        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }

    // MARK: - ASAuthorizationControllerDelegate
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("AppleSignInManager :: AppleID Credential Failed With Error: \(error.localizedDescription)")
        self.appleSignInResult?(nil, "Apple Login Error", false)
    }

    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            
            print("AppleID Credential Authorization: userId: \(appleIDCredential.user), email: \(String(describing: appleIDCredential.email)),  name: \(String(describing: appleIDCredential.fullName))")

            let dataBlank = "".data(using: .utf8)
            
            // unique ID for each user, this uniqueID will always be returned
            let userIdentifier = appleIDCredential.user
            
            // optional, might be nil
            let userFirstName = appleIDCredential.fullName?.givenName ?? ""
            
            // optional, might be nil
            let userLastName = appleIDCredential.fullName?.familyName ?? ""
            
            let userFullName = userFirstName + userLastName
            
            // optional, might be nil
            let userEmail = appleIDCredential.email ?? ""

            /*
             identityToken & authorizationCode - useful for server side, the app can send identityToken and authorizationCode
                to the server for verification purpose
            */
            
            var identityToken: String = ""
            if let token = appleIDCredential.identityToken {
                identityToken = String(bytes: token, encoding: .utf8) ?? ""
            }

            var authorizationCode: String = ""
            if let code = appleIDCredential.authorizationCode {
                authorizationCode = String(bytes: code, encoding: .utf8) ?? ""
            }

            let email = appleIDCredential.email ?? String(decoding: KeychainManager.load(key: APPLE_USER_EMAIL) ?? dataBlank!, as: UTF8.self)
            let fName = appleIDCredential.fullName?.givenName ?? String(decoding: KeychainManager.load(key: APPLE_USER_NAME) ?? dataBlank!, as: UTF8.self)
            let lName = appleIDCredential.fullName?.familyName ?? String(decoding: KeychainManager.load(key: APPLE_USER_LNAME) ?? dataBlank!, as: UTF8.self)
            let identifier = appleIDCredential.user
            
            
            //--- Save User Info in Keychain ---//
            if(fName.count > 0) {
                if let fNameData = fName.data(using: .utf8) {
                   _ = KeychainManager.save(key: APPLE_USER_NAME, data: fNameData)
                }
            }
            
            if(lName.count > 0) {
                if let lNameData = lName.data(using: .utf8) {
                   _ = KeychainManager.save(key: APPLE_USER_LNAME, data: lNameData)
                }
            }
            
            if(email.count > 0) {
                if let emailData = email.data(using: .utf8) {
                   _ = KeychainManager.save(key: APPLE_USER_EMAIL, data: emailData)
                }
            }
            
            
            let socialUserInfo = SocialUserInfo(type: SocialLoginType.apple, userId: identifier, name: "\(fName + " " + lName)", email: email, profilePic: "")
            
            print("AppleSignInManager :: User Info = UserId: \(userIdentifier), UserName: \(userFullName), UserEmail: \(userEmail), IdentityToken: \(identityToken), AuthorizationCode: \(authorizationCode)")
            
            self.appleSignInResult?(socialUserInfo, "Apple Login Success", true)
        }
    }

    // MARK: - ASAuthorizationControllerPresentationContextProviding
    
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //return appDelegate.window!
        return kSceneDelegate.window!
    }
}

