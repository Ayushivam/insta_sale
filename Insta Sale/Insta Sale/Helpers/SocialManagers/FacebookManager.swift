//
//  FacebookManager.swift
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit

let params = ["fields": "id, name, email, birthday, is_verified,user_friends"]

enum Permission: String {
    case publicProfile
    case birthday
    case friends
    case photos
    case email
    case gender
    case profileLink
    
    var stringValue: String {
        switch self {
        case .publicProfile: return "public_profile"
        case .birthday: return "user_birthday"
        case .photos: return "user_photos"
        case .email: return "email"
        case .gender: return "gender"
        case .profileLink: return "link"
        case .friends: return "user_friends"
        }
    }
}

class FacebookManager: NSObject {
    
    // MARK: - Singleton Instantiation
    private static let _shared: FacebookManager = FacebookManager()
    static var shared: FacebookManager {
        return ._shared
    }
    
    // MARK: - Private Properties
    private weak var guestViewC: UIViewController? = nil
    
    // MARK: - Public Properties
    public var fbInfoProvider: ((Bool, FBUser)->())? = nil
    
    // MARK: - Initializers
    private override init() {
        // This will resctrict the instantiation of this class.
    }
    
    // MARK: - Public Methods
    func loginWithFacebook(readPermissions: [String], target: UIViewController?) {
        guard let target = target else { return }
        self.guestViewC = target
        let login = LoginManager()
        login.logOut()
        login.logIn(permissions: readPermissions,
                    from: guestViewC, handler: { (result, error) in
                        if (error == nil) {
                            if let loginResult: LoginManagerLoginResult = result {
                                if(loginResult.isCancelled) {
                                    //Show Cancel alert
                                } else if(loginResult.grantedPermissions.contains("email")) {
                                    self.getInfoAboutUser()
                                    self.getProfileLink()
                                }
                            }
                        } else {
                            return
                        }
        })
    }
    
    // MARK: - Private Methods
    private func getInfoAboutUser() {
        if AccessToken.current != nil {
            let requestMe = GraphRequest.init(graphPath: "me", parameters: ["fields": "id, name, first_name ,last_name ,picture, email, birthday, gender, link, is_verified"])
            
            let connection = GraphRequestConnection()
            connection.add(requestMe, completionHandler: {
                (request, result, error) in
                let result = result as? [String: Any]
                if error != nil {
                    LoginManager().logOut()
                    return
                } else {
                    
                    let fbUser = FBUser()
                    if let birthday = result?["birthday"] as? String {
                        fbUser.birthday = birthday
                        fbUser.age = self.retrieveAgeFrom(birthday: birthday)
                    }
                    if let email = result?["email"] as? String {
                        fbUser.email = email
                    }
                    else {fbUser.email = ""}
                    if let name = result?["name"] as? String {
                        fbUser.name = name
                    }
                    if let firstname = result?["first_name"] as? String {
                        fbUser.first_name = firstname
                    }
                    if let lastname = result?["last_name"] as? String {
                        fbUser.last_name = lastname
                    }
                    if let picture = result?["picture"] as? [String : Any]
                    {
                        if let data = picture["data"] as? [String : Any]
                        {
                            if let url = data["url"] as? String
                            {
                                fbUser.profileImage = url
                            }
                        }
                    }
                    if let userID = result?["id"] as? String{
                        fbUser.fbId = userID
                        self.getFriendsList(userId: userID)
                    }
                    if let isVerified = result?["is_verified"] as? Bool {
                        fbUser.is_verified = isVerified
                    }
                    if let facebookToken = AccessToken.current?.tokenString {
                        fbUser.fbToken = facebookToken
                    }
                    if let phone = result?["phone"] as? String{
                        fbUser.phone = phone
                    }
                    self.fbInfoProvider?(true, fbUser)
                }
            })
            connection.start()
        }
    }
    
    private func retrieveAgeFrom(birthday: String) -> Int {
        let dateOfBrith = Date.convertFBDateString(dateString: birthday);
        let age = dateOfBrith.userAge();
        return age
    }
    
    //10215867683280093?fields=link
    func getProfileLink() {
        let request = GraphRequest(graphPath: "/me?fields=link")
        let connection = GraphRequestConnection()
        connection.add(request, completionHandler: {
                        (request, response, error) in
                        if(error != nil){
                        }
                        if(error == nil) {
                            print(response)
                        }})
        connection.start()
    }
    
    func getFriendsList(userId: String) {
        let request = GraphRequest(graphPath: "/me/friends", parameters: params)
        let connection = GraphRequestConnection()
        connection.add(request, completionHandler: {
            (request, response, error) in
            if(error != nil){
            }
            if(error == nil) {
                
            }})
        connection.start()
    }
}

