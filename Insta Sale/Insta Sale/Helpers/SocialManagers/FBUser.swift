//
//  File.swift
//

import Foundation

class FBUser: NSObject {
    
    var name: String?
    var fbId: String?
    var birthday: String?
    var email: String?
    var fbToken: String?
    var profileImage: String?
    var is_verified: Bool?
    var first_name: String?
    var last_name: String?
    var phone: String?

    var googleToken : String?
    var googleID : String?
    var googleImageURL : URL?
        

    var age: Int?
    
    func getSocialHash() -> [String: AnyObject] {
        var socialHash = [String: AnyObject]()
        if let isVerified = is_verified {
            if !(isVerified) {
              return socialHash
            }
        }
        if let fbId = fbId {
            socialHash["FacebookId"] = fbId as AnyObject
        }
        if let name = name {
            socialHash["Name"] = name as AnyObject
        }
        if let firstName = first_name {
            socialHash["first_name"] = firstName as AnyObject
        }
        
        if let lastName = last_name {
            socialHash["last_name"] = lastName as AnyObject
        }
        
        if let phone = phone {
            socialHash["phone"] = phone as AnyObject
        }
        
        if let email = email {
            socialHash["Email"] = email as AnyObject
        }
        if let profileImage = profileImage {
            socialHash["ProfileImage"] = profileImage as AnyObject
        }
        if let is_verified = is_verified {
            socialHash["is_verified"] = is_verified as AnyObject
        }
        return socialHash
    }
}
