//
//  Typealiases.swift
//  CampsEZ
//
//  Created by Mobcoder on 13/10/19.
//  Copyright © 2020 Mobcoder. All rights reserved.
//


import Foundation

public typealias EmptyCompletion = () -> Void
public typealias CompletionObject<T> = (_ response: T) -> Void
public typealias CompletionOptionalObject<T> = (_ response: T?) -> Void
//public typealias CompletionResponse = (_ response: Result<Void, Error>) -> Void
