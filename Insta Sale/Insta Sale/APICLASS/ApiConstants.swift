
import UIKit

let kCustId = "customerId"
let KCustEmail = "custEmail"
let kOrgId = "orgId"
let kAuthId = "authId"
let kRoleType = "roletype"
let kLat = "lat"
let kLong = "long"
let kFCMToken = "fcmTokenID"

//API Names
let NSUSERDEFAULT = UserDefaults.standard

let kSignUp = "signup"   
let kLogin = "login"
let kForgotPassword = "forgot-password"
let kGetCategory = "getCS"
let kGetShopCategory = "getSC"
let kGetProductDetail = "getProductBySubcategory"
let kGetFeaturedCategories = "getFeaturedCategories"
let kWishList = "wishlist"
let kRemoveItemFromWishlist = "removeItemFromWishlist"
let kGetWishList = "getWishlist"
let kSearchKey = "getSuggestions"
let kGetProductDetails = "getProductByIdd?id="
let kGetFilters = "getFilters?subcategory_id="
let kComboProduct = "getCombo?product_id="
let kGetReviews = "get_product_rating?product_id="
let kGetReviewsVariationID = "&product_variation_id="
let kGetCoupons = "getCoupons"
let kGetProfile = "myProfile?user_id="
let kGetAddress = "getAddresses?user_id="
let KRemoveAddress = "removeAddress"
let kCreateOrupdateAddress = "createOrupdateAddress"
let kGetOrders = "getOrders?user_id="
let kUpdateProfile = "updateProfile"
let kupdateProfileImage = "update-profile-image"

class ApiConstants: NSObject {
    
}
