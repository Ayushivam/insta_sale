//
//  StaticScreenViewController.swift
//  Zoloh
//
//  Created by Ayushi Agarwal on 01/06/19.
//  Copyright © 2019 nile. All rights reserved.
//

import UIKit

class APIManager: NSObject {
    
    // MARK: ---  LogIN up Api Handler  ---
    
    class func apiForLogIn(email : String, password : String, controller:UINavigationController, completion: @escaping ((Bool, Dictionary<String,AnyObject>) -> Void)) {
        
        var userDict : Dictionary = Dictionary<String,AnyObject>()
        userDict["email"] = email as AnyObject
        userDict["password"] = password as AnyObject
        
        ServiceHelper.request(userDict, method: .post, apiName: kLogin, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {

                        let data : Dictionary = results.validatedValue("user", expected: "" as AnyObject) as! Dictionary<String,AnyObject>
                        kUserDefaults.isUserLogin = true
                        kAppDelegate.setRootViewC()

                        completion(true, data)

                    }
                    
                } else {
                    
                    let err =  results["errors"] as? Dictionary<String,AnyObject>
                    if err == nil {
                        MessageErrView.showMessage(message: results["errors"] as? String ?? "", time: 3.0, verticalAlignment: .bottom)
                        
                    }
                    else {
                        let vErr = err?["email"] as? Array<AnyObject>
                        
                        MessageErrView.showMessage(message: vErr?[0] as? String ?? "", time: 3.0, verticalAlignment: .bottom)
                    }
                    completion(false, [:])
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [:])
            }
        }
    }
    
    
    // MARK: ---  Forgot PAssword Api Handler  ---
    
    class func apiForForgotPAssword(email : String, controller:UINavigationController, completion: @escaping ((Bool, String) -> Void)) {
        
        var userDict : Dictionary = Dictionary<String,AnyObject>()
        userDict["email"] = email as AnyObject
        
        ServiceHelper.request(userDict, method: .post, apiName: kForgotPassword, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: false as AnyObject) as! Bool
                    if status == true {

                        let msg : String = results.validatedValue("msg", expected: "" as AnyObject) as! String

                        completion(true, msg)

                    }
                    
                } else {
                    let err =  results["errors"] as? Dictionary<String,AnyObject>
                    if err == nil {
                        MessageErrView.showMessage(message: results["message"] as? String ?? "", time: 3.0, verticalAlignment: .bottom)
                        
                    }
                    else {
                        let vErr = err?["email"] as? Array<AnyObject>
                        
                        MessageErrView.showMessage(message: vErr?[0] as? String ?? "", time: 3.0, verticalAlignment: .bottom)
                    }
                    completion(false, "")
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, "")
            }
        }
    }

    
    // MARK: ---  Categoies Get Api Handler  ---
    
    class func apiForGetCategoryData(controller:UINavigationController, completion: @escaping ((Bool, Array<AnyObject>, Array<AnyObject>, Array<AnyObject>, String, String, String) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
      
        
        ServiceHelper.request(userDict, method: .get, apiName: kGetCategory, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("code", expected: "" as AnyObject) as! String
                    if status == "1" {
                       
                        let category : Array  = results.validatedValue("categories", expected: "" as AnyObject) as! Array<AnyObject>
                        
                        let subCategories : Array  = results.validatedValue("subcategories", expected: "" as AnyObject) as! Array<AnyObject>
                        let banners : Array  = results.validatedValue("banners", expected: "" as AnyObject) as! Array<AnyObject>
                        let catUrl : String = results.validatedValue("cimg_url", expected: "" as AnyObject) as! String
                        let subCatUrl : String = results.validatedValue("scimg_url", expected: "" as AnyObject) as! String
                        let bannerUrl : String = results.validatedValue("bimg_url", expected: "" as AnyObject) as! String

                        completion(true, category, subCategories, banners, catUrl, subCatUrl, bannerUrl)
                    }
                    
                } else {
                    completion(false, [], [], [], "", "", "")
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [], [], [], "", "", "")
            }
        }
    }

    //    // MARK: ---  SignUp Api Handler  ---
    class func apiForSignUp(name: String, email : String, password : String, signUpType: String, controller:UINavigationController, completion: @escaping ((Bool, String) -> Void)) {
        
        var userDict : Dictionary = Dictionary<String,AnyObject>()
        userDict["email"] = email as AnyObject
        userDict["password"] = password as AnyObject
        userDict["name"] = name as AnyObject
        userDict["signUpType"] = "0" as AnyObject
        userDict["device_token"] = "rtrw" as AnyObject
        userDict["device_type"] = "1" as AnyObject
        userDict["mobile"] = "766" as AnyObject
        userDict["country_code"] = "6767" as AnyObject

      
        ServiceHelper.request(userDict, method: .post, apiName: kSignUp, hudType: .default) { (result, error, responseCode) in
            
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("status", expected: true as AnyObject) as! Bool
                    if status == true {
                        let msg : String = results.validatedValue("msg", expected: "" as AnyObject) as! String

                        completion(true, msg)
                    }
                    else {
                        // AlertController.alert(title: results.validatedValue("message", expected: "" as AnyObject) as! String)
                        completion(false,"")
                    }
                } else {
                    MessageErrView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,"")
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,"")
            }
        }
    }
    
    // MARK: ---  Shop catgories Get Api Handler  ---
    
    class func apiForGetShopCategoryData(id: String, controller:UINavigationController, completion: @escaping ((Bool, Array<AnyObject>, String) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
      
        
        ServiceHelper.request(userDict, method: .get, apiName: kGetShopCategory + "?id=" + id, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("code", expected: "" as AnyObject) as! String
                    if status == "1" {
                       
                        
                        let subCategories : Array  = results.validatedValue("subcategories", expected: "" as AnyObject) as! Array<AnyObject>
                        let subCatUrl : String = results.validatedValue("scimg_url", expected: "" as AnyObject) as! String

                        completion(true, subCategories,subCatUrl)
                    }
                    
                } else {
                    completion(false, [], "")
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [], "")
            }
        }
    }
    
    // MARK: ---  Product Detail Category Get Api Handler  ---
    
    class func apiForGetProductDetailCategoryData(id: String, opt: String, page: Int,controller:UINavigationController, completion: @escaping ((Bool, Array<AnyObject>, Int) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
        let subCat = "?subcategory_id=" + id
        let opt = subCat + "&opt=" + opt
        let userID = opt + "&user_id=" + "\(kUserDefaults.userId)"
        let params = userID + "&page=" + "\(page)"
        
        ServiceHelper.request(userDict, method: .get, apiName: kGetProductDetail + params, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("code", expected: "" as AnyObject) as! String
                    if status == "1" {
                       
                        
                        let subCategories : Array  = results.validatedValue("products", expected: "" as AnyObject) as! Array<AnyObject>
                        let total : Int = results.validatedValue("total", expected: 0 as AnyObject) as! Int

                        completion(true, subCategories,total)
                    }
                    
                } else {
                    completion(false, [], 0)
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [], 0)
            }
        }
    }
    
    
    // MARK: ---  Featured Categories Get Api Handler  ---
    
    class func apiForGetFeaturedCategories(controller:UINavigationController, completion: @escaping ((Bool, Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
      
        
        ServiceHelper.request(userDict, method: .get, apiName: kGetFeaturedCategories, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("code", expected: "" as AnyObject) as! String
                    if status == "1" {
                       
                        let featuredcategories : Array  = results.validatedValue("featuredcategories", expected: "" as AnyObject) as! Array<AnyObject>
                    

                        completion(true, featuredcategories)
                    }
                    
                } else {
                    completion(false, [])
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [])
            }
        }
    }
    
    
    // MARK: ---  Wishlist Api Handler  ---
    
    class func apiForWishlist(prodID : String, prodVariationID: String, controller:UINavigationController, completion: @escaping ((Bool) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
        let userID =  "?user_id=" + "\(kUserDefaults.userId)"
        let productID = "&product_id=" + prodID
        let prodVariationID = "&product_variation_id=" + prodVariationID
        let params = userID + productID + prodVariationID
        ServiceHelper.request(userDict, method: .post, apiName: kWishList + params, hudType: .smoothProgress) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("code", expected: "" as AnyObject) as! String
                    if status == "1" {
                    

                        completion(true)
                    }
                    
                } else {
                    completion(false)
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false)
            }
        }
    }

    
    // MARK: ---  Remove Wishlist Api Handler  ---
    
    class func apiForRemoveWishlist(whislist : String, controller:UINavigationController, completion: @escaping ((Bool) -> Void)) {
        
        var userDict : Dictionary = Dictionary<String,AnyObject>()
        userDict["user_id"] = "\(kUserDefaults.userId)" as AnyObject
        userDict["wishlist_id"] = whislist as AnyObject

        ServiceHelper.request(userDict, method: .post, apiName: kRemoveItemFromWishlist, hudType: .smoothProgress) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("code", expected: "" as AnyObject) as! String
                    if status == "1" {
                        completion(true)
                    }
                    
                } else {
                    completion(false)
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false)
            }
        }
    }
    
    // MARK: ---  Get WishList Get Api Handler  ---
    
    class func apiForGetWishList(controller:UINavigationController, completion: @escaping ((Bool, Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
      
        
        ServiceHelper.request(userDict, method: .get, apiName: kGetWishList + "?user_id=" + "\(kUserDefaults.userId)" , hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("code", expected: "" as AnyObject) as! String
                    if status == "1" {
                       
                        let getWishlist : Array  = results.validatedValue("getWishlist", expected: "" as AnyObject) as! Array<AnyObject>
                    

                        completion(true, getWishlist)
                    }
                    
                } else {
                    completion(false, [])
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [])
            }
        }
    }
    
    
    // MARK: ---  Search Api Handler  ---
    
    class func apiForSearch(searchText: String, controller:UINavigationController, completion: @escaping ((Bool, Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
      
        
        ServiceHelper.request(userDict, method: .get, apiName: kSearchKey + "?qry=" + searchText , hudType: .smoothProgress) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("code", expected: "" as AnyObject) as! String
                    if status == "1" {
                       
                        let getWishlist : Array  = results.validatedValue("suggestions", expected: "" as AnyObject) as! Array<AnyObject>
                    

                        completion(true, getWishlist)
                    }
                    
                } else {
                    completion(false, [])
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [])
            }
        }
    }
    
    // MARK: ---  Get Filters Api Handler  ---
    
    class func apiForGetFilters(id: String, controller:UINavigationController, completion: @escaping ((Bool, Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
      
        
        ServiceHelper.request(userDict, method: .get, apiName: kGetFilters + id , hudType: .smoothProgress) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let product_filter : Array  = results.validatedValue("product_filter", expected: [] as AnyObject) as! Array<AnyObject>
                        completion(true, product_filter)
                } else {
                    completion(false, [])
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [])
            }
        }
    }
    
    // MARK: ---  Get Product Detail Api Handler  ---
    
    class func apiForProductDetail(id: String, controller:UINavigationController, completion: @escaping ((Bool, Array<AnyObject>,Dictionary<String,AnyObject>) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
      
        
        ServiceHelper.request(userDict, method: .get, apiName: kGetProductDetails + id , hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let produtsDetails : Dictionary  = results.validatedValue("product", expected: [] as AnyObject) as! Dictionary<String,AnyObject>

                    let relatedProducts : Array  = results.validatedValue("relatedProducts", expected: [] as AnyObject) as! Array<AnyObject>
                       
                    

                        completion(true, relatedProducts,produtsDetails)
                    
                    
                } else {
                    completion(false, [],[:])
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [],[:])
            }
        }
    }
    
    // MARK: ---  Combo Product Api Handler  ---
    
    class func apiForComboProduct(id: String, controller:UINavigationController, completion: @escaping ((Bool, Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
        
        ServiceHelper.request(userDict, method: .get, apiName: kComboProduct + id , hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    
                    let array : Array  = results.validatedValue("combo", expected: [] as AnyObject) as! Array<AnyObject>

                    

                        completion(true, array)
                    
                    
                } else {
                    completion(false, [])
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [])
            }
        }
    }
    
    
    
    // MARK: ---  GetReviews Api Handler  ---
    
    class func apiForGetReviews(id: String,vId: String, controller:UINavigationController, completion: @escaping ((Bool, Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
      
        
        ServiceHelper.request(userDict, method: .get, apiName: kGetReviews + id + kGetReviewsVariationID + vId , hudType: .smoothProgress) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("code", expected: "" as AnyObject) as! String
                    if status == "1" {
                       
                        let getWishlist : Array  = results.validatedValue("productRatings", expected: "" as AnyObject) as! Array<AnyObject>
                    

                        completion(true, getWishlist)
                    }
                    
                } else {
                    completion(false, [])
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [])
            }
        }
    }
    // MARK: ---  Coupons Api Handler  ---

    class func apiForCoupons(controller:UINavigationController, completion: @escaping ((Bool, Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
        
        ServiceHelper.request(userDict, method: .get, apiName: kGetCoupons , hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    
                    let array : Array  = results.validatedValue("couponDetails", expected: [] as AnyObject) as! Array<AnyObject>
                        completion(true, array)
                    
                    
                } else {
                    completion(false, [])
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [])
            }
        }
    }
    
    // MARK: ---  GetProfile up Api Handler  ---
    
    class func apiForGetProfile(controller:UINavigationController, completion: @escaping ((Bool, Dictionary<String,AnyObject>,String) -> Void)) {
        
        var userDict : Dictionary = Dictionary<String,AnyObject>()
      
        let userID = "\(kUserDefaults.userId)"

        ServiceHelper.request(userDict, method: .get, apiName: kGetProfile + userID, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("code", expected: "" as AnyObject) as! String
                    if status == "1" {

                        let data : Dictionary = results.validatedValue("myProfile", expected: "" as AnyObject) as! Dictionary<String,AnyObject>
                        
                        let image_url : String = results.validatedValue("image_url", expected: "" as AnyObject) as! String


                        completion(true, data,image_url)

                    }
                    
                } else {
                    
                    let err =  results["errors"] as? Dictionary<String,AnyObject>
                    if err == nil {
                        MessageErrView.showMessage(message: results["errors"] as? String ?? "", time: 3.0, verticalAlignment: .bottom)
                        
                    }
                    else {
                        let vErr = err?["email"] as? Array<AnyObject>
                        
                        MessageErrView.showMessage(message: vErr?[0] as? String ?? "", time: 3.0, verticalAlignment: .bottom)
                    }
                    completion(false, [:],"")
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [:],"")
            }
        }
    }
    
    // MARK: ---  GetAddress Api Handler  ---

    class func apiForGetAddress(controller:UINavigationController, completion: @escaping ((Bool, Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
        let userID = "\(kUserDefaults.userId)"

        ServiceHelper.request(userDict, method: .get, apiName: kGetAddress +  userID, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    
                    let array : Array  = results.validatedValue("addressDeatils", expected: [] as AnyObject) as! Array<AnyObject>
                        completion(true, array)
                    
                    
                } else {
                    completion(false, [])
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [])
            }
        }
    }
    
    class func apiForRemoveAddress(addressID : String, controller:UINavigationController, completion: @escaping ((Bool) -> Void)) {
        
        var userDict : Dictionary = Dictionary<String,AnyObject>()
        userDict["user_id"] = "\(kUserDefaults.userId)" as AnyObject
        userDict["address_id"] = addressID as AnyObject

        ServiceHelper.request(userDict, method: .post, apiName: KRemoveAddress, hudType: .smoothProgress) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("code", expected: "" as AnyObject) as! String
                    if status == "1" {
                        completion(true)
                    }
                    
                } else {
                    completion(false)
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false)
            }
        }
    }
    
    class func apiForCreateUpdateAddress(name : String, mobNum : String, pincode: String, address: String, locality_town : String, city_district: String, stateLoc : String, address_type: String, defaultType: String,isEdit: Bool, address_Id : String, controller:UINavigationController, completion: @escaping ((Bool,String) -> Void)) {
       
        var userDict : Dictionary = Dictionary<String,AnyObject>()
        userDict["user_id"] = "\(kUserDefaults.userId)" as AnyObject
        userDict["name"] = name as AnyObject
        userDict["mobile_no"] = mobNum as AnyObject
        userDict["pincode"] = pincode as AnyObject
        userDict["address"] = address as AnyObject
        userDict["locality_town"] = locality_town as AnyObject
        userDict["city_district"] = city_district as AnyObject
        userDict["state"] = stateLoc as AnyObject
        userDict["address_type"] = address_type as AnyObject
        userDict["default"] = defaultType as AnyObject
        if isEdit == true {
            userDict["id"] = address_Id as AnyObject

        }

        ServiceHelper.request(userDict, method: .post, apiName: kCreateOrupdateAddress, hudType: .smoothProgress) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("code", expected: "" as AnyObject) as! String
                    if status == "1" {
                        let message = results.validatedValue("message", expected: "" as AnyObject) as! String

                        completion(true,message)
                    }
                    
                } else {
                    completion(false,"")
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,"")
            }
        }
    }
    
    
    class func apiForUpdateProfile(name : String, mobNum : String, gender: String, dob: String, address: String, controller:UINavigationController, completion: @escaping ((Bool,String) -> Void)) {
       
        var userDict : Dictionary = Dictionary<String,AnyObject>()
        userDict["id"] = "\(kUserDefaults.userId)" as AnyObject
        userDict["name"] = name as AnyObject
        userDict["mobile_no"] = mobNum as AnyObject
        userDict["dob"] = dob as AnyObject
        userDict["gender"] = gender as AnyObject
        userDict["location"] = address as AnyObject

        ServiceHelper.request(userDict, method: .post, apiName: kUpdateProfile, hudType: .smoothProgress) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let status = results.validatedValue("code", expected: "" as AnyObject) as! String
                    if status == "1" {
                        let msg = results.validatedValue("msg", expected: "" as AnyObject) as! String

                        completion(true,msg)
                    }
                    
                } else {
                    completion(false,"")
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,"")
            }
        }
    }
    
    
    // MARK: ---  GetOrders Api Handler  ---

    class func apiForGetOrders(controller:UINavigationController, completion: @escaping ((Bool, Array<AnyObject>) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
        let userID = "\(kUserDefaults.userId)"

        ServiceHelper.request(userDict, method: .get, apiName: kGetOrders +  userID, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    
                    let array : Array  = results.validatedValue("getOrders", expected: [] as AnyObject) as! Array<AnyObject>
                        completion(true, array)
                    
                    
                } else {
                    completion(false, [])
                }
            } else {
                MessageErrView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false, [])
            }
        }
    }
    
    
    // MARK: ---   Update Profile
    class func apiForUpdateProfile(dict:Dictionary<String,AnyObject>,data:Data,haveImage:Bool,completion: @escaping ((Bool) -> Void)) {

        ServiceHelper.hideAllHuds(false, type: loadingIndicatorType.default)
        ServiceHelper.updateProfileInfo(parameterDict: dict, imageDta: data, haveImage: haveImage) { (dict, error) in
            ServiceHelper.hideAllHuds(true, type: loadingIndicatorType.default)
            if error == nil{
                let status = dict!["status"] as! Bool//results.validatedValue("status", expected: false as AnyObject) as! Bool
                if status{
                    completion(true)
                }else {
                    completion(false)
                }
        }
        else {
                completion(false)
            }
        }
    }

}


