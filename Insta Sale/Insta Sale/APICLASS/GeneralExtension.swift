//
//  GeneralExtension.swift
//  ProjectTemplate
//  Created by Ayushi Agarwal on 01/06/19.
//  Copyright Â© 2017 Nile technology. All rights reserved.
//

import UIKit
import Photos

// MARK:- Array Extensions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

extension Array {
    func contains<T>(_ obj: T) -> Bool where T : Equatable {
        return self.filter({$0 as? T == obj}).count > 0
    }
}


// MARK:- UIImage URL Extensions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

extension UIImageView{
    
    func setImageFromURl(stringImageUrl url: String){
        
        if let url = NSURL(string: url) {
            if let data = NSData(contentsOf: url as URL) {
                self.image = UIImage(data: data as Data)
            }
        }
    }
}


// MARK:- NSURL Extensions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

extension URL {
    
    var isValid: Bool {
        return UIApplication.shared.canOpenURL(self)
    }
}


// MARK:- Int/Float/Double Extensions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

extension Int {
    func format(_ f: String) -> String {
        return NSString(format: "%\(f)d" as NSString, self) as String
    }
}

extension Double {
    func format(_ f: String) -> String {
        return NSString(format: "%\(f)f" as NSString, self) as String
    }
}

extension Float {
    func format(_ f: String) -> String {
        return NSString(format: "%\(f)f" as NSString, self) as String
    }
}

// MARK:- Dictionary Extensions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

extension Dictionary {
    mutating func unionInPlace(
        _ dictionary: Dictionary<Key, Value>) {
        for (key, value) in dictionary {
            self[key] = value
        }
    }
    
    mutating func unionInPlace<S: Sequence>(_ sequence: S) where S.Iterator.Element == (Key,Value) {
        for (key, value) in sequence {
            self[key] = value
        }
    }
    
    func validatedValue(_ key: Key, expected: AnyObject) -> AnyObject {
        
        if let object = self[key] {
            if object is NSNumber && expected is String {
                    return "\(object)" as AnyObject
            }

            else if object is String {
                if ((object as! String == "null") || (object as! String == "<null>") || (object as! String == "(null)") || (object as! String == "NULL")) {
                    //logInfo("null string")
                    return "" as AnyObject
                }
            }
            else if  object is NSNull
            {
                return "" as AnyObject
                
            }
            return object as AnyObject
        }
        else {            
            if expected is String || expected as! String == "" {
                return "" as AnyObject
            }
            return expected
        }
    }
}

extension Date {
    var jsonDate: String {
        let ticks = lround(timeIntervalSince1970 * 1000)
        return "/Date(\(ticks))/"
    }
}

extension UIView {

    // OUTPUT 1
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {

        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}

extension UIImage {

    func fixOrientationAndResize(width:CGFloat,height:CGFloat) -> UIImage? {

        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: height)))
        imageView.contentMode = .scaleAspectFill
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
       
        if self.imageOrientation == UIImage.Orientation.up {
            return result
        }

            return result
    }
}

extension URL {
    
    func saveVideo(videoPath:URL){

        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoPath)
        }) { saved, error in
            if saved {
                let fetchOptions = PHFetchOptions()
                fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                
                let fetchResult = PHAsset.fetchAssets(with: .video, options: fetchOptions).firstObject
                // fetchResult is your latest video PHAsset
                // To fetch latest image  replace .video with .image
            }
        }
    }
}
